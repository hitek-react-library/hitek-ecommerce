import { Text, TouchableOpacity, View } from 'react-native';
// import {
//   createStackNavigator,
//   StackScreenProps,
// } from '@react-navigation/stack';
import React from 'react'
// // import { AppRegistry } from 'react-native'

// type RootStackParamList = {
//   Home: undefined;
//   Profile: { userId: string };
//   Feed: { sort: 'latest' | 'top' } | undefined;
// };

// const RootStack = createStackNavigator<RootStackParamList>();

// type ProfileProps = StackScreenProps<RootStackParamList, 'Profile'>;
// // type HomeProps = StackScreenProps<RootStackParamList, 'Profile'>;

// const HitekNav = () => {
//   // return (
//   //   <RootStack.Group
//   //   screenOptions={{
//   //     headerShown: false,
//   //   }}>
//   //     <RootStack.Screen name="Home" component={HomeScreen} />
//   //     <RootStack.Screen name="Profile" component={SettingsScreen} />
//   //   </RootStack.Group>
//   // );
//   return (
//     <RootStack.Navigator
//       screenOptions={{
//         headerShown: false,
//       }}
//     >
//       <RootStack.Screen name="Home" component={HomeScreen} />
//       <RootStack.Screen name="Profile" component={SettingsScreen} />
//     </RootStack.Navigator>
//   );
// };

// export default HitekNav;

export const HomeScreen = (props: any) => {
  const { navigation } = props;
  return (
    <View>
      <Text>Home screen</Text>
      <TouchableOpacity
        onPress={() => {
          navigation.replace('SettingHitek');
        }}
      >
        <Text>Open setting</Text>
      </TouchableOpacity>
    </View>
  );
};

export const SettingsScreen = () => {
  return (
    <View>
      <Text>Setting screen</Text>
    </View>
  );
};
