import { googleLogin, signOutGoogle } from '../services/google_service';
import * as React from 'react';
import { appleLogin, signOutApple } from '../services/apple_service';
import { facebookLogin, signOutFacebook } from '../services/facebook_service';
import { kakaoLogin, signOutKakao } from '../services/kakao_service';
import { naverLogin, signOutNaver } from '../services/naver_service';
import { SignInMethod } from './method_enum';
import {
  getItem as getToken,
  setItem as setToken,
  removeItem as removeToken,
} from './storage';
import { emailPasswordLogin } from '../services/other_login_service';

interface AuthContextState {
  isLogged: boolean;
  user?: any;
  method?: number;
  authToken?: string;
  signIn: (method: number) => any;
  signOut: () => void;
  autoSignIn: () => boolean;
  signInWithPassword: (username: string, password: string) => any;
  signInWithFbToken: (fbToken: string) => any;

  // findMyAccount: (phone: string, birthday: number) => string;
  // generatePasswordViaEmail: (email: string) => boolean;
  // resetPasswordViaDeeplink: (email:string) => boolean;
  // changePasswordByToken: (email: string, token: string) => boolean;
  // verifyEmailStep1: (email: string) => boolean;
  // verifyEmailStep2: (email: string, code: string) => boolean;
}

const AuthContext = React.createContext<AuthContextState>({
  isLogged: false,
  signIn: () => {},
  signOut: () => {},
  autoSignIn: () => false,
  signInWithPassword: () => {},
  signInWithFbToken: () => {},
  // findMyAccount: () => "",
  // generatePasswordViaEmail: () => false,
  // resetPasswordViaDeeplink: () => false,
  // changePasswordByToken: () => false,
  // verifyEmailStep1: () => false,
  // verifyEmailStep2: () => false,
});

export const useAuthorization = () => {
  const context = React.useContext(AuthContext);
  if (!context) {
    throw new Error('Error');
  }
  return context;
};

export const AuthProvider = (props: any) => {
  const [state, dispatch] = React.useReducer(reducer, {
    status: 'idle',
    authToken: null,
  });

  const actions = React.useMemo(
    () => ({
      signInWithFbToken: async (fbToken: string) => {
        const response: any = await loginWithFbToken(fbToken);
        const token = response.token;
        dispatch({
          type: 'SIGN_IN',
          token,
          method: SignInMethod.PHONE_CODE,
          user: response.object,
        });
        await setToken(token);
        return response.object;
      },
      signInWithPassword: async (username: string, password: string) => {
        const response: any = await emailPasswordLogin(username, password);
        const token = response.token;
        dispatch({
          type: 'SIGN_IN',
          token,
          method: SignInMethod.EMAIL_AND_PASSWORD,
          user: response.result,
        });
        await setToken(token);
        return response.result;
      },
      signIn: async (method: number) => {
        let response: any = undefined;
        switch (method) {
          case SignInMethod.GOOGLE:
            response = await googleLogin();
            break;
          case SignInMethod.FACEBOOK:
            response = await facebookLogin();
            break;
          case SignInMethod.APPLE:
            response = await appleLogin();
            break;
          case SignInMethod.NAVER:
            response = await naverLogin();
            break;
          case SignInMethod.KAKAO:
            response = await kakaoLogin();
            break;
          default:
            response = undefined;
            break;
        }
        if (response === undefined) {
          throw new TypeError(`Method ${method} not supported!`);
        }

        const token = response.token;
        dispatch({ type: 'SIGN_IN', token, method, user: response.object });
        await setToken(token);
        return response.object;
      },
      signOut: async () => {
        switch (state.method) {
          case SignInMethod.GOOGLE:
            signOutGoogle();
            break;

          case SignInMethod.FACEBOOK:
            signOutFacebook();
            break;

          case SignInMethod.NAVER:
            signOutNaver();
            break;

          case SignInMethod.KAKAO:
            signOutKakao();
            break;

          case SignInMethod.APPLE:
            signOutApple();
            break;
        }
        dispatch({ type: 'SIGN_OUT' });
        await removeToken();
      },
      getToken: async () => await getToken(),
      autoSignIn: async () => {
        let signInSuccess = false;
        try {
          const authToken = await getToken();
          // Todo get profile

          if (authToken !== null) {
            signInSuccess = true;
            dispatch({ type: 'SIGN_IN', token: authToken });
          } else {
            signInSuccess = false;
            dispatch({ type: 'SIGN_OUT' });
          }
        } catch (e) {
          console.log(e);
        }
        return signInSuccess;
      },
      // findMyAccount: async (phone: string, birthday: number) => {
      //   const response: any = await findMyAccount(phone, birthday);
      //   return response?.email;
      // },
      // generatePasswordViaEmail: async (email: string) => {
      //   const response = await generatePasswordViaEmail(email);
      //   return response;
      // },
      // resetPasswordViaDeeplink: async (email:string) => {
      //   const response = await resetPasswordViaDeepLink(email);
      //   return response;
      // },
      // changePasswordByToken: async (token: string, password: string) => {
      //   const response = await changePasswordByToken(token, password);
      //   return response;
      // },
      // verifyEmailStep1: async (email: string) => {
      //   const response = await verifyEmailStep1(email);
      //   return response;
      // },
      // verifyEmailStep2: async (email: string, code: string) => {
      //   const response = await verifyEmailStep2(email, code);
      //   return response;
      // }
    }),
    [state, dispatch]
  );

  return (
    <AuthContext.Provider value={{ ...state, ...actions }}>
      {props.children}
    </AuthContext.Provider>
  );
};

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case 'SIGN_OUT':
      return {
        ...state,
        isLogged: false,
        authToken: undefined,
        user: undefined,
        method: undefined,
      };
    case 'SIGN_IN':
      return {
        ...state,
        isLogged: true,
        authToken: action.token,
        method: action.method,
        user: action.user,
      };
  }
};
