import { ConfigParam, NaverLogin } from '@react-native-seoul/naver-login';
import { Platform } from 'react-native';
import {
  LoginMethod,
  LOGIN_METHOD,
  SOCIAL_CONFIG,
} from '../utils/constants';
import { fetch } from '../utils/fetch';
import { print } from '../utils/log';
import { read, write } from './local_service';

export interface NaverConfig {
  androidConfig: ConfigParam;
  iosConfig: ConfigParam;
}

const getTokenNaver = (androidKeys: ConfigParam, iosKeys: ConfigParam) => {
  const initials = Platform.OS === 'ios' ? iosKeys : androidKeys;

  print(initials);
  return new Promise(async (resolve, reject) => {
    NaverLogin.login(initials, (err, token: any) => {
      if (err) {
        console.log('errr la gi', err);
        reject(err);
        // Spinner.hide();
        return;
      } else {
        resolve(token.accessToken);
      }
    });
  });
};

export async function naverLogin<Type>(): Promise<Type> {
  const config = await read(SOCIAL_CONFIG);
  const token = await getTokenNaver(
    config.naver.androidConfig,
    config.naver.iosConfig
  );
  write(LOGIN_METHOD, LoginMethod.NAVER);
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST',
      path: `/auth/login_with_naver`,
      body: {
        naverToken: token,
      },
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          // const token = res.data.results.token;
          // print("@got token", token);
          // write(TOKEN, token);
          // write(USER, res.data.results.object);
          accept(res.data.results);
        } else {
          reject(res.data);
        }
      })
      .catch((err: any) => {
        print('@err', err);
        reject(err);
      });
  });
}

export function signOutNaver() {
  NaverLogin.logout();
}
