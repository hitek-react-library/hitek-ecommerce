import { LoginMethod, LOGIN_METHOD } from '../utils/constants';
import { fetch } from '../utils/fetch';
import { print } from '../utils/log';
import { write } from './local_service';

// import {FBLoginManager} from 'react-native-facebook-login';
import { AccessToken, LoginManager } from 'react-native-fbsdk';



const getFacebookToken = () => {
  return new Promise(async (accept, reject) => {
    const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
    if (result.isCancelled) {
      reject('cancel');
    } else {
      const response = await AccessToken.getCurrentAccessToken();
      accept(response?.accessToken);
    }
  });
};

export async function facebookLogin<Type>(): Promise<Type> {
  const token = await getFacebookToken();
  print('@fbtoken', token);
  write(LOGIN_METHOD, LoginMethod.FACEBOOK);
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST',
      path: `/auth/login_with_facebook`,
      body: {
        facebookToken: token,
      },
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          // const token = res.data.results.token;
          // print('@got token', token);
          // write(TOKEN, token);
          // write(USER, res.data.results.object);
          accept(res.data.results);
        } else {
          reject(res.data);
        }
      })
      .catch((err) => {
        print('@err', err);
        reject(err);
      });
  });
}

export function signOutFacebook() {
  LoginManager.logOut();
}
