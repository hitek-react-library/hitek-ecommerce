import {
  KakaoOAuthToken,
  // KakaoProfile,
  // getProfile as getKakaoProfile,
  login,
  logout,
  // unlink,
} from '@react-native-seoul/kakao-login';
import { LoginMethod, LOGIN_METHOD } from '../utils/constants';
import { fetch } from '../utils/fetch';
import { print } from '../utils/log';
import { write } from './local_service';

const getKakaoToken = () => {
  return new Promise(async (accept, reject) => {
    try {
      const response: KakaoOAuthToken = await login();
      accept(response.accessToken);
    } catch (e) {
      reject(e);
    }
  });
};

export async function kakaoLogin<Type>(): Promise<Type> {
  const token = await getKakaoToken();
  print('@kakao', token);
  write(LOGIN_METHOD, LoginMethod.KAKAO);
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST',
      path: `/auth/login_with_kakaotalk`,
      body: {
        kakaotalkToken: token,
      },
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          // const token = res.data.results.token;
          // print('@got token', token);
          // write(TOKEN, token);
          // write(USER, res.data.results.object);
          accept(res.data.results);
        } else {
          reject(res.data);
        }
      })
      .catch((err) => {
        print('@err', err);
        reject(err);
      });
  });
}

export function signOutKakao() {
  logout();
}
