import { appleAuth } from '@invertase/react-native-apple-authentication';
import { LoginMethod, LOGIN_METHOD } from '../utils/constants';
import { fetch } from '../utils/fetch';
import { print } from '../utils/log';
import { write } from './local_service';

export async function appleLogin<Type>(): Promise<Type> {
  const token = await getAppleToken();
  print('@apple', token);
  write(LOGIN_METHOD, LoginMethod.APPLE);
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST',
      path: `/auth/login_with_apple`,
      body: {
        appleToken: token,
      },
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          // const token = res.data.results.token;
          // print('@got token', token);
          // write(TOKEN, token);
          // write(USER, res.data.results.object);
          accept(res.data.results);
        } else {
          reject(res.data);
        }
      })
      .catch((err) => {
        print('@err', err);
        reject(err);
      });
  });
}

export function signOutApple() {
// Don't support by library
//   appleAuth.logout();
}

const getAppleToken = () => {
  return new Promise(async (accept, reject) => {
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });

      // get current authentication state for user
      // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
      const credentialState = await appleAuth.getCredentialStateForUser(
        appleAuthRequestResponse.user
      );

      // use credentialState response to ensure the user is authenticated
      if (credentialState === appleAuth.State.AUTHORIZED) {
        // user is authenticated
        print(appleAuthRequestResponse);
        accept(appleAuthRequestResponse.identityToken);
      } else {
          reject(appleAuth.State.toString());
      }
      
    } catch (e) {
      reject(e);
    }
  });
};