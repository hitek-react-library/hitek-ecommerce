import { print } from '../utils/log';
import { fetch } from '../utils/fetch';
import { LOGIN_METHOD, TOKEN, USER } from '../utils/constants';
import { read, write } from './local_service';

export async function register<Type>(body: any): Promise<Type> {
  // Todo should hash password
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST',
      path: `/auth/register`,
      body: body,
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          accept(res.data.results.object);
        } else {
          reject(res.data);
        }
      })
      .catch((err) => {
        print('@err', err);
        reject(err);
      });
  });
}

export async function emailPasswordLogin<Type>(
  email: string,
  password: string
): Promise<Type> {
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST',
      path: `/auth/login`,
      body: {
        username: email,
        password,
      },
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          // const token = res.data.results.object.token;
          // print('@got token', token);
          // write(TOKEN, token);
          // write(LOGIN_METHOD, LoginMethod.EMAIL_AND_PASSWORD);
          accept(res.data.results.object);
        } else {
          reject(res.data);
        }
      })
      .catch((err) => {
        print('@err', err);
        reject(err);
      });
  });
}

export async function signOut() {
  // const method = (await read(LOGIN_METHOD)) ?? '-1';
  // print(method);
  // switch (parseInt(method)) {
  //   case LoginMethod.GOOGLE:
  //     signOutGoogle();
  //     break;

  //   case LoginMethod.FACEBOOK:
  //     signOutFacebook();
  //     break;

  //   case LoginMethod.NAVER:
  //     signOutNaver();
  //     break;

  //   case LoginMethod.KAKAO:
  //     signOutKakao();
  //     break;

  //   case LoginMethod.APPLE:
  //     signOutKakao();
  //     break;
  // }

  write(LOGIN_METHOD, '');
  write(TOKEN, '');
  write(USER, '');
}

export function getToken() {
  return read(TOKEN);
}

export function getUser() {
  return read(USER);
}
