import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { BASE_URL2, IS_ANDROID } from '~constants/constants';
import { CategoryProps, ProductProps } from '~constants/types';

export const apiProductServices = createApi({
  reducerPath: 'productApi',
  baseQuery: fetchBaseQuery({ baseUrl: BASE_URL2 }),
  tagTypes: ['Category', 'Product'],
  refetchOnMountOrArgChange: true,
  refetchOnFocus: true,
  refetchOnReconnect: true,
  endpoints: (builder) => ({
    listCategory: builder.query<CategoryProps[], number | void>({
      query: (page = 1) => `category?fields=["$all"]&page=${page}&order=[["order","asc"]]`,
      transformResponse: (response: { results: { objects: { rows: CategoryProps[] } } }) => {
        return response.results.objects.rows;
      },
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ id }) => ({ type: 'Category' as const, id })),
              { type: 'Category', id: 'LIST' },
            ]
          : [{ type: 'Category', id: 'LIST' }],
    }),
    listProductByCategory: builder.query<
      ProductProps[],
      Partial<{ category_id: string; txt_search?: string; page?: number }>
    >({
      query: ({ category_id, page = 1, txt_search = '' }) => {
        const fixAPI = IS_ANDROID ? `"%25${txt_search}%25"` : `"%${txt_search}%"`;
        return {
          url: txt_search
            ? `product?fields=["$all"]&page=${page}&order=[["created_at_unix_timestamp","desc"]]&filter={"category_id":"${category_id}","title": {"$iLike": ${fixAPI}}}`
            : `product?fields=["$all"]&page=${page}&order=[["created_at_unix_timestamp","desc"]]&filter={"category_id":"${category_id}"}`,
        };
      },
      transformResponse: (response: { results: { objects: { rows: ProductProps[] } } }) => {
        return response.results.objects.rows;
      },
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ id }) => ({ type: 'Product' as const, id })),
              { type: 'Product', id: 'LIST' },
            ]
          : [{ type: 'Product', id: 'LIST' }],
    }),
    listProductBanner: builder.query<ProductProps[], number | void>({
      query: () => `product?fields=["$all"]&limit=10&order=[["created_at_unix_timestamp","desc"]]`,
      transformResponse: (response: { results: { objects: { rows: ProductProps[] } } }) => {
        return response.results.objects.rows;
      },
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ id }) => ({ type: 'Product' as const, id })),
              { type: 'Product', id: 'LIST' },
            ]
          : [{ type: 'Product', id: 'LIST' }],
    }),
    findOneProduct: builder.query<ProductProps, string>({
      query: (product_id) => `product/${product_id}?fields=["$all"]`,
      transformResponse: (response: { results: { object: ProductProps } }) => {
        return response.results.object;
      },
    }),
  }),
});

export const {
  usePrefetch,
  useListCategoryQuery,
  useListProductByCategoryQuery,
  useListProductBannerQuery,
  useFindOneProductQuery,
} = apiProductServices;
