import axios, { AxiosError, AxiosResponse, Method } from 'axios';
import { read } from '../services/local_service';
import { HOST } from './constants';
import { print } from './log';
import {
  getItem as getToken,
} from '../base/storage';


interface QueryParam {
    method: Method;
    path: String;
    body?: Object;
    query?: Object;
}
export const fetch = async (data: QueryParam) => {

  const token = await getToken();  
  const host = await read(HOST);
  print('@host', host);
  const fullURL = `${host}${data.path}`;
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`,
  };
  print('>>>', data.method, fullURL);
  print('@header', headers);
  print('@query', data.query);
  print('@body', data.body);

  return new Promise((resolve, reject) => {
    axios({
      method: data.method,
      url: fullURL,
      data: !data.body || Object.keys(data.body).length === 0 ? undefined : data.body,
      params: !data.query || Object.keys(data.query).length === 0 ? undefined : data.query,
      headers,
    })
      .then((res: AxiosResponse) => {
        print('<<<', data.method, fullURL);
        print('Result', res.data);
        resolve(res);
      })
      .catch((err: AxiosError) => {
        print('<<<', data.method, fullURL);
        print('Error', err);
        reject(err);
      });
  });
};

export const uploadImage = async (formData: FormData, size: number) => {
  const token = await getToken();  
  const host = await read(HOST);
  return new Promise((resolve, reject) => {
    const headers = {
      'Content-Type': 'image/png',
      'Authorization': `Bearer ${token}`,
    };
    axios({
      method: 'POST',
      url: `${host}/image/upload/${size}`,
      data: formData,
      headers,
    })
      .then((res: any) => {
        resolve(res);
      })
      .catch((err: any) => {
        console.log('error bug', err);
        reject(err);
      });
  });
};
