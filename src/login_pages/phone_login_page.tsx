import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  TextInput,
} from 'react-native';
import React from 'react';
import { PAGE_NAME } from '../page_names';

export const PhoneLoginPage = (props: any) => {
  const { navigation } = props;
  const [phone, setPhone] = React.useState('');

  const onDone = async () => {
    // Spinner.show();
    let p = phone;
    if (p.startsWith('0')) p = '+84' + phone.substring(1);
    navigation.replace(PAGE_NAME.HVerifyCodePage);
    // Spinner.hide();
  };

  return (
    <SafeAreaView style={{ flex: 1, justifyContent: 'center' }}>
      <View style={{ alignSelf: 'center' }}>
        <Text>Phone number</Text>
        <TextInput
          style={{
            backgroundColor: '#e6e6e6',
            fontSize: 24,
            minWidth: 300,
            marginVertical: 24,
            alignSelf: 'center',
            paddingHorizontal: 12,
          }}
          maxLength={12}
          keyboardType="numeric"
          onChangeText={(code) => setPhone(code)}
        />
        <TouchableOpacity style={{ alignItems: 'center' }} onPress={onDone}>
          <Text>Login</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
