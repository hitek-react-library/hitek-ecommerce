import { Text, TouchableOpacity } from 'react-native';
import React from 'react';

interface CustomButtonProp {
  text: string;
  onPressed?: Function;
}

export const CustomButton: React.FC<CustomButtonProp> = ({
  text,
  onPressed,
}) => {
  return (
    <TouchableOpacity onPress={() => (onPressed ? onPressed() : null)}>
      <Text
        style={{
          textAlign: 'center',
          padding: 14,
          marginVertical: 8,
          borderColor: 'blue',
          borderRadius: 12,
          borderWidth: 1,
          color: 'blue',
        }}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};


export const NoBorderButton: React.FC<CustomButtonProp> = ({
  text,
  onPressed,
}) => {
  return (
    <TouchableOpacity onPress={() => (onPressed ? onPressed() : null)}>
      <Text
        style={{
          textAlign: 'center',
          padding: 14,
          marginVertical: 8,
          color: 'blue',
        }}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};
