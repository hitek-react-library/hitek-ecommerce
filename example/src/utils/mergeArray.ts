export function mergeArray(...toMerge: any[]) {
  let output = {};
  toMerge.forEach((arr) => {
    arr.forEach((item) => {
      output[item.id] = item;
    });
  });
  return Object.values(output);
}
