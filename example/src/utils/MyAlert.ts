import { Alert } from 'react-native';

export const myAlert = (title = '', detail = '', func = () => {}, text = 'Đóng') => {
  setTimeout(() => {
    Alert.alert(
      title,
      detail,
      [
        {
          text,
          onPress: () => {
            func();
          },
        },
      ],
      { cancelable: false }
    );
  }, 300);
};
export const myAlertYesNo = (
  title = '',
  detail = '',
  onYes = () => {},
  textYes = 'Đồng ý',
  textNo = 'Không',
  onNo = () => {}
) => {
  setTimeout(() => {
    Alert.alert(
      title,
      detail,
      [
        {
          text: textYes,
          onPress: () => {
            onYes();
          },
          style: 'cancel',
        },
        {
          text: textNo,
          onPress: () => {
            onNo();
          },
        },
      ],
      { cancelable: false }
    );
  }, 300);
};
