import { ActivityOrder, OrderActivitys } from '~constants/types';

export const getStatusOrder = (activity: OrderActivitys[]) => {
  if (activity && activity.length) {
    const order_activitys = activity
      .slice()
      ?.sort(
        (a, b) => parseInt(b.created_at_unix_timestamp) - parseInt(a.created_at_unix_timestamp)
      )[0];
    switch (order_activitys.activity) {
      case ActivityOrder.PROCESSING:
        return {
          status: ActivityOrder.PROCESSING,
          text: 'Đang xử lí',
          color: '#F4BA40',
          icon: 'clock-time-three',
        };
      case ActivityOrder.CONFIRMED:
        return {
          status: ActivityOrder.CONFIRMED,
          text: 'Đã xác nhận',
          color: '#1DAB9A',
          icon: 'moped',
        };
      case ActivityOrder.COMPLETED:
        return {
          status: ActivityOrder.COMPLETED,
          text: 'Hoàn thành',
          color: '#1DAB9A',
          icon: 'check-decagram',
        };
      case ActivityOrder.CANCELLED:
        return {
          status: ActivityOrder.CANCELLED,
          text: 'Đã hủy',
          color: '#DC143C',
          icon: 'close-octagon',
        };
      default:
        return {
          status: ActivityOrder.PROCESSING,
          text: 'Đang xử lí',
          color: '#F4BA40',
          icon: 'clock-time-three',
        };
    }
  }
  return {
    status: ActivityOrder.PROCESSING,
    text: 'Đang xử lí',
    color: '#F4BA40',
    icon: 'clock-time-three',
  };
};
