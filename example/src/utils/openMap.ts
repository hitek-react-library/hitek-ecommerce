import { Linking, Platform } from 'react-native';

export const openMap = (latitude: number, longitude: number) => {
  const daddr = `${latitude},${longitude}`;
  const company = Platform.OS === 'ios' ? 'apple' : 'google';
  Linking.openURL(`http://maps.${company}.com/maps?daddr=${daddr}`);
};
