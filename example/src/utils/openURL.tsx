import { Linking } from 'react-native';
import { myAlert } from './MyAlert';

export const openURL = async (url: string) => {
  Linking.openURL(url).catch((err) => {
    myAlert('không mở được bro');
    console.error('openURL -> An error occurred ', err);
  });
};
