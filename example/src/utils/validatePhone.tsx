export const validatePhone = (phone: number | string) => {
  let phoneNumber = phone.toString();
  if (phoneNumber.indexOf('+84') === 0) {
    phoneNumber = phoneNumber.replace('+84', '0');
  }
  var regexp = /^[0]{1}[0-9]{2}([0-9]{3}|[0-9]{4})[0-9]{4}$/gm;
  return regexp?.test(phoneNumber?.toString());
};
