import { StyleProp, ViewStyle } from 'react-native';
import { EdgeInsets } from 'react-native-maps';
import { myAlert } from './MyAlert';

export const myMessageDEV = () => myAlert('Chức năng đang phát triển');

export const myMessageErrorOrder = ({
  message = 'Có lỗi xãy ra khi đặt hàng',
}: {
  message?: string;
  description?: string;
  insets: EdgeInsets;
  style?: StyleProp<ViewStyle>;
}) => myAlert(message);
