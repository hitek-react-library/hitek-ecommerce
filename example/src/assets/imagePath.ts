const images = '../assets/images';
const icons = '../assets/icons';

const ICON = {
  ic_news: require(`${icons}/ic_news.png`),
  ic_noti_order: require(`${icons}/ic_noti_order.png`),
  user: require(`${icons}/user.png`),
  back: require(`${icons}/back.png`),
  search: require(`${icons}/search.png`),
  location: require(`${icons}/location.png`),
  up: require(`${icons}/up.png`),
  down: require(`${icons}/down.png`),
  ginseng: require(`${icons}/ginseng.png`),
  close: require(`${icons}/close.png`),
  heart: require(`${icons}/heart.png`),
  heartFull: require(`${icons}/heartFull.png`),
  grid: require(`${icons}/grid.png`),
  list: require(`${icons}/list.png`),
  right: require(`${icons}/right.png`),
};

const IMAGE = {
  logo: require(`${images}/logo.png`),
  logoWhite: require(`${images}/bootsplash_logo.png`),
  logo2: require(`${images}/logo2.png`),
  splash: require(`${images}/splash.jpg`),
  bootsplash_logo: require(`${images}/bootsplash_logo.png`),
  template_logo: require(`${images}/template_logo.png`),
};

export { IMAGE, ICON };

