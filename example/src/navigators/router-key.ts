function createEnum<T extends { [P in keyof T]: P }>(o: T) {
  return o;
}
export const ROUTE_KEY = createEnum({
  // typed as {  Home: 'Home' }
  Drawer: 'Drawer',
  Splash: 'Splash',
  Home: 'Home',
  Details: 'Details',
  Profile: 'Profile',
  One: 'One',
  Two: 'Two',
  Category: 'Category',
  ListProduct: 'ListProduct',
  MyWebView: 'MyWebView',
  BottomTab: 'BottomTab',
  SignIn: 'SignIn',
  Cart: 'Cart',
  Payment: 'Payment',
  DeliveryAddress: 'DeliveryAddress',
  CreateDeliveryAddress: 'CreateDeliveryAddress',
  SelectRegion: 'SelectRegion',
  MyOrder: 'MyOrder',
  InfoOrder: 'InfoOrder',
  StatisticWithdrawMoney: 'StatisticWithdrawMoney',
  NewsMore: 'NewsMore',
  PaymentSuccess: 'PaymentSuccess',
  DetailPost: 'DetailPost',
});
