import { DrawerNavigationProp } from '@react-navigation/drawer';
import { MaterialBottomTabNavigationProp } from '@react-navigation/material-bottom-tabs';
import {
  CompositeNavigationProp,
  RouteProp,
  useFocusEffect,
  useNavigation,
} from '@react-navigation/native';
import React, { useCallback, useEffect } from 'react';
import { Platform, StyleSheet, TouchableOpacity } from 'react-native';
import { useTheme } from 'react-native-paper';
import { NativeStackNavigationProp } from 'react-native-screens/native-stack';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { ModalContext } from '~components/modalContext';
import { createMyNavigator } from '~components/react-native-curved-bottom-bar/src/components/CurvedBottomBar/components/navigator';
import { UserProps } from '~constants/types';
import useIsDarkTheme from '~hooks/use-is-dark-theme';
import { DrawerScreensParams } from '~navigators/drawer';
import { RootStackScreensParams } from '~navigators/root-stack';
import { ROUTE_KEY } from '~navigators/router-key';
import { setVoucher } from '~reducers/userReducer';
import Profile from '~screens/profile';
import { apiAddressServices } from '~services/api_address_services';
import { apiFavoriteServices } from '~services/api_favorite_services';
import { apiOrderServices } from '~services/api_order_services';
import { useAppDispatch, useAppSelector } from '~store/storeHooks';
import Category from '../screens/category';
const queryString = require('query-string');

export type BottomTabScreensParams = {
  Home: undefined;
  Profile: undefined;
};

export type BottomTabScreens = keyof BottomTabScreensParams;

export interface BottomTabScreenProps<T extends BottomTabScreens> {
  navigation: CompositeNavigationProp<
    MaterialBottomTabNavigationProp<BottomTabScreensParams, T>,
    CompositeNavigationProp<
      DrawerNavigationProp<DrawerScreensParams>,
      NativeStackNavigationProp<RootStackScreensParams>
    >
  >;
  route: RouteProp<BottomTabScreensParams, T>;
}

const { Navigator, Screen } = createMyNavigator();

const BottomTab = () => {
  const { colors, dark } = useTheme();
  const [isDark] = useIsDarkTheme();
  const navigation = useNavigation();
  const dispatch = useAppDispatch();
  const userReducer = useAppSelector(
    (state) => state.userReducer.user
  ) as UserProps;
  const { refetch: refetchUpdateDefaultAddress } =
    apiAddressServices.endpoints.getDefaultAddress.useQuerySubscription({});
  const { refetch: refetchUpdateGetListFavorite } =
    apiFavoriteServices.endpoints.getListFavorite.useQuerySubscription({});

  useEffect(() => {
    refetchUpdateDefaultAddress();
    refetchUpdateGetListFavorite();
  }, []);
  useFocusEffect(useCallback(() => {}, [isDark]));

  const onPressCenterButton = () => {
    navigation.navigate(ROUTE_KEY.Category);
  };
  return (
    <Navigator
      bgColor={colors.primary}
      iconColor={dark ? colors.accent : colors.surface}
      renderCircle={() => (
        <TouchableOpacity
          activeOpacity={0.9}
          style={[
            styles.btnCircle,
            { backgroundColor: dark ? colors.accent : colors.accent },
          ]}
          onPress={onPressCenterButton}
        >
          <Ionicons
            name="cart"
            size={26}
            color={dark ? colors.surface : colors.surface}
          />
        </TouchableOpacity>
      )}
      initialRouteName={ROUTE_KEY.Category}
    >
      <Screen
        name={ROUTE_KEY.Category}
        component={Category}
        options={{ tabBarIcon: 'home', position: 'left' }}
      />

      <Screen
        name={ROUTE_KEY.Profile}
        component={Profile}
        options={{ tabBarIcon: 'account', position: 'right' }}
      />
    </Navigator>
  );
};

export default BottomTab;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  btnCircle: {
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 1,
  },
  imgCircle: {
    width: 30,
    height: 30,
    tintColor: '#48CEF6',
  },
  img: {
    width: 30,
    height: 30,
  },
});
