import { NavigatorScreenParams } from '@react-navigation/native';
import React from 'react';
import {
  createNativeStackNavigator,
  NativeStackNavigationProp,
  NativeStackScreenProps,
} from 'react-native-screens/native-stack';
import {
  AddressProps,
  CartProps,
  CategoryProps,
  OrderProps,
  PostProps,
  PostType,
} from '~constants/types';
import BottomTab, { BottomTabScreensParams } from '~navigators/bottom-tab';
import Drawer, { DrawerScreensParams } from '~navigators/drawer';
import { ROUTE_KEY } from '~navigators/router-key';
import Cart from '~screens/cart';
import Category from '~screens/category';
import CreateDeliveryAddress from '~screens/create-delivery-address';
import DeliveryAddress from '~screens/delivery-address';
import DetailPost from '~screens/detail-post';
import InfoOrder from '~screens/info-order';
import ListProduct from '~screens/list-product';
import MyOrder from '~screens/my-order';
import MyWebView from '~screens/my-web-view';
import NewsMore from '~screens/news-more';
import Payment from '~screens/payment';
import PaymentSuccess from '~screens/payment_success';
import SelectRegion from '~screens/select-region';
import Splash from '~screens/splash';
import StatisticWithdrawMoney from '~screens/statistic-withdraw-money';

export type RootStackScreensParams = {
  Splash: undefined | { delay?: number; text?: string };
  PhoneLogin: undefined | {};
  Drawer: undefined | NavigatorScreenParams<DrawerScreensParams>;
  BottomTab: undefined | NavigatorScreenParams<BottomTabScreensParams>;
  Category: undefined | {};
  ListProduct: undefined | { category: CategoryProps };
  Cart: undefined | {};
  Payment: undefined | { cart: CartProps[]; totalPrice: number };
  MyWebView: undefined | { uri?: string; html?: string; title: string };
  StatisticWithdrawMoney: undefined | {};

  DeliveryAddress:
    | undefined
    | {
        onChangeAddress?: (dataAddress?: AddressProps) => void;
        fromProfile?: boolean;
      };
  CreateDeliveryAddress:
    | undefined
    | {
        dataAddress?: AddressProps;
        onChangeAddress?: (dataAddress?: AddressProps) => void;
        firstAddress?: boolean;
      };
  SelectRegion: undefined | {};
  MyOrder: undefined | {};
  InfoOrder: {
    order: OrderProps;
  };
  NewsMore: { title: string; type: PostType };
  PaymentSuccess: undefined | {};
  DetailPost: undefined | PostProps;
};

export type RootStackScreens = keyof RootStackScreensParams;

export type RootStackScreenProps<T extends RootStackScreens> =
  NativeStackScreenProps<RootStackScreensParams, T>;

export type UseRootStackNavigation<T extends RootStackScreens = 'Splash'> =
  NativeStackNavigationProp<RootStackScreensParams, T>;

const { Navigator, Screen } =
  createNativeStackNavigator<RootStackScreensParams>();

const RootStack = () => (
  <Navigator
    screenOptions={{
      headerShown: false,
      stackAnimation: 'fade',
    }}
  >
    <Screen name={ROUTE_KEY.Splash} component={Splash} />
    <Screen name={ROUTE_KEY.Drawer} component={Drawer} />
    <Screen name={ROUTE_KEY.BottomTab} component={BottomTab} />
    <Screen name={ROUTE_KEY.ListProduct} component={ListProduct} />
    <Screen name={ROUTE_KEY.MyWebView} component={MyWebView} />
    <Screen name={ROUTE_KEY.Cart} component={Cart} />
    <Screen name={ROUTE_KEY.Category} component={Category} />

    <Screen
      name={ROUTE_KEY.StatisticWithdrawMoney}
      component={StatisticWithdrawMoney}
    />
    <Screen name={ROUTE_KEY.Payment} component={Payment} />
    <Screen name={ROUTE_KEY.DeliveryAddress} component={DeliveryAddress} />
    <Screen
      name={ROUTE_KEY.CreateDeliveryAddress}
      component={CreateDeliveryAddress}
    />
    <Screen name={ROUTE_KEY.SelectRegion} component={SelectRegion} />
    <Screen name={ROUTE_KEY.MyOrder} component={MyOrder} />
    <Screen name={ROUTE_KEY.InfoOrder} component={InfoOrder} />
    <Screen name={ROUTE_KEY.NewsMore} component={NewsMore} />
    <Screen name={ROUTE_KEY.PaymentSuccess} component={PaymentSuccess} />
    <Screen name={ROUTE_KEY.DetailPost} component={DetailPost} />
  </Navigator>
);

export default RootStack;
