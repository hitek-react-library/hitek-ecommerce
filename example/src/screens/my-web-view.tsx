import React from 'react';
import { Edge } from 'react-native-safe-area-context';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { PScrollView } from '~components/PScrollView';
import PWebView from '~components/PWebView';

const edges: Edge[] = ['right', 'left'];

function MyWebView({ route }: { route: any }) {
  const params = route.params;
  return (
    <FixedContainer edges={edges}>
      <CustomHeader title={params.title} showBackButton />
      <PScrollView style={{ flex: 1 }}>
        <PWebView source={params.html ? { html: params.html } : { uri: params?.uri }} />
      </PScrollView>
    </FixedContainer>
  );
}

export default MyWebView;
