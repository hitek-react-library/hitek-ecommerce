import React, { useEffect, useState } from 'react';
import { Image, View } from 'react-native';
import { Caption, Paragraph, Text, useTheme } from 'react-native-paper';
import { Edge } from 'react-native-safe-area-context';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { PFlatList } from '~components/PFlatList';
import PTouchableOpacity from '~components/PTouchableOpacity';
import { PostProps } from '~constants/types';
import { RootStackScreenProps } from '~navigators/root-stack';
import { ROUTE_KEY } from '~navigators/router-key';
import { useListPostByTypeQuery } from '~services/api_post_services';
import { widthScale } from '~styles/scaling-utils';
import { mergeArray } from '~utils/mergeArray';

const edges: Edge[] = ['right', 'left', 'bottom'];

function NewsMore(props: RootStackScreenProps<'NewsMore'>) {
  const { navigation, route } = props;
  const { colors, dark } = useTheme();
  const { type, title } = route.params;
  const [page, setPage] = useState(1);
  const [_isLoading, setIsLoading] = useState(true);
  const { data, isLoading, isFetching, isSuccess } = useListPostByTypeQuery({ type, page });
  const [dataPostProduct, setDataPost] = useState(data);

  useEffect(() => {
    setIsLoading(true);
    data && setDataPost((oldData) => mergeArray(oldData || [], data) as any);
    let timer = setTimeout(() => {
      setIsLoading(false);
    }, 1500);
    return () => {
      clearInterval(timer);
    };
  }, [data]);
  const _renderItem = ({ item }: { item: PostProps }) => {
    return (
      <PTouchableOpacity
        onPress={() => navigation.navigate(ROUTE_KEY.DetailPost, item)}
        style={{
          flexDirection: 'row',
          paddingHorizontal: widthScale(12),
          paddingBottom: widthScale(4),
        }}
      >
        <Image
          source={{ uri: item.image }}
          style={{
            width: widthScale(55),
            height: widthScale(55),
            marginRight: widthScale(8),
            top: widthScale(6),
          }}
          resizeMode="cover"
        />
        <View style={{ flex: 1 }}>
          <Text style={{ fontWeight: 'bold' }}>{item.title}</Text>
          <Paragraph style={{ color: colors.placeholder }} numberOfLines={2}>
            {item.description}
          </Paragraph>
          <Caption style={{ textAlign: 'right' }}>{item.date}</Caption>
        </View>
      </PTouchableOpacity>
    );
  };
  return (
    <FixedContainer edges={edges}>
      <CustomHeader title={title} showBackButton />
      <PFlatList
        isTabScreen
        data={dataPostProduct}
        renderItem={_renderItem as any}
        onEndReached={() => {
          if (data && data?.length) {
            setPage((page) => page + 1);
          }
        }}
        keyExtractor={(item: any) => item?.id}
        isLoading={_isLoading || isLoading || isFetching || !isSuccess}
        contentContainerStyle={{ paddingTop: widthScale(16) }}
        ItemSeparatorComponent={() => (
          <View
            style={{
              flexDirection: 'row',
              borderBottomWidth: 1,
              borderBottomColor: colors.disabled,
              marginBottom: widthScale(12),
              paddingBottom: widthScale(4),
            }}
          ></View>
        )}
      />
    </FixedContainer>
  );
}

export default NewsMore;
