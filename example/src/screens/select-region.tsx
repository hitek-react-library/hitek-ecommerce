import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { Button, Text, TouchableRipple, useTheme } from 'react-native-paper';
import { Edge, useSafeAreaInsets } from 'react-native-safe-area-context';
import ButtonToggleGroup from '~components/button-toggle-group';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { PFlatList } from '~components/PFlatList';
import { PRow } from '~components/PRow';
import { DVHCVN } from '~constants/dvhcvn';
import { RootStackScreenProps } from '~navigators/root-stack';
import { heightScale, widthScale } from '~styles/scaling-utils';

const edges: Edge[] = ['right', 'left'];

function SelectRegion(props: RootStackScreenProps<'Cart'>) {
  const insets = useSafeAreaInsets();
  const { navigation, route } = props;

  const params = route.params;

  const { colors, dark } = useTheme();
  const [defaultAddress, setDefaultAddress] = useState<string | number>(0);
  const [value, setValue] = React.useState('Chọn Tỉnh/Thành Phố');
  const dataCity = DVHCVN;
  const [dataDistrict, setDataDistrict] = React.useState<any>();
  const [dataWards, setDataWards] = React.useState<any>();
  const [city, setCity] = React.useState<any>(null);
  const [district, setDistrict] = React.useState<any>(null);
  const [ward, setWard] = React.useState<any>(null);

  useEffect(() => {
    if (value === 'Chọn Quận/Huyện' && city && dataCity) {
      const filterDistrict = dataCity?.filter((v) => v.level1_id === city?.level1_id)[0]?.level2s;
      setDataDistrict(filterDistrict);
    }
    if (value === 'Chọn Phường/Xã' && district && dataDistrict) {
      const filterWards = dataDistrict?.filter((v: any) => v.level2_id === district?.level2_id)[0]
        ?.level3s;
      setDataWards(filterWards);
    }
  }, [value]);
  useEffect(() => {
    let tempCity: any = null;
    let tempDistrict: any = null;
    let tempWard: any = null;

    if (params && params.data && params.data.city && params.data.district && params.data.ward) {
      tempCity = dataCity.find((city) => city.name === params.data.city);
      tempDistrict = tempCity.level2s.find((city) => city.name === params.data.district);
      tempWard = tempDistrict.level3s.find((city) => city.name === params.data.ward);
      // console.log(tempCity, tempDistrict, tempWard);
    }
    setCity(tempCity);
    setDistrict(tempDistrict);
    setWard(tempWard);
  }, []);
  const onDone = () => {};
  const renderItem = ({ item }) => {
    return (
      <TouchableRipple
        onPress={() => {
          if (value === 'Chọn Tỉnh/Thành Phố') {
            setCity({ level1_id: item.level1_id, name: item.name, type: item.type });
            setDistrict(null);
            setValue('Chọn Quận/Huyện');
          }
          if (value === 'Chọn Quận/Huyện') {
            setDistrict({ level2_id: item.level2_id, name: item.name, type: item.type });
            setWard(null);
            setValue('Chọn Phường/Xã');
          }
          if (value === 'Chọn Phường/Xã') {
            setWard({ level3_id: item.level3_id, name: item.name, type: item.type });
            onDone();
          }
        }}
        style={{
          borderBottomWidth: widthScale(2),
          borderBottomColor: colors.background2,
          paddingVertical: widthScale(14),
          paddingHorizontal: widthScale(8),
        }}
      >
        <Text>{item?.name}</Text>
      </TouchableRipple>
    );
  };
  const renderKeyExtractor = (item: any) => item?.name;
  return (
    <FixedContainer edges={edges} style={{ flex: 1, backgroundColor: colors.background }}>
      <CustomHeader
        iconsLeftMenu="keyboard-backspace"
        onLeftMenuPress={() => {
          setCity(null);
          setDistrict(null);
          setWard(null);
          setTimeout(() => {
            navigation.goBack();
          }, 100);
        }}
        title="Chọn khu vực của bạn"
      />
      <View style={{ flex: 1, backgroundColor: colors.background2 }}>
        <View style={{ flex: 1, backgroundColor: colors.background }}>
          <PRow
            style={{
              justifyContent: 'space-between',
              paddingVertical: widthScale(16),
              paddingHorizontal: widthScale(12),
            }}
          >
            <Text style={{}}>Khu vực được chọn</Text>
            <Text
              style={{ color: colors.accent }}
              onPress={() => {
                setCity(null);
                setDistrict(null);
                setWard(null);
                setValue('Chọn Tỉnh/Thành Phố');
              }}
            >
              Thiết lập lại
            </Text>
          </PRow>
          {city ? (
            <ButtonToggleGroup
              style={{ marginHorizontal: widthScale(12) }}
              heightItem={50}
              highlightBackgroundColor={'white'}
              highlightTextColor={colors.accent}
              inactiveBackgroundColor={'transparent'}
              inactiveTextColor={colors.text}
              values={
                district
                  ? ['Chọn Tỉnh/Thành Phố', 'Chọn Quận/Huyện', 'Chọn Phường/Xã']
                  : ['Chọn Tỉnh/Thành Phố', 'Chọn Quận/Huyện']
              }
              valuesShow={[city?.name, district?.name, ward?.name]}
              value={value}
              onSelect={(val) => setValue(val)}
            />
          ) : null}
          <Text
            style={{
              paddingVertical: widthScale(16),
              paddingHorizontal: widthScale(12),
              backgroundColor: colors.background2,
            }}
          >
            {value}
          </Text>
          <PFlatList
            data={
              value === 'Chọn Phường/Xã'
                ? dataWards
                : value === 'Chọn Quận/Huyện'
                ? dataDistrict
                : dataCity
            }
            showsVerticalScrollIndicator={false}
            renderItem={renderItem as any}
            ListFooterComponent={() => {
              return <View style={{ height: heightScale(200) }}></View>;
            }}
            keyExtractor={renderKeyExtractor}
            style={{ marginBottom: heightScale(50) }}
          />
        </View>
      </View>
      <Button
        mode="contained"
        style={{
          borderRadius: 0,
          backgroundColor: colors.accent,
          paddingBottom: insets.bottom / 2,
        }}
        disabled={!city?.name || !district?.name || !ward?.name}
        color={colors.surface}
        contentStyle={{ height: widthScale(60) }}
        onPress={() => {
          route.params?.onChangeLocation({
            city: city.name,
            district: district.name,
            ward: ward.name,
          });
          setCity(null);
          setDistrict(null);
          setWard(null);
          setTimeout(() => {
            navigation.goBack();
          }, 100);
        }}
      >
        Xác nhận
      </Button>
    </FixedContainer>
  );
}

export default SelectRegion;
