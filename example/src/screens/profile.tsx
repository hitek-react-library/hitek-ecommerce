import React, { useEffect, useState } from 'react';
import {
  DeviceEventEmitter,
  Image,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { ActivityIndicator, Avatar, useTheme } from 'react-native-paper';
import { Edge, useSafeAreaInsets } from 'react-native-safe-area-context';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { ICON, IMAGE } from '~/assets/imagePath';
import FixedContainer from '~components/fixed-container';
import { PScrollView } from '~components/PScrollView';
import PTouchableOpacity from '~components/PTouchableOpacity';
import { BottomTabScreenProps } from '~navigators/bottom-tab';
import { ROUTE_KEY } from '~navigators/router-key';
import { useGetProfileQuery } from '~services/api_user_services';
import { getAppVersion } from '~services/version';
import { useAppSelector } from '~store/storeHooks';
import { pColor } from '~styles/colors';
import { heightScale, screenWidth, widthScale } from '~styles/scaling-utils';
import { pShadow, pStyle } from '~styles/style';
import { pText } from '~styles/typography';

const edges: Edge[] = ['right', 'left'];

const Profile = (props: BottomTabScreenProps<'Profile'>) => {
  const { colors, dark } = useTheme();
  const { navigation } = props;
  const { user } = useAppSelector((state) => state.userReducer);
  const { refetch, isFetching, isLoading } = useGetProfileQuery(undefined, {});
  const insets = useSafeAreaInsets();
  const [version, setVersion] = useState('');
  useEffect(() => {
    getAppVersion().then((v) => setVersion(v));
    DeviceEventEmitter.addListener('UPDATE_PROFILE', () => {
      refetch();
    });
  }, []);

  return (
    <FixedContainer edges={edges} style={{ alignItems: 'center' }}>
      <Image source={IMAGE.splash} style={styles.imageHeader} />
      {isFetching || isLoading ? (
        <ActivityIndicator
          size={widthScale(22)}
          color={'grey'}
          style={{
            position: 'absolute',
            top: insets.top,
            right: widthScale(20),
          }}
        />
      ) : (
        <MaterialCommunityIcons
          name={'refresh'}
          size={widthScale(26)}
          color={'grey'}
          onPress={refetch}
          style={{
            position: 'absolute',
            top: insets.top,
            right: widthScale(20),
          }}
        />
      )}

      <View style={[styles.headerContainer]}>
        <View style={styles.avatarContainer}>
          <Avatar.Image
            source={user?.avatar ? { uri: user.avatar } : IMAGE.logo2}
            size={widthScale(80)}
          />
        </View>
        <View style={styles.nameContainer}>
          <Text style={styles.name}>{user?.fullname ?? 'Guest'}</Text>
        </View>
      </View>
      <View style={styles.bodyContainer}>
        <PScrollView isTabScreen>
          <Item
            icon="clipboard-list"
            name={'Đơn hàng của tôi'}
            onPress={() => navigation.navigate(ROUTE_KEY.MyOrder)}
          />
          <Item
            icon="map-marker"
            name={'Địa chỉ'}
            onPress={() =>
              navigation.navigate(ROUTE_KEY.DeliveryAddress, {
                fromProfile: true,
              })
            }
          />

          <Text style={{ alignSelf: 'center', marginTop: widthScale(8) }}>
            {version}
          </Text>
        </PScrollView>
      </View>
    </FixedContainer>
  );
};

export default Profile;

interface Item {
  icon: string;
  name: string;
  onPress?: () => void;
}

const Item = ({ icon, name, onPress }: Item) => {
  return (
    <PTouchableOpacity onPress={onPress} style={styles.itemContainer}>
      <View style={styles.nameItemContainer}>
        <MaterialCommunityIcons name={icon} size={widthScale(22)} />
        <Text style={styles.nameItem}>{name}</Text>
      </View>
      <Image source={ICON.right} style={styles.iconRight} />
    </PTouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageHeader: {
    width: screenWidth(),
    height: heightScale(150),
  },
  headerContainer: {
    width: screenWidth(0.9),
    top: -heightScale(80),
    ...pShadow.BLUR0,
    borderRadius: widthScale(4),
    alignItems: 'center',
    backgroundColor: pColor.white,
    paddingBottom: widthScale(12),
  },
  avatarContainer: {
    width: widthScale(100),
    height: widthScale(100),
    borderRadius: widthScale(50),
    backgroundColor: pColor.white,
    top: -widthScale(50),
    ...pStyle.center,
    position: 'absolute',
  },
  name: {
    ...pText.H3,
    fontWeight: 'bold',
  },
  nameContainer: {
    marginTop: widthScale(55),
    alignItems: 'center',
  },
  bodyContainer: {
    flex: 1,
    width: screenWidth(0.9),
    top: -heightScale(60),
    ...pShadow.BLUR0,
  },
  itemContainer: {
    height: widthScale(60),
    width: '100%',
    paddingHorizontal: widthScale(20),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  nameItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  nameItem: {
    fontWeight: '600',
    ...pText.H4,
    marginLeft: widthScale(12),
  },
  iconRight: {
    width: widthScale(15),
    height: widthScale(15),
  },
});
