import React from 'react';
import { DeviceEventEmitter, Image, StyleSheet, View } from 'react-native';
import { Button, Headline, useTheme } from 'react-native-paper';
import { IMAGE } from '~/assets/imagePath';
import FixedContainer from '~components/fixed-container';
import { RootStackScreenProps } from '~navigators/root-stack';
import { ROUTE_KEY } from '~navigators/router-key';
import { widthScale, windowWidth } from '~styles/scaling-utils';

const PaymentSuccess = (props: RootStackScreenProps<'PaymentSuccess'>) => {
  const { navigation } = props;
  const { colors } = useTheme();
  return (
    <FixedContainer edges={['right', 'left', 'bottom']}>
      <View style={styles.center}>
        <View style={[{ flex: 1, alignItems: 'center', justifyContent: 'center' }]}>
          <Image source={IMAGE.logo2} resizeMode="contain" style={{ height: windowWidth(0.3) }} />
          <Headline
            style={[
              {
                textAlign: 'center',
                alignSelf: 'center',
                marginBottom: widthScale(36),
              },
            ]}
          >
            Đặt hàng thành công
          </Headline>
          <Button
            mode="contained"
            onPress={() => {
              navigation.popToTop();
              DeviceEventEmitter.emit('CHANGE_TAB', ROUTE_KEY.Profile);
              navigation.navigate(ROUTE_KEY.MyOrder);
            }}
            style={{
              height: widthScale(36),
              borderRadius: widthScale(36),
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            Xem đơn hàng
          </Button>
          <Button
            onPress={() => {
              navigation.pop(3);
            }}
            style={{
              marginVertical: widthScale(8),
              height: widthScale(36),
              borderRadius: widthScale(36),
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: colors.accent,
            }}
          >
            Tiếp tục mua hàng
          </Button>
        </View>
      </View>
    </FixedContainer>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  path: {
    fontSize: 11,
  },
});

export default PaymentSuccess;
