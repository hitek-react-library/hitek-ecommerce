import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { DeviceEventEmitter, Image, View } from 'react-native';
import { Caption, Subheading, Text, TouchableRipple, useTheme } from 'react-native-paper';
import { Edge } from 'react-native-safe-area-context';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { PFlatList } from '~components/PFlatList';
import { PRow } from '~components/PRow';
import { OrderProps } from '~constants/types';
import { RootStackScreenProps } from '~navigators/root-stack';
import { ROUTE_KEY } from '~navigators/router-key';
import { useListOrderQuery } from '~services/api_order_services';
import { useAppSelector } from '~store/storeHooks';
import { widthScale } from '~styles/scaling-utils';
import { pFont } from '~styles/typography';
import { getNumberString } from '~utils/numberUtils';
import { getStatusOrder } from '~utils/statusOrder';
const edges: Edge[] = ['right', 'left', 'bottom'];

function MyOrder(props: RootStackScreenProps<'MyOrder'>) {
  const { navigation } = props;
  const { colors, dark } = useTheme();
  const [page, setPage] = useState(1);
  const userReducer = useAppSelector((state) => state.userReducer);

  const { data, isLoading, isFetching, isSuccess, refetch } = useListOrderQuery({
    user_id: userReducer.user?.id,
    page,
  });

  const [dataOrder, setDataOrder] = useState(data || []);

  const onRefresh = () => {
    if (page > 1) {
      setDataOrder([]);
      setPage(1);
    }
    refetch();
  };
  useEffect(() => {
    let subscription = DeviceEventEmitter.addListener('UPDATE_ORDER', () => {
      onRefresh();
    });
    return () => {
      subscription.remove();
    };
  }, []);

  useEffect(() => {
    if (page === 1) {
      data && setDataOrder((oldData) => _.uniqBy([...data, ...oldData], 'id'));
    } else {
      data && setDataOrder((oldData) => _.uniqBy([...oldData, ...data], 'id'));
    }
  }, [data]);

  const onEndReached = () => {
    if (data && data?.length) {
      setPage((page) => page + 1);
    }
  };
  const _renderItem = ({ item }: { item: OrderProps }) => {
    if (!item?.order_items[0]) {
      return;
    } else {
      const status = getStatusOrder(item?.order_activitys);
      const orderItems = item?.order_items[0];
      return (
        <TouchableRipple
          onPress={() => navigation.navigate(ROUTE_KEY.InfoOrder, { order: item })}
          style={{ marginBottom: widthScale(10) }}
        >
          <View style={{ backgroundColor: colors.background }}>
            <PRow
              style={{
                marginTop: widthScale(8),
                paddingHorizontal: widthScale(12),
                paddingVertical: widthScale(8),
                justifyContent: 'space-between',
              }}
            >
              <Text style={{}}>Đơn hàng #{item?.id?.slice(0, 8)}</Text>
              <Subheading style={{ color: status.color, fontFamily: pFont.FONT2 }}>
                {status?.text}
              </Subheading>
            </PRow>

            <PRow
              style={{
                alignItems: 'flex-start',
                paddingLeft: widthScale(12),
                paddingVertical: widthScale(10),
                borderBottomWidth: widthScale(1),
                borderBottomColor: colors.background2,
              }}
            >
              <Image
                source={{ uri: orderItems.thumbnail || '' }}
                style={{
                  width: widthScale(80),
                  height: widthScale(80),
                  marginRight: widthScale(8),
                }}
                resizeMode="contain"
              />
              <View style={{ width: '72%', justifyContent: 'space-between' }}>
                <Text style={{ minHeight: widthScale(40) }} numberOfLines={2}>
                  {orderItems.title}
                </Text>
                <PRow style={{ justifyContent: 'space-between' }}>
                  <Caption style={{ alignSelf: 'flex-end' }}>x{orderItems.quantity}</Caption>
                </PRow>
                <PRow style={{ marginLeft: widthScale(4), alignSelf: 'flex-end' }}>
                  <Subheading style={{ fontFamily: pFont.FONT2, color: colors.accent }}>
                    {getNumberString(orderItems?.price)}
                  </Subheading>
                </PRow>
              </View>
            </PRow>

            <PRow
              style={{
                marginTop: widthScale(10),
                paddingHorizontal: widthScale(12),
                paddingVertical: widthScale(8),
                justifyContent: 'space-between',
              }}
            >
              <Caption style={{}}>{item?.order_items?.length} sản phẩm</Caption>
              <PRow>
                <Text style={{ marginRight: widthScale(8) }}>Thành tiền:</Text>
                <Subheading style={{ color: colors.accent, fontFamily: pFont.FONT2, bottom: 1.5 }}>
                  {getNumberString(item?.total_cost)}
                </Subheading>
              </PRow>
            </PRow>
          </View>
        </TouchableRipple>
      );
    }
  };
  return (
    <FixedContainer edges={edges} style={{ backgroundColor: colors.background2 }}>
      <CustomHeader showBackButton title="Đơn mua" />
      <PFlatList
        refreshing={isFetching}
        onRefresh={onRefresh}
        textNull={`"Hông" có đơn hàng nào hết\nLướt Langbiang, lựa hàng ngay nào!`}
        data={dataOrder}
        renderItem={_renderItem as any}
        contentContainerStyle={{ paddingTop: widthScale(16), backgroundColor: colors.background2 }}
        onEndReached={onEndReached}
        isLoading={isLoading || isFetching || !isSuccess}
      />
    </FixedContainer>
  );
}

export default MyOrder;
