import React, { useEffect, useState } from 'react';
import { Alert, Image, View } from 'react-native';
import {
  Button,
  Caption,
  Paragraph,
  Subheading,
  Text,
  useTheme,
} from 'react-native-paper';
import { Edge } from 'react-native-safe-area-context';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { IMAGE } from '~/assets/imagePath';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { PFlatList } from '~components/PFlatList';
import { PRow } from '~components/PRow';
import PTouchableOpacity from '~components/PTouchableOpacity';
import { AddressProps } from '~constants/types';
import { RootStackScreenProps } from '~navigators/root-stack';
import { ROUTE_KEY } from '~navigators/router-key';
import { updateDefaultAddress } from '~reducers/userReducer';
// import { apiAddressServices } from 'hitek-auth';

import {
  useDeleteAddressMutation,
  useGetListAddressQuery,
  useUpdateAddressMutation,
} from '~services/api_address_services';
import { useAppDispatch, useAppSelector } from '~store/storeHooks';
import { widthScale, windowWidth } from '~styles/scaling-utils';
import { mergeArray } from '~utils/mergeArray';

const edges: Edge[] = ['right', 'left', 'bottom'];

function DeliveryAddress(props: RootStackScreenProps<'DeliveryAddress'>) {
  const { navigation, route } = props;
  const params = route.params;
  const { colors, dark } = useTheme();
  const [editAddress, setEditAddress] = useState<boolean>(
    params?.fromProfile || false
  );

  const [page, setPage] = useState(1);
  const userInfo = useAppSelector((state) => state.userReducer.user);

  const { data, isFetching, error } = useGetListAddressQuery(
    { page },
    {
      refetchOnFocus: true,
      refetchOnReconnect: true,
      refetchOnMountOrArgChange: true,
    }
  );

  //Dùng để tạo lib
  // const { data } = apiAddressServices.useGetListAddressQuery(
  //   { page },
  //   {
  //     refetchOnFocus: true,
  //     refetchOnReconnect: true,
  //     refetchOnMountOrArgChange: true,
  //   }
  // );
  const [deleteAddressAPI, { isLoading: isUpdatingAddress }] =
    useDeleteAddressMutation();
  const [updateAddress] = useUpdateAddressMutation();
  const dispatch = useAppDispatch();

  const [defaultAddress, setDefaultAddress] = useState(
    userInfo?.defaultAddress
  );
  const [listAddress, setListAddress] = useState<AddressProps[]>(data || []);
  useEffect(() => {
    if (data) {
      data.length >= 2
        ? setDefaultAddress(userInfo?.defaultAddress)
        : setDefaultAddress(data[0]);
      data && setListAddress((oldData) => mergeArray(oldData, data) as any);
    }
  }, [data]);
  async function deleteAddress(id: string) {
    await deleteAddressAPI({ id });
    Alert.alert('Xóa địa chỉ thành công', '', [
      {
        text: 'Đóng',
        onPress: () => {
          const newList = listAddress.filter((v) => v.id !== id);
          if (newList.length === 1) {
            updateAddress({ id: newList[0].id, body: { is_default: true } });
          } else if (newList.length === 0) {
            dispatch(updateDefaultAddress(undefined));
          }
          setListAddress(newList);
        },
      },
    ]);
  }
  const _renderItem = ({ item }: { item: AddressProps }) => {
    return (
      <PTouchableOpacity
        onPress={() => {
          if (editAddress) {
            navigation.navigate(ROUTE_KEY.CreateDeliveryAddress, {
              dataAddress: item,
            });
          } else {
            params?.onChangeAddress && params?.onChangeAddress(item);
            navigation.goBack();
          }
        }}
        style={{
          backgroundColor: colors.background,
          paddingVertical: widthScale(10),
        }}
      >
        <PRow style={{ paddingHorizontal: widthScale(12) }}>
          <View style={{ flex: 1, marginLeft: widthScale(8) }}>
            <Subheading style={{ fontWeight: 'bold' }}>
              {item?.fullname}
              <Caption style={{ color: colors.accent }}>
                {defaultAddress?.id === item.id && ' [Mặc định]'}
              </Caption>
            </Subheading>
            <Caption style={{}}>{item?.phone}</Caption>
            <Caption style={{}}>{item?.address}</Caption>
            <Caption style={{}}>
              {item?.ward &&
                item.district &&
                item.city &&
                `${item?.ward || ''}, ${item?.district || ''}, ${
                  item?.city || ''
                }`}
            </Caption>
          </View>
          {editAddress ? (
            <PTouchableOpacity
              onPress={() => {
                Alert.alert('Bạn có muốn xóa địa chỉ này không?', '', [
                  {
                    text: 'Không',
                    onPress: () => {},
                  },
                  {
                    text: 'Có',
                    onPress: () => {
                      deleteAddress(item.id);
                    },
                    style: 'cancel',
                  },
                ]);
              }}
              style={{
                marginLeft: widthScale(8),
                alignSelf: 'center',
              }}
            >
              <MaterialCommunityIcons
                name="delete-forever"
                color={colors.error}
                size={widthScale(30)}
                style={{}}
              />
            </PTouchableOpacity>
          ) : (
            <View style={{ marginLeft: widthScale(8), alignSelf: 'flex-end' }}>
              {defaultAddress?.id === item.id && (
                <MaterialCommunityIcons
                  name="check"
                  color={colors.accent}
                  size={widthScale(24)}
                  style={{ position: 'absolute', bottom: '40%' }}
                />
              )}
              <MaterialCommunityIcons
                name="map-marker"
                color={colors.accent}
                size={widthScale(24)}
                style={{}}
              />
            </View>
          )}
        </PRow>
      </PTouchableOpacity>
    );
  };
  return (
    <FixedContainer edges={edges}>
      <CustomHeader
        // showBackButton
        iconsLeftMenu="keyboard-backspace"
        title={editAddress ? 'Sửa địa chỉ giao hàng' : 'Chọn địa chỉ nhận hàng'}
        customRightMenu={
          listAddress.length && !route.params?.fromProfile ? (
            <Button
              labelStyle={{ color: 'white' }}
              onPress={() => setEditAddress(!editAddress)}
            >
              {editAddress ? 'Xong' : 'Sửa'}
            </Button>
          ) : null
        }
        onLeftMenuPress={() => {
          if (listAddress.length > 0) {
            params?.onChangeAddress &&
              params?.onChangeAddress(
                listAddress.find((v) => v.is_default === true)
              );
          } else {
            params?.onChangeAddress && params?.onChangeAddress(undefined);
          }
          navigation.goBack();
        }}
      />
      {listAddress.length ? (
        <PFlatList
          data={listAddress}
          renderItem={_renderItem as any}
          contentContainerStyle={{
            paddingTop: widthScale(16),
            backgroundColor: colors.background2,
          }}
          ItemSeparatorComponent={() => (
            <View
              style={{
                backgroundColor: colors.background2,
                marginBottom: widthScale(12),
                paddingBottom: widthScale(4),
              }}
            ></View>
          )}
          ListFooterComponent={() => (
            <PTouchableOpacity
              onPress={() =>
                navigation.navigate(ROUTE_KEY.CreateDeliveryAddress)
              }
            >
              <PRow
                style={{
                  backgroundColor: colors.background,
                  marginTop: widthScale(10),
                  paddingHorizontal: widthScale(12),
                  paddingVertical: widthScale(8),
                }}
              >
                <PRow
                  style={{
                    flex: 1,
                    marginLeft: widthScale(8),
                    justifyContent: 'space-between',
                  }}
                >
                  <Text style={{}}>Thêm địa chỉ mới</Text>
                  <MaterialCommunityIcons
                    name="plus"
                    color={colors.accent}
                    size={widthScale(24)}
                    style={{ alignSelf: 'flex-start', top: widthScale(4) }}
                  />
                </PRow>
              </PRow>
            </PTouchableOpacity>
          )}
        />
      ) : (
        <PTouchableOpacity
          onPress={() => navigation.navigate(ROUTE_KEY.CreateDeliveryAddress)}
          style={{
            flex: 1,
            backgroundColor: colors.background,
            paddingTop: widthScale(8),
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Image
            source={IMAGE.logo2}
            resizeMode="contain"
            style={{ height: windowWidth(0.3) }}
          />
          <MaterialCommunityIcons
            name="map-marker"
            color={colors.accent}
            size={widthScale(60)}
            style={{}}
          />
          <Text style={{ marginVertical: widthScale(14) }}>
            Ôi! Chúng tôi chưa biết nên giao đến đâu?
          </Text>
          <Paragraph style={{ color: colors.error }}>
            Vui lòng thêm địa chỉ nhận hàng
          </Paragraph>
        </PTouchableOpacity>
      )}
    </FixedContainer>
  );
}

export default DeliveryAddress;
