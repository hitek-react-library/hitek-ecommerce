import React, { useEffect } from 'react';
import { View } from 'react-native';
import RNBootSplash from 'react-native-bootsplash';
import { useTheme } from 'react-native-paper';
import { Edge } from 'react-native-safe-area-context';
import { RootStackScreenProps } from '~navigators/root-stack';
import { ROUTE_KEY } from '~navigators/router-key';

const edges: Edge[] = ['right', 'left'];

const Splash = (props: RootStackScreenProps<'Splash'>) => {
  const { navigation, route } = props;
  const { colors } = useTheme();
  useEffect(() => {
    (async () => {
      try {
        setTimeout(() => {
          navigation.replace(ROUTE_KEY.BottomTab);
          RNBootSplash.hide({ fade: true });
        }, 2000);
      } catch (error) {
        console.log('phat 🚀 ~ file: splash.tsx ~ line 30 ~ error', error);
      }
    })();
  }, [navigation]);

  return <View style={{ flex: 1, backgroundColor: colors.primary }} />;
};

export default Splash;
