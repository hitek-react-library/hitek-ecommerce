import React from 'react';
import { View } from 'react-native';
import { Subheading, Title, useTheme } from 'react-native-paper';
import { Edge } from 'react-native-safe-area-context';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { PFlatList } from '~components/PFlatList';
import { PRow } from '~components/PRow';
import { HEIGHT_SCALE, WIDTH_SCALE } from '~constants/constants';
import { RootStackScreenProps } from '~navigators/root-stack';
import { useGetListStaticWithDrawMoneyQuery } from '~services/api_user_services';
import { pColor } from '~styles/colors';
import { heightScale, widthScale } from '~styles/scaling-utils';
import { pFont, pFontSize } from '~styles/typography';
import { getNumberString } from '~utils/numberUtils';

const edges: Edge[] = ['right', 'left'];
const StatisticWithdrawMoney = (props: RootStackScreenProps<'StatisticWithdrawMoney'>) => {
  const { navigation } = props;
  const { colors, fonts } = useTheme();

  const { data, isFetching, refetch, isLoading, isSuccess } = useGetListStaticWithDrawMoneyQuery();

  const renderItem = ({ item }: { item: any }) => {
    return (
      <PRow
        style={{
          paddingHorizontal: 16 * WIDTH_SCALE,
          paddingVertical: 4 * WIDTH_SCALE,
          flexDirection: 'row',
          justifyContent: 'space-between',
          borderBottomColor: pColor.borderColor,
          borderBottomWidth: 1 * HEIGHT_SCALE,
        }}
      >
        <Subheading style={{}}>Tháng: {item[0]}</Subheading>
        <Subheading style={{ color: colors.accent, fontFamily: pFont.FONT2 }}>
          {getNumberString(item[1])}
        </Subheading>
      </PRow>
    );
  };
  return (
    <FixedContainer edges={edges}>
      <CustomHeader title={`Thông kê thu nhập`} showBackButton />
      <View
        style={{
          width: '100%',
          alignItems: 'center',
          paddingVertical: widthScale(16),
          paddingHorizontal: widthScale(12),
        }}
      >
        <Title style={{}}>Tổng thu nhập hiện tại</Title>
        <Title style={{ color: colors.accent, fontSize: pFontSize(25), ...fonts.font2 }}>
          {getNumberString(data && Object.values(data)?.reduce((a: any, b: any) => a + b, 0))}
        </Title>
      </View>
      <PFlatList
        refreshing={isFetching}
        onRefresh={() => refetch()}
        data={Object.entries(data || {})}
        keyExtractor={(item: any) => item[0] + item[1]}
        renderItem={renderItem}
        contentContainerStyle={{ paddingBottom: heightScale(70) }}
        isLoading={isLoading || isFetching || !isSuccess}
      />
    </FixedContainer>
  );
};

export default StatisticWithdrawMoney;
