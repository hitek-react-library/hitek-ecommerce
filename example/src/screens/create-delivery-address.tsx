import React, { useState } from 'react';
import { Alert, StyleSheet, View } from 'react-native';
import { Button, Switch, Text, TextInput, useTheme } from 'react-native-paper';
import { Edge, useSafeAreaInsets } from 'react-native-safe-area-context';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { PScrollView } from '~components/PScrollView';
import PSpinner from '~components/PSpinner';
import PTouchableOpacity from '~components/PTouchableOpacity';
import { RootStackScreenProps } from '~navigators/root-stack';
import { ROUTE_KEY } from '~navigators/router-key';
import { useAddAddressMutation, useUpdateAddressMutation } from '~services/api_address_services';
import { useAppDispatch, useAppSelector } from '~store/storeHooks';
import { heightScale, widthScale } from '~styles/scaling-utils';
import { pFontSize } from '~styles/typography';
import { myAlert } from '~utils/MyAlert';
import { validateNameVi } from '~utils/validateNameVi';
import { validatePhone } from '~utils/validatePhone';

const edges: Edge[] = ['right', 'left'];

function CreateDeliveryAddress(props: RootStackScreenProps<'CreateDeliveryAddress'>) {
  const { navigation, route } = props;
  const params = route.params;
  const insets = useSafeAreaInsets();

  const { colors, dark } = useTheme();
  const [location, setLocation] = useState<any>({
    ward: params?.dataAddress?.ward,
    district: params?.dataAddress?.district,
    city: params?.dataAddress?.city,
  });
  const dispatch = useAppDispatch();

  const [fullname, setfullname] = useState<string>(params?.dataAddress?.fullname || '');
  const [phoneNumber, setphoneNumber] = useState<string>(params?.dataAddress?.phone || '');
  const [address, setaddress] = useState<string>(params?.dataAddress?.address || '');
  const userReducer = useAppSelector((state) => state.userReducer);

  const [isDefaultAdd, setIsDefaultAdd] = useState<boolean>(
    params?.dataAddress?.is_default || params?.firstAddress || false
  );
  const onChangeLocation = (data: any) => {
    setLocation(data);
  };
  const [createAddress, { isLoading: isUpdating }] = useAddAddressMutation();
  const [updateAddress, { isLoading: isUpdatingAddress }] = useUpdateAddressMutation();

  const styles = StyleSheet.create({
    input: { backgroundColor: colors.background, fontSize: 13 },
  });
  async function addAddress() {
    let dataSentToServer = {
      user_id: userReducer.user?.id,
      address: address,
      phone: phoneNumber,
      fullname: fullname,
      ward: location.ward,
      district: location.district,
      is_default: isDefaultAdd,
      city: location.city,
    };
    PSpinner.show();
    await createAddress(dataSentToServer)
      .unwrap()
      .then((dataAddress) => {
        if (dataAddress.is_default) {
          if (params?.firstAddress) {
            params.onChangeAddress && params.onChangeAddress(dataAddress);
          }
        }
        navigation.goBack();
      })
      .catch((err) => {
        myAlert('Tạo địa chỉ thất bại');
      })
      .finally(() => PSpinner.hide());
  }

  async function editAddress() {
    let dataSentToServer = {
      address: address,
      phone: phoneNumber,
      fullname: fullname,
      ward: location.ward,
      is_default: isDefaultAdd,
      district: location.district,
      city: location.city,
    };
    PSpinner.show();
    await updateAddress({ id: params?.dataAddress?.id, body: dataSentToServer })
      .unwrap()
      .then((res) => {
        navigation.goBack();
      })
      .catch((err) => {
        myAlert('Cập nhật địa chỉ thất bại');
      })
      .finally(() => PSpinner.hide());
  }

  return (
    <FixedContainer edges={edges}>
      <CustomHeader showBackButton title="Địa chỉ mới" />

      <PScrollView  isHaveTextInput style={{ flex: 1, backgroundColor: colors.background2 }}>
        <Text
          style={{
            fontSize: pFontSize(12),
            paddingVertical: widthScale(16),
            paddingHorizontal: widthScale(12),
          }}
        >
          Liên hệ
        </Text>
        <TextInput
          onChangeText={(text) => {
            setfullname(text);
          }}
          placeholder={'Họ và tên'}
          style={styles.input}
          value={fullname}
        />
        <TextInput
          placeholder={'Số điện thoại'}
          maxLength={10}
          keyboardType={'phone-pad'}
          style={styles.input}
          onChangeText={(text) => {
            setphoneNumber(text);
          }}
          underlineColor="transparent"
          value={phoneNumber}
        />
        <Text
          style={{
            fontSize: pFontSize(12),
            paddingVertical: widthScale(16),
            paddingHorizontal: widthScale(12),
          }}
        >
          Địa chỉ
        </Text>
        <PTouchableOpacity
          onPress={() =>
            navigation.navigate(ROUTE_KEY.SelectRegion, {
              onChangeLocation,
              data: { ward: location.ward, district: location.district, city: location.city },
            })
          }
        >
          <TextInput
            placeholder={'Phường/Xã, Quận/Huyện, Tỉnh/Thành phố'}
            style={styles.input}
            editable={false}
            onTouchEnd={() =>
              navigation.navigate(ROUTE_KEY.SelectRegion, {
                onChangeLocation,
                data: { ward: location.ward, district: location.district, city: location.city },
              })
            }
            value={
              location.ward && location.district && location.city
                ? `${location.ward}, ${location.district}, ${location.city}`
                : ''
            }
          />
        </PTouchableOpacity>
        <TextInput
          placeholder="Tên đường tòa nhà số nhà"
          style={styles.input}
          underlineColor="transparent"
          value={address}
          onChangeText={(text) => {
            setaddress(text);
          }}
        />
        <Text
          style={{
            fontSize: pFontSize(12),
            paddingVertical: widthScale(16),
            paddingHorizontal: widthScale(12),
          }}
        >
          Cài đặt
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingHorizontal: widthScale(12),
            height: heightScale(50),
            backgroundColor: 'white',
          }}
        >
          <Text>Đặt địa chỉ làm mặc định</Text>
          <Switch
            value={isDefaultAdd}
            onValueChange={() => {
              setIsDefaultAdd(!isDefaultAdd);
            }}
          />
          {(params?.dataAddress?.is_default || params?.firstAddress) && (
            <View
              style={{
                position: 'absolute',
                width: 70,
                height: 30,
                backgroundColor: 'red',
                opacity: 0.0,
                right: 0,
              }}
            ></View>
          )}
        </View>
      </PScrollView>
      <Button
        mode="contained"
        style={{
          borderRadius: 0,
          backgroundColor: colors.accent,
          paddingBottom: insets.bottom / 2,
        }}
        disabled={
          !phoneNumber ||
          !fullname ||
          !location.ward ||
          !location.district ||
          !location.city ||
          !address
        }
        color={colors.surface}
        contentStyle={{ height: widthScale(60) }}
        onPress={() => {
          if (!validatePhone(phoneNumber)) {
            Alert.alert('Số điện thoại không đúng vui lòng nhập lại', '', [
              {
                text: 'Đóng',
                onPress: () => {},
              },
            ]);
          } else if (!validateNameVi(fullname)) {
            Alert.alert('Tên không hợp lệ, vui lòng nhập lại', '', [
              {
                text: 'Đóng',
                onPress: () => {},
              },
            ]);
          } else if (!location.ward && !location.district && !location.city) {
            Alert.alert('Vui lòng nhập Phường/Xã, Quận/Huyện, Tỉnh/Thành phố', '', [
              {
                text: 'Đóng',
                onPress: () => {},
              },
            ]);
          } else if (!address) {
            Alert.alert('Vui lòng nhập địa chỉ', '', [
              {
                text: 'Đóng',
                onPress: () => {},
              },
            ]);
          } else {
            params?.dataAddress ? editAddress() : addAddress();
          }
        }}
      >
        {params?.dataAddress ? 'Lưu' : 'Tạo'}
      </Button>
    </FixedContainer>
  );
}

export default CreateDeliveryAddress;
