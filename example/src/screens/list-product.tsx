import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { Image, Text, TextInput, View } from 'react-native';
import {
  Caption,
  Divider,
  Menu,
  Subheading,
  useTheme,
} from 'react-native-paper';
import { Edge, useSafeAreaInsets } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { ICON } from '~/assets/imagePath';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { ModalContext } from '~components/modalContext';
import { PFlatList } from '~components/PFlatList';
import { PRow } from '~components/PRow';
import PTouchableOpacity from '~components/PTouchableOpacity';
import { ProductProps, UserProps } from '~constants/types';
import { RootStackScreenProps } from '~navigators/root-stack';
import { ROUTE_KEY } from '~navigators/router-key';
import { changeCity } from '~reducers/addressReducer';
import { addItemToCart } from '~reducers/cartReducer';
import { useListProductByCategoryQuery } from '~services/api_product_services';
import { useAppDispatch, useAppSelector } from '~store/storeHooks';
import { pColor } from '~styles/colors';
import { heightScale, widthScale } from '~styles/scaling-utils';
import { pShadow } from '~styles/style';
import { pFont, pText } from '~styles/typography';
import { getNumberString } from '~utils/numberUtils';
import { myAlert } from '../utils/MyAlert';

interface Item {
  id: string;
  name: string;
  image: Array<string>;
  price: number;
  data: ProductProps[];
}
const edges: Edge[] = ['right', 'left', 'bottom'];

export default function ListProduct(
  props: RootStackScreenProps<'ListProduct'>
) {
  const { navigation, route } = props;
  const params = route.params;
  const { colors, dark } = useTheme();
  const insets = useSafeAreaInsets();
  const dispatch = useAppDispatch();

  const [page, setPage] = useState(1);
  const userReducer = useAppSelector(
    (state) => state.userReducer.user
  ) as UserProps;
  const selectCity = useAppSelector((state) => state.addressReducer.selectCity);
  const [txtSearch, setTxtSearch] = useState('');

  const [isGridView, setIsGridView] = useState<boolean>(true);

  const { data, isLoading, isFetching, isSuccess, refetch } =
    useListProductByCategoryQuery({
      category_id: params?.category.id,
      page,
      txt_search: txtSearch,
    });

  const [dataProduct, setDataProduct] = useState(data || []);
  const onRefresh = () => {
    if (page > 1) {
      setDataProduct([]);
      setPage(1);
    }
    refetch();
  };
  const onEndReached = () => {
    if (data && data?.length) {
      setPage((page) => page + 1);
    }
  };

  useEffect(() => {
    if (page === 1) {
      data &&
        setDataProduct((oldData) => _.uniqBy([...data, ...oldData], 'id'));
    } else {
      data &&
        setDataProduct((oldData) => _.uniqBy([...oldData, ...data], 'id'));
    }
  }, [data]);
  const addCart = (item: ProductProps) => {
    dispatch(addItemToCart({ product: item }));
    myAlert('Đã thêm sản phẩm');
  };

  const { showModalProduct } = React.useContext(ModalContext);

  const renderContentGrid = ({
    item,
    index,
  }: {
    item: ProductProps;
    index: number;
  }) => {
    return (
      <PTouchableOpacity
        onPress={() => {
          console.log(
            '🚀 code.bug ~ file: list-product.tsx ~ line 77 ~ item',
            item.id
          );
          showModalProduct(item);
        }}
        key={index}
        style={{
          width: '48%',
          alignItems: 'center',
          justifyContent: 'center',
          ...pShadow.BLUR0,
          margin: 4,
          backgroundColor: colors.background,
          borderRadius: widthScale(8),
        }}
      >
        <Image
          source={{ uri: item?.thumbnail || '' }}
          style={{
            width: '100%',
            height: heightScale(150),
            alignItems: 'center',
            justifyContent: 'center',
            borderTopRightRadius: widthScale(8),
            borderTopLeftRadius: widthScale(8),
          }}
          resizeMode="cover"
        />
        <PTouchableOpacity
          style={{
            backgroundColor: colors.placeholder,
            width: widthScale(32),
            height: widthScale(32),
            borderRadius: widthScale(30),
            alignItems: 'center',
            top: heightScale(8),
            right: widthScale(8),
            justifyContent: 'center',
            position: 'absolute',
          }}
          onPress={() => console.log('share ne')}
        >
          <MaterialCommunityIcons
            name={'share-variant'}
            color={colors.accent}
            size={widthScale(16)}
            style={{}}
          />
        </PTouchableOpacity>

        <View
          style={{
            paddingHorizontal: widthScale(6),
            width: '100%',
            marginBottom: widthScale(4),
          }}
        >
          <Text
            numberOfLines={2}
            style={{
              ...pText.BODY1,
              fontWeight: 'bold',
              height: heightScale(40),
              alignSelf: 'flex-start',
              marginTop: heightScale(4),
            }}
          >
            {item?.title}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            {userReducer.is_collab && item.contributor_rate > 0 ? (
              <PRow>
                <Text
                  style={{
                    color: colors.disabled,
                    fontFamily: pFont.FONT2,
                    textDecorationLine: 'line-through',
                    top: widthScale(1),
                    marginRight: widthScale(4),
                  }}
                >
                  {getNumberString(item?.price)}
                </Text>
                <Subheading
                  style={{ fontFamily: pFont.FONT2, color: colors.accent }}
                >
                  {getNumberString(item?.price * (1 - item?.contributor_rate))}
                </Subheading>
              </PRow>
            ) : (
              <Subheading
                style={{ color: colors.accent, fontFamily: pFont.FONT2 }}
              >
                {item?.price ? getNumberString(item?.price) : 'Liên hệ'}
              </Subheading>
            )}
            {ButtonAdd(item)}
          </View>
        </View>
      </PTouchableOpacity>
    );
  };
  const renderContentList = ({
    item,
    index,
  }: {
    item: ProductProps;
    index: number;
  }) => {
    return (
      <PTouchableOpacity
        onPress={() => {
          showModalProduct(item);
        }}
        key={index}
        style={{
          width: '100%',
          paddingHorizontal: widthScale(12),
          marginTop: heightScale(12),
          alignItems: 'center',
          flexDirection: 'row',
        }}
      >
        <Image
          source={{ uri: item?.thumbnail || '' }}
          style={{
            width: widthScale(66),
            height: heightScale(60),
            overflow: 'hidden',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        />

        <View style={{ marginLeft: widthScale(12), flex: 1 }}>
          <Text
            numberOfLines={2}
            style={{
              ...pText.BODY1,
              fontWeight: 'bold',
              width: '90%',
              height: heightScale(40),
              alignSelf: 'flex-start',
            }}
          >
            {item?.title}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            {userReducer.is_collab && item.contributor_rate > 0 ? (
              <PRow>
                <Text
                  style={{
                    color: colors.disabled,
                    fontFamily: pFont.FONT2,
                    textDecorationLine: 'line-through',
                    top: widthScale(1),
                    marginRight: widthScale(4),
                  }}
                >
                  {getNumberString(item?.price)}
                </Text>
                <Subheading
                  style={{ fontFamily: pFont.FONT2, color: colors.accent }}
                >
                  {getNumberString(item?.price * (1 - item?.contributor_rate))}
                </Subheading>
              </PRow>
            ) : (
              <Subheading
                style={{ color: colors.accent, fontFamily: pFont.FONT2 }}
              >
                {item?.price ? getNumberString(item?.price) : 'Liên hệ'}
              </Subheading>
            )}

            <PTouchableOpacity
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: widthScale(30),
                top: widthScale(2),
              }}
              onPress={() => onShare(userReducer.is_collab, item)}
            >
              <Text style={{ color: colors.primary }}>Chia sẻ</Text>
            </PTouchableOpacity>
          </View>
        </View>
        {ButtonAdd(item)}
      </PTouchableOpacity>
    );
  };
  const [visible, setVisible] = React.useState(false);

  const openMenu = () => setVisible(true);

  const closeMenu = () => setVisible(false);

  return (
    <FixedContainer edges={edges} style={{}}>
      <CustomHeader
        title="Ho Chi Minh"
        subtitle="Khu vực đặt hàng"
        showBackButton
        customCenter={
          <View
            style={{
              paddingHorizontal: widthScale(30),
              backgroundColor: '#FBFBFB',
              borderRadius: 50,
              flex: 1,
              height: heightScale(36),
              justifyContent: 'center',
            }}
          >
            <Image
              source={ICON.search}
              style={{
                width: widthScale(14),
                height: widthScale(14),
                position: 'absolute',
                alignSelf: 'center',
                left: widthScale(10),
              }}
            />
            <TextInput
              placeholder={'Tìm kiếm sản phẩm'}
              style={{ ...pText.BODY2, flex: 1, color: colors.text }}
              placeholderTextColor={colors.placeholder}
              onEndEditing={(event) => {
                setPage(1);
                setDataProduct([]);
                setTxtSearch(event.nativeEvent.text ?? '');
              }}
            />
          </View>
        }
        iconRightMenu="cart-outline"
        onRightMenuPress={() => navigation.navigate(ROUTE_KEY.Cart)}
      />
      <View style={{ flex: 1 }}>
        <PRow
          style={{
            height: heightScale(50),
            justifyContent: 'space-between',
            paddingLeft: widthScale(20),
            paddingRight: widthScale(12),
            borderBottomWidth: widthScale(2),
            borderBottomColor: pColor.backgroundGrey,
          }}
        >
          <Menu
            visible={visible}
            contentStyle={{ marginTop: widthScale(20) }}
            onDismiss={closeMenu}
            anchor={
              <PTouchableOpacity
                style={{ flexDirection: 'row', alignItems: 'center' }}
                onPress={openMenu}
              >
                <Image
                  source={ICON.location}
                  style={{
                    width: widthScale(14),
                    height: widthScale(14),
                    tintColor: pColor.black,
                  }}
                />
                <Caption style={{ marginLeft: widthScale(3) }}>
                  {selectCity}
                </Caption>
              </PTouchableOpacity>
            }
          >
            <Menu.Item
              onPress={() => {
                dispatch(changeCity('Hà Nội'));
                closeMenu();
              }}
              title="Hà Nội"
            />
            <Menu.Item
              onPress={() => {
                dispatch(changeCity('Đà Nẵng'));
                closeMenu();
              }}
              title="Đà Nẵng"
            />
            <Menu.Item
              onPress={() => {
                dispatch(changeCity('TP. Hồ Chí Minh'));
                closeMenu();
              }}
              title="TP. Hồ Chí Minh"
            />
            <Divider />
          </Menu>
          <PRow style={{ height: '100%' }}>
            <PTouchableOpacity
              style={{
                width: widthScale(40),
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => {
                !isGridView && setIsGridView(true);
              }}
            >
              <Image
                source={ICON.grid}
                style={{
                  width: widthScale(16),
                  height: widthScale(16),
                  tintColor: isGridView ? colors.accent : pColor.black,
                }}
              />
            </PTouchableOpacity>
            <PTouchableOpacity
              style={{
                width: widthScale(40),
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => {
                isGridView && setIsGridView(false);
              }}
            >
              <Image
                source={ICON.list}
                style={{
                  width: widthScale(16),
                  height: widthScale(16),
                  tintColor: !isGridView ? colors.accent : pColor.black,
                }}
              />
            </PTouchableOpacity>
          </PRow>
        </PRow>
        {isGridView ? (
          <PFlatList
            refreshing={isFetching}
            onRefresh={onRefresh}
            textNull={`"Hông" có gì ở trong danh mục này hết\nLướt Langbiang, sản phẩm sẽ sớm cập nhật!`}
            key="GridView"
            contentContainerStyle={{ paddingVertical: widthScale(8) }}
            columnWrapperStyle={{
              paddingHorizontal: widthScale(3),
              paddingBottom: widthScale(3),
            }}
            data={dataProduct}
            numColumns={2}
            keyExtractor={(item: any) => item?.id}
            renderItem={renderContentGrid as any}
            onEndReached={onEndReached}
            isLoading={isLoading || isFetching || !isSuccess}
          />
        ) : (
          <PFlatList
            refreshing={isFetching}
            onRefresh={onRefresh}
            textNull={`"Hông" có gì ở trong danh mục này hết\nLướt Langbiang, sản phẩm sẽ sớm cập nhật!`}
            key="ListView"
            data={dataProduct}
            keyExtractor={(item: any) => item?.id}
            renderItem={renderContentList as any}
            onEndReached={onEndReached}
            isLoading={isLoading || isFetching || !isSuccess}
          />
        )}
      </View>
    </FixedContainer>
  );

  function ButtonAdd(item: ProductProps) {
    return (
      <PTouchableOpacity
        onPress={() => addCart(item)}
        style={{
          width: widthScale(36),
          height: widthScale(36),
          borderRadius: widthScale(36),
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <View
          style={{
            backgroundColor: colors.primary,
            width: '64%',
            height: '64%',
            borderRadius: widthScale(36),
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Icon name={'plus'} size={widthScale(10)} color={pColor.white} />
        </View>
      </PTouchableOpacity>
    );
  }
}
