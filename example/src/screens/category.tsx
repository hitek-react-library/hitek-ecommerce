import React, { useEffect } from 'react';
import { FlatList, ImageBackground, View } from 'react-native';
import { Headline, useTheme } from 'react-native-paper';
import { Edge } from 'react-native-safe-area-context';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import PTouchableOpacity from '~components/PTouchableOpacity';
import { CategoryProps } from '~constants/types';
import { RootStackScreenProps } from '~navigators/root-stack';
import { ROUTE_KEY } from '~navigators/router-key';
import { useListCategoryQuery } from '~services/api_product_services';
import { pColor } from '~styles/colors';
import { heightScale, widthScale } from '~styles/scaling-utils';
import { pStyle } from '~styles/style';
import { pFont } from '~styles/typography';

export default function Category(props: RootStackScreenProps<'Category'>) {
  const { navigation } = props;
  const { colors } = useTheme();
  const { data, isLoading, isFetching, error } = useListCategoryQuery();
  useEffect(() => {}, []);

  const renderItem = ({ item }: { item: CategoryProps }) => {
    return (
      <PTouchableOpacity onPress={() => navigation?.navigate('ListProduct', { category: item })}>
        <ImageBackground
          source={{ uri: item?.image }}
          style={{
            width: '100%',
            height: heightScale(150),
            backgroundColor: pColor.white,
            marginTop: heightScale(20),
            borderRadius: 8,
            overflow: 'hidden',
          }}
          resizeMode="cover"
        >
          <View
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: '#00000060',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Headline
              style={{
                fontFamily: pFont.FONT2,
                fontWeight: 'bold',
                color: 'white',
                ...pStyle.textShadow,
                textAlign: 'center',
              }}
            >
              {item?.title}
            </Headline>
          </View>
        </ImageBackground>
      </PTouchableOpacity>
    );
  };
  const edges: Edge[] = ['right', 'left', 'bottom'];

  return (
    <FixedContainer edges={edges} style={{}}>
      <CustomHeader
        title="Ho Chi Minh"
        subtitle="Khu vực đặt hàng"
        showBackButton
        iconRightMenu="cart-outline"
        onRightMenuPress={() => navigation.navigate(ROUTE_KEY.Cart)}
      />
      <View style={{ flex: 1, paddingHorizontal: widthScale(20) }}>
        <FlatList
          data={data as any}
          renderItem={renderItem}
          contentContainerStyle={{ paddingBottom: heightScale(70) }}
          showsVerticalScrollIndicator={false}
        />
      </View>
    </FixedContainer>
  );
}
