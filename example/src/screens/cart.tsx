import React, { useEffect, useState } from 'react';
import { Alert, Image, View } from 'react-native';
import { Button, Paragraph, Subheading, Text, Title, useTheme } from 'react-native-paper';
import { Edge } from 'react-native-safe-area-context';
import CheckBoxCustom from '~components/check-box-custom';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { ModalContext } from '~components/modalContext';
import { PFlatList } from '~components/PFlatList';
import { PRow } from '~components/PRow';
import PStepper from '~components/PStepper';
import PTouchableOpacity from '~components/PTouchableOpacity';
import { CartProps, UserProps } from '~constants/types';
import { RootStackScreenProps } from '~navigators/root-stack';
import { ROUTE_KEY } from '~navigators/router-key';
import { clearCart, removeItemFromCartAll, setCountForItem } from '~reducers/cartReducer';
import { useAppDispatch, useAppSelector } from '~store/storeHooks';
import { widthScale } from '~styles/scaling-utils';
import { pFont } from '~styles/typography';
import { getNumberString, getPriceWithCommas } from '~utils/numberUtils';

const edges: Edge[] = ['right', 'left', 'bottom'];

function Cart(props: RootStackScreenProps<'Cart'>) {
  const { navigation } = props;
  const { colors, dark } = useTheme();
  const dispatch = useAppDispatch();
  const { cart, totalQuantity, totalPrice } = useAppSelector((state) => state.cartReducer);
  const userReducer = useAppSelector((state) => state.userReducer.user) as UserProps;
  const [checkAll, setCheckAll] = useState(cart.length > 0 ? true : false);
  const [_cart, setCart] = useState<CartProps[]>(cart);
  const [listCheckbox, setListCheckbox] = useState<CartProps[]>(cart);
  const [_totalPrice, setTotalPrice] = useState(0);

  useEffect(() => {
    setCart(cart);
  }, [cart]);

  useEffect(() => {
    _cart.length > 0 && listCheckbox.length === _cart.length && setCheckAll(true);
  }, [listCheckbox]);

  useEffect(() => {
    let tempTotal = 0;
    listCheckbox.forEach((v: CartProps) => {
      tempTotal = Number(v.price) * Number(v.quantity) + tempTotal;
    });
    setTotalPrice(tempTotal);
  }, [listCheckbox, totalQuantity, totalPrice]);

  let { showModalProduct } = React.useContext(ModalContext);

  const _renderItem = ({ item }: { item: CartProps }) => {
    const isAvailable = listCheckbox.findIndex((e) => e.id === item.id) >= 0;
    return (
      <PRow
        style={{
          minHeight: widthScale(120),
          alignItems: 'flex-start',
          paddingVertical: widthScale(12),
        }}
      >
        <CheckBoxCustom
          isAlwaysChecked={checkAll || isAvailable}
          style={{ top: widthScale(16), marginLeft: widthScale(8) }}
          onValueChange={(isChecked) => {
            setCheckAll(false);
            if (isChecked) {
              setListCheckbox((old: CartProps[]) => [...old, item]);
            } else {
              setListCheckbox((old: CartProps[]) =>
                old?.filter((oldItem) => oldItem.id !== item.id)
              );
            }
          }}
        />
        <View
          style={{
            flexDirection: 'row',
            paddingLeft: widthScale(12),
            paddingBottom: widthScale(4),
          }}
        >
          <Image
            source={{ uri: item.thumbnail || '' }}
            style={{
              width: widthScale(65),
              height: widthScale(65),
              marginRight: widthScale(8),
            }}
            resizeMode="cover"
          />
          <View style={{ width: '72%' }}>
            <PTouchableOpacity onPress={() => showModalProduct(item)}>
              <Text style={{ fontWeight: 'bold', minHeight: widthScale(40) }} numberOfLines={2}>
                {item.title}
              </Text>
              {userReducer.is_collab && item.contributor_rate > 0 ? (
                <PRow>
                  <Text
                    style={{
                      color: colors.disabled,
                      fontFamily: pFont.FONT2,
                      textDecorationLine: 'line-through',
                      top: widthScale(1),
                      marginRight: widthScale(4),
                    }}
                  >
                    {getNumberString(item?.price)}
                  </Text>
                  <Subheading style={{ fontFamily: pFont.FONT2, color: colors.accent }}>
                    {getNumberString(item?.price * (1 - item?.contributor_rate))}
                  </Subheading>
                </PRow>
              ) : (
                <Subheading style={{ color: colors.accent, fontFamily: pFont.FONT2 }}>
                  {item?.price ? getNumberString(item?.price) : 'Liên hệ'}
                </Subheading>
              )}
            </PTouchableOpacity>

            <PStepper
              style={{ marginTop: widthScale(8) }}
              initValue={item.quantity}
              minValue={0}
              maxValue={999}
              onMinValue={() =>
                Alert.alert('Bạn muốn xóa sản phẩm', item.title || '', [
                  {
                    text: 'Hủy bỏ',
                    onPress: () => {},
                  },
                  {
                    text: 'Xóa',
                    onPress: () => {
                      dispatch(removeItemFromCartAll(item));
                      isAvailable &&
                        setListCheckbox(listCheckbox?.filter((oldItem) => oldItem.id !== item.id));
                    },
                    style: 'cancel',
                  },
                ])
              }
              onChangeValue={(count: number) => {
                dispatch(setCountForItem({ product: item, quantity: count }));
                if (isAvailable) {
                  const newCheckList = listCheckbox.map((obj) =>
                    obj.id === item.id ? { ...obj, quantity: count } : obj
                  );
                  setListCheckbox(newCheckList);
                }
              }}
            />
          </View>
        </View>
      </PRow>
    );
  };
  return (
    <FixedContainer edges={edges}>
      <CustomHeader
        showBackButton
        title="Giỏ hàng"
        iconRightMenu="delete"
        onRightMenuPress={
          totalQuantity > 0
            ? () =>
                Alert.alert(
                  checkAll
                    ? 'Bạn muốn xóa giỏ hàng'
                    : `Bạn muốn xóa ${listCheckbox.length} sản phẩm`,
                  '',
                  [
                    {
                      text: 'Hủy bỏ',
                      onPress: () => {},
                    },
                    {
                      text: 'Xóa',
                      onPress: () => {
                        if (checkAll) {
                          dispatch(clearCart());
                        } else {
                          listCheckbox.forEach((item) => {
                            dispatch(removeItemFromCartAll(item));
                          });
                          setListCheckbox([]);
                        }
                      },
                      style: 'cancel',
                    },
                  ]
                )
            : undefined
        }
      />
      <PFlatList
        isTabScreen
        textNull={`"Hông" có gì ở trong giỏ hết\nLướt Langbiang, lựa hàng ngay nào!`}
        data={_cart}
        renderItem={_renderItem as any}
        contentContainerStyle={{ paddingTop: widthScale(16) }}
        keyExtractor={(item: any) => item?.id}
        ItemSeparatorComponent={() => (
          <View
            style={{
              flexDirection: 'row',
              borderBottomWidth: 1,
              borderBottomColor: '#0002',
              marginBottom: widthScale(12),
              paddingBottom: widthScale(4),
            }}
          ></View>
        )}
      />
      {_cart.length > 0 ? (
        <PRow
          style={{
            justifyContent: 'space-between',
            borderTopWidth: 2,
            borderColor: colors.background2,
          }}
        >
          <PRow style={{}}>
            <CheckBoxCustom
              isAlwaysChecked={checkAll}
              style={{ marginLeft: widthScale(4) }}
              onValueChange={(isChecked) => {
                setCheckAll(isChecked);
                if (isChecked) {
                  setListCheckbox(cart);
                } else {
                  setListCheckbox([]);
                }
              }}
            />
            <Text>Tất cả</Text>
          </PRow>
          <PRow>
            <View style={{ alignItems: 'flex-end', top: widthScale(6) }}>
              <Paragraph>Tổng tiền:</Paragraph>
              <Title
                style={{
                  color: colors.accent,
                  fontFamily: pFont.FONT2,
                  top: widthScale(-8),
                }}
              >
                {getPriceWithCommas(checkAll ? totalPrice : _totalPrice)}
              </Title>
            </View>
            <Button
              disabled={listCheckbox.length < 1}
              style={[
                {
                  borderRadius: 0,
                  marginLeft: widthScale(12),
                },
                listCheckbox.length > 0 && { backgroundColor: colors.accent },
              ]}
              contentStyle={{ height: widthScale(60) }}
              onPress={() => {
                navigation.navigate(ROUTE_KEY.Payment, {
                  cart: listCheckbox,
                  totalPrice: _totalPrice,
                });
              }}
              mode="contained"
            >
              Đặt hàng
            </Button>
          </PRow>
        </PRow>
      ) : null}
    </FixedContainer>
  );
}

export default Cart;
