import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Headline, HelperText } from 'react-native-paper';
import { PScrollView } from '~components/PScrollView';
import { TopTabScreenProps } from '~navigators/top-tab';

const One = (props: TopTabScreenProps<'One'>) => {
  const { navigation } = props;
  return (
    <PScrollView isTabScreen>
      <View style={styles.center}>
        <Headline>Top Tab Screen One</Headline>
        <HelperText type="info" style={styles.path}>
          src/screens/one.tsx
        </HelperText>
        <Button onPress={() => navigation.jumpTo('Two')}>Go to Screen Two</Button>
        <Button onPress={() => navigation.jumpTo('Three')}>Go to Screen Three</Button>
      </View>
    </PScrollView>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    minHeight: 1000,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
  },
  path: {
    fontSize: 11,
  },
});

export default One;
