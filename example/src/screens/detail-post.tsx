import React from 'react';
import { Edge } from 'react-native-safe-area-context';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { PScrollView } from '~components/PScrollView';
import PWebView from '~components/PWebView';
import { RootStackScreenProps } from '~navigators/root-stack';

const edges: Edge[] = ['right', 'left'];

function DetailPost(props: RootStackScreenProps<'DetailPost'>) {
  const params = props.route.params;
  return (
    <FixedContainer edges={edges}>
      <CustomHeader title={params?.title} showBackButton />
      <PScrollView style={{ flex: 1 }}>
        <PWebView source={{ html: params?.description || '' }} />
      </PScrollView>
    </FixedContainer>
  );
}

export default DetailPost;
