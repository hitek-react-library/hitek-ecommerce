import Clipboard from '@react-native-clipboard/clipboard';
import { skipToken } from '@reduxjs/toolkit/dist/query';
import React, { useEffect, useState } from 'react';
import { Image, View } from 'react-native';
import {
  Button,
  Caption,
  Paragraph,
  Subheading,
  Text,
  Title,
  useTheme,
} from 'react-native-paper';
import { Edge, useSafeAreaInsets } from 'react-native-safe-area-context';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { PFlatList } from '~components/PFlatList';
import { PRow } from '~components/PRow';
import PSpinner from '~components/PSpinner';
import {
  ActivityOrder,
  CartProps,
  DiscountType,
  OrderItemProps,
  OrderProps,
} from '~constants/types';
import { RootStackScreenProps } from '~navigators/root-stack';
import {
  useGetOneOrderQuery,
  useUpdateActivityOrderMutation,
} from '~services/api_order_services';
import { useAppSelector } from '~store/storeHooks';
import { widthScale } from '~styles/scaling-utils';
import { pFont } from '~styles/typography';
import { myAlert, myAlertYesNo } from '~utils/MyAlert';
import { myMessageErrorOrder } from '~utils/myMessage';
import { getNumberString } from '~utils/numberUtils';
import { getStatusOrder } from '~utils/statusOrder';

const edges: Edge[] = ['right', 'left', 'bottom'];

function InfoOrder(props: RootStackScreenProps<'InfoOrder'>) {
  const { navigation, route } = props;
  const { colors, dark } = useTheme();
  const insets = useSafeAreaInsets();

  const [viewAll, setViewAll] = useState(false);
  const userReducer = useAppSelector((state) => state.userReducer.user);

  const [order, setOrder] = useState<OrderProps>(route.params.order);
  const [orderItems, setOrderItems] = useState<OrderItemProps[]>(
    route.params.order.order_items
  );

  const [updateActivityOrder, { isLoading: preOrderLoading }] =
    useUpdateActivityOrderMutation();
  const { data, isLoading } = useGetOneOrderQuery(
    route.params.order?.id ?? skipToken
  );
  const status = getStatusOrder(order?.order_activitys);
  useEffect(() => {
    data && setOrder(data);
  }, [data]);
  const updateOrder = () => {
    myAlertYesNo('Xác nhận hủy đơn', 'Bạn muốn hủy đơn hàng', () => {
      PSpinner.show();
      updateActivityOrder({
        order_id: order?.id,
        activity: ActivityOrder.CANCELLED,
      })
        .unwrap()
        .then((payload) => {
          myAlert('Hủy đơn hàng thành công');
          navigation.goBack();
        })
        .catch((error) => {
          myMessageErrorOrder({
            message: 'Có lỗi xãy ra khi hủy đặt hàng',
            description: error?.data?.message,
            insets,
          });
        })
        .finally(() => {
          PSpinner.hide();
        });
    });
  };

  const _renderItem = ({ item }: { item: CartProps }) => {
    return (
      <PRow
        style={{
          alignItems: 'flex-start',
          paddingLeft: widthScale(12),
          paddingVertical: widthScale(10),
        }}
      >
        <Image
          source={{ uri: item.thumbnail || '' }}
          style={{
            width: widthScale(80),
            height: widthScale(80),
            marginRight: widthScale(8),
          }}
          resizeMode="contain"
        />
        <View style={{ width: '72%', justifyContent: 'space-between' }}>
          <Text style={{ minHeight: widthScale(40) }} numberOfLines={2}>
            {item.title}
          </Text>
          <PRow
            style={{
              marginLeft: widthScale(4),
              justifyContent: 'space-between',
            }}
          >
            {item.discount_price! > 0 ? (
              <PRow>
                <Text
                  style={{
                    color: colors.disabled,
                    fontFamily: pFont.FONT2,
                    textDecorationLine: 'line-through',
                    top: widthScale(1),
                    marginRight: widthScale(4),
                  }}
                >
                  {getNumberString(item?.price)}
                </Text>
                <Subheading
                  style={{ fontFamily: pFont.FONT2, color: colors.accent }}
                >
                  {getNumberString(item.discount_price)}
                </Subheading>
              </PRow>
            ) : (
              <Subheading
                style={{ color: colors.accent, fontFamily: pFont.FONT2 }}
              >
                {item?.price ? getNumberString(item?.price) : 'Liên hệ'}
              </Subheading>
            )}
            <Caption style={{ top: widthScale(4) }}>x{item.quantity}</Caption>
          </PRow>
        </View>
      </PRow>
    );
  };

  const ListHeaderComponent = () => {
    return (
      <View style={{ flex: 1, backgroundColor: colors.background }}>
        <PRow
          style={{
            paddingHorizontal: widthScale(12),
            borderBottomWidth: widthScale(2),
            borderColor: colors.disabled,
            paddingVertical: widthScale(40),
            marginBottom: widthScale(16),
            backgroundColor: status?.color,
          }}
        >
          <View style={{ flex: 1 }}>
            <Subheading style={{ fontWeight: 'bold', color: colors.surface }}>
              {status?.text}
            </Subheading>
          </View>
          <MaterialCommunityIcons
            name={status?.icon}
            color={colors.surface}
            size={widthScale(40)}
          />
        </PRow>
        <PRow
          style={{
            paddingHorizontal: widthScale(12),
            borderBottomWidth: widthScale(2),
            borderColor: colors.disabled,
            paddingBottom: widthScale(8),
            marginBottom: widthScale(16),
          }}
        >
          <MaterialCommunityIcons
            name="map-marker"
            color={colors.accent}
            size={widthScale(20)}
            style={{ alignSelf: 'flex-start' }}
          />
          <View style={{ flex: 1, marginLeft: widthScale(8) }}>
            <Text style={{ fontWeight: 'bold' }}>Địa chỉ nhận hàng</Text>
            <Paragraph style={{}}>
              {order?.fullname} | ({order?.phone})
            </Paragraph>
            <Caption style={{}}>{order?.address}</Caption>
            <Caption style={{}}>
              {order?.ward &&
                order.district &&
                order.city &&
                `${order?.ward || ''}, ${order?.district || ''}, ${
                  order?.city || ''
                }`}
            </Caption>
          </View>
          <Text
            style={{ color: '#1DAB9A' }}
            onPress={() => {
              Clipboard.setString(order?.address || '');
              myAlert('Đã sao chép');
            }}
          >
            Sao chép
          </Text>
        </PRow>
      </View>
    );
  };
  const ListFooterComponent = () => {
    return (
      <View style={{}}>
        {orderItems.length > 2 ? (
          <View
            style={{
              width: '100%',
              alignSelf: 'center',
              backgroundColor: colors.background,
            }}
          >
            <Button onPress={() => setViewAll(!viewAll)}>
              {viewAll ? 'Thu gọn' : `Xem thêm (${orderItems.length - 2})`}
            </Button>
          </View>
        ) : null}
        {order?.discount! > 0 || order?.bonus_point! > 0 ? (
          <View
            style={{
              backgroundColor: colors.background,
              marginTop: widthScale(10),
              paddingHorizontal: widthScale(12),
              paddingVertical: widthScale(8),
            }}
          >
            <PRow style={{ flex: 1 }}>
              <MaterialCommunityIcons
                name="account-star"
                color={colors.accent}
                size={widthScale(20)}
                style={{ alignSelf: 'flex-start' }}
              />
              <Text style={{ marginLeft: widthScale(8) }}>
                {order?.discount_type === DiscountType.CONTRIBUTOR
                  ? 'Ưu đãi cộng tác viên'
                  : 'Ưu đãi'}
              </Text>
            </PRow>
            <PRow
              style={{
                justifyContent: 'space-between',
                marginTop: widthScale(8),
              }}
            >
              <Caption style={{}}>Tổng tiền được giảm</Caption>
              <Caption style={{}}>{getNumberString(order?.discount)}</Caption>
            </PRow>
            {order?.discount_type !== DiscountType.CONTRIBUTOR &&
            order?.bonus_point! > 0 ? (
              <PRow style={{ justifyContent: 'space-between' }}>
                <Caption style={{}}>Tổng điểm được nhận</Caption>
                <Caption style={{}}>
                  {getNumberString(order?.bonus_point)}
                  <MaterialCommunityIcons
                    name="star-circle"
                    size={widthScale(10)}
                    color="grey"
                  />
                </Caption>
              </PRow>
            ) : null}
          </View>
        ) : null}
        <View
          style={{
            backgroundColor: colors.background,
            marginTop: widthScale(10),
            paddingHorizontal: widthScale(12),
            paddingVertical: widthScale(8),
            paddingBottom: widthScale(50),
          }}
        >
          <PRow>
            <MaterialCommunityIcons
              name="currency-usd-circle"
              color={colors.accent}
              size={widthScale(20)}
              style={{ alignSelf: 'flex-start', top: widthScale(4) }}
            />
            <PRow
              style={{
                flex: 1,
                marginLeft: widthScale(8),
                justifyContent: 'space-between',
              }}
            >
              <Text style={{}}>Phương thức thanh toán</Text>
              <Caption style={{}}>{order?.payment_type === 'COD'}</Caption>
            </PRow>
          </PRow>
          <PRow
            style={{
              justifyContent: 'space-between',
              marginTop: widthScale(8),
            }}
          >
            <Caption style={{}}>Tổng tiền hàng</Caption>
            <Caption style={{}}>{getNumberString(order?.fee_total)}</Caption>
          </PRow>
          <PRow style={{ justifyContent: 'space-between' }}>
            <Caption style={{}}>Tổng tiền phí vận chuyển</Caption>
            <Caption style={{}}>{getNumberString(order?.fee_ship)}</Caption>
          </PRow>
        </View>

        {status.status === ActivityOrder.PROCESSING ? (
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              marginVertical: widthScale(16),
              borderWidth: widthScale(1),
            }}
          >
            <Button style={{}} onPress={updateOrder}>
              Hủy đơn hàng
            </Button>
          </View>
        ) : null}
      </View>
    );
  };
  return (
    <FixedContainer edges={edges}>
      <CustomHeader
        isLoading={isLoading}
        showBackButton
        title="Thông tin đơn hàng"
      />
      <PFlatList
        data={orderItems?.slice(0, viewAll ? undefined : 2)}
        renderItem={_renderItem as any}
        contentContainerStyle={{
          backgroundColor: colors.background2,
          flexGrow: 1,
        }}
        ListHeaderComponent={ListHeaderComponent}
        ListFooterComponent={ListFooterComponent}
      />
      <PRow
        style={{
          justifyContent: 'center',
          borderTopWidth: 2,
          borderColor: colors.background2,
        }}
      >
        <View style={{ alignItems: 'center', top: widthScale(6) }}>
          <Paragraph>Tổng thanh toán</Paragraph>
          <Title
            style={{
              color: colors.accent,
              fontFamily: pFont.FONT2,
              top: widthScale(-8),
            }}
          >
            {getNumberString(order?.total_cost)}
          </Title>
        </View>
      </PRow>
    </FixedContainer>
  );
}

export default InfoOrder;
