import React, { useEffect, useState } from 'react';
import { Alert, Image, View } from 'react-native';
import { Button, Caption, Paragraph, Subheading, Text, Title, useTheme } from 'react-native-paper';
import { Edge, useSafeAreaInsets } from 'react-native-safe-area-context';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { IMAGE } from '~/assets/imagePath';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { PFlatList } from '~components/PFlatList';
import { PRow } from '~components/PRow';
import PSpinner from '~components/PSpinner';
import PTouchableOpacity from '~components/PTouchableOpacity';
import { AddressProps, CartProps, OrderCart, PreOrderCallbackProps } from '~constants/types';
import { RootStackScreenProps } from '~navigators/root-stack';
import { ROUTE_KEY } from '~navigators/router-key';
import { clearCart } from '~reducers/cartReducer';
import { setVoucher } from '~reducers/userReducer';
import { useAddOrderMutation, usePreOrderMutation } from '~services/api_order_services';
import { useAppDispatch, useAppSelector } from '~store/storeHooks';
import { widthScale, windowWidth } from '~styles/scaling-utils';
import { pFont } from '~styles/typography';
import { myMessageErrorOrder } from '~utils/myMessage';
import { getNumberString, getNumberWithCommas, getPriceWithCommas } from '~utils/numberUtils';

const edges: Edge[] = ['right', 'left', 'bottom'];

function Payment(props: RootStackScreenProps<'Payment'>) {
  const { navigation, route } = props;
  const dispatch = useAppDispatch();
  const cart = route.params?.cart || [];
  const { colors, dark } = useTheme();
  const [viewAll, setViewAll] = useState(false);
  const userReducer = useAppSelector((state) => state.userReducer.user);
  const voucher_id = useAppSelector((state) => state.userReducer.voucher);
  const insets = useSafeAreaInsets();

  const [addOrder, { isLoading: addOrderLoading }] = useAddOrderMutation();
  const [preOrder, { isLoading: preOrderLoading }] = usePreOrderMutation();

  const [dataAddress, setDataAddress] = useState<AddressProps | null>(
    (userReducer?.defaultAddress?.id ? userReducer?.defaultAddress : null) || null
  );

  const [dataPreOrder, setDataPreOrder] = useState<PreOrderCallbackProps>();

  const itemCart: OrderCart[] = cart.map((e) => {
    return {
      product_id: e.id,
      quantity: e.quantity,
    };
  });

  useEffect(() => {
    if (itemCart && dataAddress?.id) {
      PSpinner.show();
      preOrder({
        voucher_id: voucher_id,
        shop_id: 'ad8ed960-f042-11eb-b072-b3c81d13f9f0',
        address_id: dataAddress.id,
        payment_type: 'COD',
        items: itemCart,
      })
        .unwrap()
        .then((payload) => {
          setDataPreOrder(payload);
        })
        .catch((error) => {
          myMessageErrorOrder({
            message: 'Có lỗi xãy ra khi đặt hàng',
            description: error?.data?.message,
            insets,
          });
        })
        .finally(() => {
          PSpinner.hide();
        });
    }
  }, [cart, dataAddress]);

  const onPayment = async () => {
    if (dataAddress?.id) {
      PSpinner.show();
      await addOrder({
        voucher_id: voucher_id,
        shop_id: 'ad8ed960-f042-11eb-b072-b3c81d13f9f0',
        address_id: dataAddress.id,
        payment_type: 'COD',
        items: itemCart,
      })
        .unwrap()
        .then(() => {
          dispatch(clearCart());
          dispatch(setVoucher(null));
          navigation.navigate(ROUTE_KEY.PaymentSuccess);
        })
        .catch((error) => {
          myMessageErrorOrder({
            message: 'Có lỗi xãy ra khi đặt hàng',
            description: error?.data?.message,
            insets,
          });
        })
        .finally(() => {
          PSpinner.hide();
        });
    } else {
      Alert.alert('Vui lòng kiểm tra địa chỉ nhận hàng');
    }
  };
  const onChangeAddress = (data?: AddressProps) => {
    if (data === undefined) {
      setDataAddress(null);
    } else {
      data && setDataAddress(data);
    }
  };

  const _renderItem = ({ item }: { item: CartProps }) => {
    return (
      <PRow
        style={{
          alignItems: 'flex-start',
          paddingLeft: widthScale(12),
          paddingVertical: widthScale(10),
        }}
      >
        <Image
          source={{ uri: item.thumbnail || '' }}
          style={{
            width: widthScale(80),
            height: widthScale(80),
            marginRight: widthScale(8),
          }}
          resizeMode="contain"
        />
        <View style={{ width: '72%', justifyContent: 'space-between' }}>
          <Text style={{ minHeight: widthScale(40) }} numberOfLines={2}>
            {item.title}
          </Text>
          <PRow style={{ marginLeft: widthScale(4), justifyContent: 'space-between' }}>
            {item.discount_price! > 0 ? (
              <PRow>
                <Text
                  style={{
                    color: colors.disabled,
                    fontFamily: pFont.FONT2,
                    textDecorationLine: 'line-through',
                    top: widthScale(1),
                    marginRight: widthScale(4),
                  }}
                >
                  {getNumberString(item?.price)}
                </Text>
                <Subheading style={{ fontFamily: pFont.FONT2, color: colors.accent }}>
                  {getNumberString(item.discount_price)}
                </Subheading>
              </PRow>
            ) : (
              <Subheading style={{ color: colors.accent, fontFamily: pFont.FONT2 }}>
                {item?.price ? getNumberString(item?.price) : 'Liên hệ'}
              </Subheading>
            )}
            <Caption style={{ top: widthScale(4) }}>x{item.quantity}</Caption>
          </PRow>
        </View>
      </PRow>
    );
  };
  const data = dataPreOrder?.order_items?.slice(0, viewAll ? undefined : 2);
  return (
    <FixedContainer edges={edges}>
      <CustomHeader showBackButton title="Thanh toán" />
      {dataAddress?.id ? (
        <>
          <PFlatList
            data={data}
            renderItem={_renderItem as any}
            keyExtractor={(e: any) => e?.product_id}
            contentContainerStyle={{
              paddingTop: widthScale(16),
              backgroundColor: colors.background2,
            }}
            ListHeaderComponent={() => (
              <PTouchableOpacity
                onPress={() => navigation.navigate(ROUTE_KEY.DeliveryAddress, { onChangeAddress })}
                style={{ backgroundColor: colors.background, paddingTop: widthScale(8) }}
              >
                <PRow
                  style={{
                    paddingHorizontal: widthScale(12),
                    borderBottomWidth: widthScale(2),
                    borderColor: colors.disabled,
                    paddingBottom: widthScale(8),
                    marginBottom: widthScale(16),
                  }}
                >
                  <MaterialCommunityIcons
                    name="map-marker"
                    color={colors.accent}
                    size={widthScale(20)}
                    style={{ alignSelf: 'flex-start' }}
                  />
                  <View style={{ flex: 1, marginLeft: widthScale(8) }}>
                    <Text style={{}}>Địa chỉ nhận hàng</Text>
                    {dataAddress ? (
                      <Paragraph style={{}}>
                        {dataAddress?.fullname} | ({dataAddress?.phone})
                      </Paragraph>
                    ) : (
                      <Paragraph style={{ color: colors.error }}>
                        Vui lòng thêm địa chỉ nhận hàng
                      </Paragraph>
                    )}

                    <Caption style={{}}>{dataAddress?.address}</Caption>
                    <Caption style={{}}>
                      {dataAddress?.ward &&
                        dataAddress.district &&
                        dataAddress.city &&
                        `${dataAddress?.ward || ''}, ${dataAddress?.district || ''}, ${
                          dataAddress?.city || ''
                        }`}
                    </Caption>
                  </View>
                  <MaterialCommunityIcons
                    name="chevron-right"
                    color={colors.accent}
                    size={widthScale(24)}
                  />
                </PRow>
              </PTouchableOpacity>
            )}
            ListFooterComponent={() => (
              <View style={{}}>
                {cart.length > 2 ? (
                  <View
                    style={{
                      width: '100%',
                      alignSelf: 'center',
                      backgroundColor: colors.background,
                    }}
                  >
                    <Button onPress={() => setViewAll(!viewAll)}>
                      {viewAll ? 'Thu gọn' : `Xem thêm (${cart.length - 2})`}
                    </Button>
                  </View>
                ) : null}
                {dataPreOrder?.order?.discount! > 0 || dataPreOrder?.order?.bonus_point! > 0 ? (
                  <View
                    style={{
                      backgroundColor: colors.background,
                      marginTop: widthScale(10),
                      paddingHorizontal: widthScale(12),
                      paddingVertical: widthScale(8),
                    }}
                  >
                    <PRow style={{ flex: 1 }}>
                      <MaterialCommunityIcons
                        name="account-star"
                        color={colors.accent}
                        size={widthScale(20)}
                        style={{ alignSelf: 'flex-start' }}
                      />
                      <Text style={{ marginLeft: widthScale(8) }}>
                        {userReducer?.is_collab ? 'Ưu đãi cộng tác viên' : 'Ưu đãi'}
                      </Text>
                    </PRow>
                    {dataPreOrder?.order?.discount! > 0 ? (
                      <PRow style={{ justifyContent: 'space-between', marginTop: widthScale(8) }}>
                        <Caption style={{}}>Tổng tiền được giảm</Caption>
                        <Caption style={{}}>
                          {getNumberString(dataPreOrder?.order?.discount)}
                        </Caption>
                      </PRow>
                    ) : null}
                    {!userReducer?.is_collab && dataPreOrder?.order?.bonus_point! > 0 ? (
                      <PRow style={{ justifyContent: 'space-between' }}>
                        <Caption style={{}}>Tổng điểm được nhận</Caption>
                        <Caption style={{}}>
                          {getNumberWithCommas(dataPreOrder?.order?.bonus_point)}
                          <MaterialCommunityIcons
                            name="star-circle"
                            size={widthScale(10)}
                            color="grey"
                          />
                        </Caption>
                      </PRow>
                    ) : null}
                  </View>
                ) : null}

                <View
                  style={{
                    backgroundColor: colors.background,
                    marginTop: widthScale(10),
                    paddingHorizontal: widthScale(12),
                    paddingVertical: widthScale(8),
                  }}
                >
                  <PRow>
                    <MaterialCommunityIcons
                      name="currency-usd-circle"
                      color={colors.accent}
                      size={widthScale(20)}
                      style={{ alignSelf: 'flex-start', top: widthScale(4) }}
                    />
                    <PRow
                      style={{
                        flex: 1,
                        marginLeft: widthScale(8),
                        justifyContent: 'space-between',
                      }}
                    >
                      <Text style={{}}>Phương thức thanh toán</Text>
                      <Caption style={{}}>COD</Caption>
                    </PRow>
                  </PRow>
                  <PRow style={{ justifyContent: 'space-between', marginTop: widthScale(8) }}>
                    <Caption style={{}}>Tổng tiền hàng</Caption>
                    <Caption style={{}}>{getNumberString(dataPreOrder?.order?.fee_total)}</Caption>
                  </PRow>
                  <PRow style={{ justifyContent: 'space-between' }}>
                    <Caption style={{}}>Tổng tiền phí vận chuyển</Caption>
                    <Caption style={{}}>{getNumberString(dataPreOrder?.order?.fee_ship)}</Caption>
                  </PRow>
                  {!!dataPreOrder?.order?.discount && dataPreOrder?.order?.discount > 0 ? (
                    <PRow style={{ justifyContent: 'space-between' }}>
                      <Caption style={{}}>Tổng cộng giảm giá</Caption>
                      <Caption style={{}}>{getNumberString(dataPreOrder?.order?.discount)}</Caption>
                    </PRow>
                  ) : null}
                  <PRow style={{ justifyContent: 'space-between', marginTop: widthScale(2) }}>
                    <Subheading style={{ fontWeight: 'bold' }}>Tổng tiền thanh toán</Subheading>
                    <Caption style={{}}>{getNumberString(dataPreOrder?.order?.total_cost)}</Caption>
                  </PRow>
                </View>
                <PRow
                  style={{
                    backgroundColor: colors.background,
                    marginTop: widthScale(10),
                    marginBottom: widthScale(30),
                    paddingHorizontal: widthScale(12),
                    paddingVertical: widthScale(8),
                  }}
                >
                  <MaterialCommunityIcons
                    name="newspaper-variant-multiple-outline"
                    color={colors.accent}
                    size={widthScale(20)}
                    style={{ alignSelf: 'flex-start', top: widthScale(4) }}
                  />

                  <Text style={{ marginLeft: widthScale(8) }}>
                    Nhấn đặt hàng đồng nghĩa là bạn đồng ý tuân theo{' '}
                    <Text
                      style={{ color: colors.accent }}
                      onPress={() =>
                        navigation.navigate(ROUTE_KEY.MyWebView, {
                          uri: 'https://server.hitek.com.vn:5001/api/v1/setting/policy',
                          title: 'Điều khoản và Chính sách',
                        })
                      }
                    >
                      điều khoản của chúng tôi
                    </Text>
                  </Text>
                </PRow>
              </View>
            )}
          />

          <PRow
            style={{
              justifyContent: 'flex-end',
              borderTopWidth: 2,
              borderColor: colors.background2,
            }}
          >
            <View style={{ alignItems: 'flex-end', top: widthScale(6) }}>
              <Paragraph>Tổng thanh toán</Paragraph>
              <Title
                style={{
                  color: colors.accent,
                  fontFamily: pFont.FONT2,
                  top: widthScale(-8),
                }}
              >
                {getPriceWithCommas(dataPreOrder?.order?.total_cost)}
              </Title>
            </View>
            <Button
              style={{
                borderRadius: 0,
                marginLeft: widthScale(12),
                backgroundColor: colors.accent,
              }}
              contentStyle={{ height: widthScale(60) }}
              onPress={() => {
                if (!dataAddress) {
                  Alert.alert('Vui lòng thêm địa chỉ nhận hàng');
                } else {
                  onPayment();
                }
              }}
              mode="contained"
            >
              Hoàn tất
            </Button>
          </PRow>
        </>
      ) : (
        <PTouchableOpacity
          onPress={() =>
            dataAddress
              ? navigation.navigate(ROUTE_KEY.DeliveryAddress, { onChangeAddress })
              : navigation.navigate(ROUTE_KEY.CreateDeliveryAddress, {
                  firstAddress: true,
                  onChangeAddress,
                })
          }
          style={{
            flex: 1,
            backgroundColor: colors.background,
            paddingTop: widthScale(8),
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Image source={IMAGE.logo2} resizeMode="contain" style={{ height: windowWidth(0.3) }} />
          <MaterialCommunityIcons
            name="map-marker"
            color={colors.accent}
            size={widthScale(60)}
            style={{}}
          />
          <Text style={{ marginVertical: widthScale(14) }}>
            Ôi! Chúng tôi chưa biết nên giao đến đâu?
          </Text>
          <Paragraph style={{ color: colors.error }}>Vui lòng thêm địa chỉ nhận hàng</Paragraph>
        </PTouchableOpacity>
      )}
    </FixedContainer>
  );
}

export default Payment;
