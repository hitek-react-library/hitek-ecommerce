import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { Image, Platform, StyleSheet, View } from 'react-native';
import { ActivityIndicator, Text, Title, useTheme } from 'react-native-paper';
import { Edge } from 'react-native-safe-area-context';

import { IMAGE } from '~/assets/imagePath';
import CustomHeader from '~components/custom-header';
import FixedContainer from '~components/fixed-container';
import { PScrollView } from '~components/PScrollView';
import PTouchableOpacity from '~components/PTouchableOpacity';
import { DATA_NEWS } from '~constants/dataDemo';
import { PostProps, PostType } from '~constants/types';
import { BottomTabScreenProps } from '~navigators/bottom-tab';
import { ROUTE_KEY } from '~navigators/router-key';
import { useListPostByTypeQuery } from '~services/api_post_services';
import { useAppDispatch } from '~store/storeHooks';
import { screenWidth, widthScale, windowWidth } from '~styles/scaling-utils';
import { pShadow } from '~styles/style';

const edges: Edge[] = ['right', 'left'];
const data1 = DATA_NEWS.slice(4, 8);
const data2 = DATA_NEWS?.slice(8, 12);

const Home = (props: BottomTabScreenProps<'Home'>) => {
  const { colors, dark } = useTheme();
  const { navigation } = props;
  const dispatch = useAppDispatch();
  const {
    data: dataBanner,
    isLoading: isLoadingBanner,
    isSuccess: isSuccessBanner,
  } = useListPostByTypeQuery({ type: PostType.SALE, limit: 10 });
  const [activeSlide, setActiveSlide] = useState(0);

  function _renderItem(
    { item, index }: { item: PostProps; index: number },
    parallaxProps: any
  ) {
    return (
      <PTouchableOpacity
        onPress={() => {
          navigation.navigate(ROUTE_KEY.DetailPost, item);
        }}
      >
        <View style={styles.item2}>
          <ParallaxImage
            source={{ uri: item.image }}
            containerStyle={styles.imageContainer}
            parallaxFactor={0.4}
            {...parallaxProps}
          />
        </View>
      </PTouchableOpacity>
    );
  }

  return (
    <FixedContainer edges={edges}>
      <CustomHeader
        customCenter={
          <Image
            source={IMAGE.logoWhite}
            style={{ width: '100%', height: '100%' }}
            resizeMode="contain"
          />
        }
        iconRightMenu="cart-outline"
        onRightMenuPress={() => navigation.navigate(ROUTE_KEY.Cart)}
      />
      <PScrollView isTabScreen bounces={false}>
        {isSuccessBanner && dataBanner ? (
          <>
            {/* <Carousel
              autoplay={true}
              loop
              hasParallaxImages={true}
              data={dataBanner ?? []}
              initialNumToRender={10}
              sliderWidth={screenWidth()}
              inactiveSlideScale={1}
              pagingEnabled
              itemWidth={screenWidth()}
              renderItem={_renderItem as any}
              containerCustomStyle={{
                marginTop: widthScale(12),
                marginBottom: widthScale(30),
              }}
              onSnapToItem={(index) => setActiveSlide(index)}
            />
            <Pagination
              dotsLength={dataBanner?.length}
              activeDotIndex={activeSlide}
              containerStyle={{
                position: 'absolute',
                alignSelf: 'center',
                top: widthScale(170),
              }}
              dotStyle={{
                width: widthScale(14),
                height: widthScale(14),
                borderRadius: widthScale(14),
                marginHorizontal: widthScale(-4),
                backgroundColor: colors.accent,
                borderWidth: widthScale(3),
                borderColor: colors.primary,
              }}
              inactiveDotOpacity={0.8}
              inactiveDotScale={0.6}
            /> */}
          </>
        ) : isLoadingBanner ? (
          <View
            style={[
              styles.item2,
              styles.imageContainer,
              {
                justifyContent: 'center',
                alignSelf: 'center',
                marginTop: widthScale(8),
              },
            ]}
          >
            <ActivityIndicator></ActivityIndicator>
          </View>
        ) : null}
        <ItemCategory type={PostType.PROMOTION} title="Khuyến mãi" />
        <ItemCategory type={PostType.NEWS} title="Tin tức" />
      </PScrollView>
    </FixedContainer>
  );
};

export default Home;

function ItemCategory({ type, title }: { type: PostType; title?: string }) {
  const navigation = useNavigation();
  const { colors, dark } = useTheme();
  const { data, isLoading, isSuccess } = useListPostByTypeQuery({
    type,
    limit: 10,
  });
  function _renderItem(
    { item, index }: { item: PostProps; index: number },
    parallaxProps: any
  ) {
    return (
      <View
        style={[pShadow.BLUR0, { backgroundColor: 'transparent', margin: 3 }]}
      >
        <PTouchableOpacity
          onPress={() => {
            navigation.navigate(ROUTE_KEY.DetailPost, item);
          }}
          style={[styles.item, { backgroundColor: colors.background2 }]}
        >
          <ParallaxImage
            source={{ uri: item.image }}
            containerStyle={styles.imageContainer}
            style={{ ...StyleSheet.absoluteFillObject, resizeMode: 'contain' }}
            {...parallaxProps}
          />
          <Text
            style={{
              paddingHorizontal: widthScale(8),
              paddingVertical: widthScale(12),
            }}
            numberOfLines={2}
          >
            {item.title}
          </Text>
        </PTouchableOpacity>
      </View>
    );
  }
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingHorizontal: widthScale(12),
          paddingBottom: widthScale(4),
        }}
      >
        <Title>{title}</Title>
        {data?.length === 10 ? (
          <Text
            onPress={() =>
              navigation.navigate(ROUTE_KEY.NewsMore, { title, type })
            }
          >
            Xem thêm {'>'}
          </Text>
        ) : null}
      </View>
      {isSuccess && data ? (
        <Carousel
          hasParallaxImages={true}
          data={data}
          sliderWidth={windowWidth()}
          sliderHeight={widthScale(180)}
          inactiveSlideScale={0.95}
          itemWidth={windowWidth(0.7)}
          activeSlideAlignment="start"
          renderItem={_renderItem as any}
          containerCustomStyle={{
            marginBottom: widthScale(16),
            paddingHorizontal: widthScale(12),
          }}
        />
      ) : isLoading ? (
        <View
          style={[
            pShadow.BLUR0,
            {
              backgroundColor: 'transparent',
              margin: 3,
              marginBottom: widthScale(16),
              paddingHorizontal: widthScale(12),
            },
          ]}
        >
          <View
            style={[
              styles.item,
              {
                backgroundColor: colors.background2,
                justifyContent: 'center',
                alignItems: 'center',
              },
            ]}
          >
            <ActivityIndicator></ActivityIndicator>
          </View>
        </View>
      ) : null}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    width: screenWidth(0.7),
    height: screenWidth(0.6),
    borderRadius: widthScale(8),
    overflow: 'hidden',
  },
  item2: {
    width: screenWidth(0.95),
    alignSelf: 'center',
    height: widthScale(180),
    borderRadius: widthScale(8),
    overflow: 'hidden',
  },
  imageContainer: {
    flex: 1,
    marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
    backgroundColor: '#0002',
  },
});
