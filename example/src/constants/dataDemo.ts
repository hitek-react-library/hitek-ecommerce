import { AgentProps, PostProps, ProductProps } from '~constants/types';

export const CURRENT_LOCATION = {
  longitude: 106.68969190000007,
  latitude: 10.763525,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421,
};

export const DATA_NEWS: PostProps[] = [
  {
    id: 1,
    type: 'news',
    thumbnail:
      'https://nongsanlangbiang.com/thumb/365x205/1/upload/news/an-1-cu-toi-moi-ngay-tang-cuong-suc-de-khang-ngan-ngua-covid.jpg',
    title: 'Ăn 1 củ tỏi mỗi ngày - tăng cường sức đề kháng, ngăn ngừa covid',
    url: 'https://nongsanlangbiang.com/tin-tuc/an-1-cu-toi-moi-ngay-tang-cuong-suc-de-khang-ngan-ngua-covid-495.html',
    date: '05-06-2021 03:37',
    view: '113',
    description:
      'Ngoài các biện pháp phòng ngừa như đeo khẩu trang, rửa tay thường xuyên, tránh đến nơi đông người… như khuyến cáo của Bộ Y tế ...',
  },
  {
    id: 2,
    type: 'status',
    thumbnail:
      'https://nongsanlangbiang.com/thumb/365x205/1/upload/news/cong-thuc-lam-xot-rau-cu-tartar.jpg',
    title: 'Công thức làm xốt rau củ TARTAR',
    url: 'https://nongsanlangbiang.com/tin-tuc/cong-thuc-lam-xot-rau-cu-tartar-494.html',
    date: '03-06-2021 09:39',
    view: '194',
    description:
      'Xốt rau củ TARTAR đơn giản, dễ làm, bạn có thể tự làm tại nhà để làm món trộn, kẹp bánh mì hay chấm khoai tây, đồ ...',
  },
  {
    id: 3,
    type: 'status',
    thumbnail:
      'https://nongsanlangbiang.com/thumb/365x205/1/upload/news/10-loi-ich-suc-khoe-dang-ngac-nhien-cua-can-tay.jpg',
    title: '10 lợi ích sức khỏe đáng ngạc nhiên của cần tây',
    url: 'https://nongsanlangbiang.com/tin-tuc/10-loi-ich-suc-khoe-dang-ngac-nhien-cua-can-tay-493.html',
    date: '03-06-2021 08:56',
    view: '84',
    description:
      'Cần tây là nguồn cung cấp chất chống oxy hóa và các enzym có lợi, ngoài ra còn cung cấp thêm các vitamin và khoáng chất, chẳng hạn như ...',
  },
  {
    id: 4,
    type: 'news',
    thumbnail:
      'https://nongsanlangbiang.com/thumb/365x205/1/upload/news/8-phat-hien-lich-su-ve-toi.jpg',
    title: '8 phát hiện lịch sử về tỏi bạn nên biết',
    url: 'https://nongsanlangbiang.com/tin-tuc/8-phat-hien-lich-su-ve-toi-ban-nen-biet-492.html',
    date: '28-05-2021 09:29',
    view: '79',
    description:
      'Richard S. Rivlin, (Rivlin 2001) đã nghiên cứu các văn bản y học cổ đại của Ai Cập, Hy Lạp, La Mã, Trung Quốc và Ấn Độ và cách mà ...',
  },
  {
    id: 5,
    type: 'status',
    thumbnail:
      'https://nongsanlangbiang.com/thumb/365x205/1/upload/news/toi-vacin-tu-nhien-cua-nguoi-ai-cap-co-dai-phong-ngua-dich-benh.jpg',
    title: 'Tỏi - Vacin tự nhiên của người Ai Cập cổ đại phòng ngừa dịch bệnh',
    url: 'https://nongsanlangbiang.com/tin-tuc/toi-vacin-tu-nhien-cua-nguoi-ai-cap-co-dai-phong-ngua-dich-benh-491.html',
    date: '28-05-2021 08:50',
    view: '169',
    description:
      'Trong ngôi mộ cổ xưa ở Ai Cập, người ta đã tìm thấy đơn thuốc làm từ tỏi. Còn ở Trung Quốc, ngay từ năm 2600 trước Công nguyên ...',
  },
  {
    id: 6,
    type: 'news',
    thumbnail:
      'https://nongsanlangbiang.com/thumb/365x205/1/upload/news/ca-phe-ngon-la-ca-phe-hop-gu.jpg',
    title:
      'LBA không cam kết cà phê NGON, nhưng đảm bảo cung cấp cà phê SẠCH đảm bảo CHUẨN VỊ tự nhiên',
    url: 'https://nongsanlangbiang.com/tin-tuc/lba-khong-cam-ket-ca-phe-ngon-nhung-dam-bao-cung-cap-ca-phe-sach-dam-bao-chuan-vi-tu-nhien-490.html',
    date: '24-05-2021 10:28',
    view: '98',
    description:
      'Nông sản Langbiang không dám cam kết cà phê của chúng tôi ngon tuyệt vời. Bởi lẽ những người uống cà phê đều có GU riêng của mình. ...',
  },
  {
    id: 7,
    type: 'news',
    thumbnail:
      'https://nongsanlangbiang.com/thumb/365x205/1/upload/news/dau-me-cong-dung-va-nhung-loi-ich-suc-khoe-to-lon-ma-chung-mang-lai.jpg',
    title: 'Dầu mè, công dụng và những lợi ích sức khỏe to lớn mà chúng mang lại',
    url: 'https://nongsanlangbiang.com/tin-tuc/dau-me-cong-dung-va-nhung-loi-ich-suc-khoe-to-lon-ma-chung-mang-lai-489.html',
    date: '17-05-2021 09:58',
    view: '155',
    description:
      'Dầu mè là một thành phần mạnh mẽ có từ hàng ngàn năm trước và được biết đến với khả năng tăng hương vị và lợi ích cho sức ...',
  },
  {
    id: 8,
    type: 'news',
    thumbnail:
      'https://nongsanlangbiang.com/thumb/365x205/1/upload/news/tang-cuong-suc-de-khang-danh-bay-covid19.jpeg',
    title: 'Tăng cường sức đề kháng - Đánh bay Covid-19',
    url: 'https://nongsanlangbiang.com/tin-tuc/tang-cuong-suc-de-khang-danh-bay-covid19-488.html',
    date: '15-05-2021 09:40',
    view: '160',
    description:
      'Cuộc chiến chống Covid-19 vẫn chưa có hồi kết khi “đại dịch” tiếp tục tốc độ lây lan nhanh chưa từng có và hiện chưa có thuốc ...',
  },
  {
    id: 9,
    type: 'news',
    thumbnail:
      'https://nongsanlangbiang.com/thumb/365x205/1/upload/news/cac-loai-thuc-pham-giau-vitamin-c-co-the-tang-cuong-mien-dich-ngan-ngua-covid19.jpg',
    title: ' Các loại thực phẩm giàu vitamin C giúp tăng cường miễn dịch - Ngăn ngừa covid-19',
    url: 'https://nongsanlangbiang.com/tin-tuc/cac-loai-thuc-pham-giau-vitamin-c-giup-tang-cuong-mien-dich-ngan-ngua-covid19-487.html',
    date: '12-05-2021 03:58',
    view: '155',
    description:
      'Nâng cao sức đề kháng của bản thân chính là biện pháp bảo vệ sức khỏe hiệu quả nhất, đặc biệt là trong mùa dịch. Phòng dịch ...',
  },
  {
    id: 10,
    type: 'news',
    thumbnail:
      'https://nongsanlangbiang.com/thumb/365x205/1/upload/news/hanh-tim-co-loi-nhat-cho-dieu-gi-co-the-lam-gi-voi-hanh-tim-73.jpg',
    title: 'Hành tím-Thực phẩm tăng cường hệ miễn dịch',
    url: 'https://nongsanlangbiang.com/tin-tuc/hanh-timthuc-pham-tang-cuong-he-mien-dich-486.html',
    date: '10-05-2021 10:18',
    view: '153',
    description:
      'rong lịch sử, hẹ tây đã được sử dụng cho cả đặc tính dinh dưỡng và hương thơm của nó trong các món ăn Ấn Độ, Châu Á, Pháp và ...',
  },
  {
    id: 11,
    type: 'news',
    thumbnail:
      'https://nongsanlangbiang.com/thumb/365x205/1/upload/news/7-thuc-pham-dung-moi-ngay-giup-tang-cuong-suc-de-khang.jpg',
    title: '7 thực phẩm dùng mỗi ngày giúp tăng cường sức đề kháng phòng ngừa Covid-19',
    url: 'https://nongsanlangbiang.com/tin-tuc/7-thuc-pham-dung-moi-ngay-giup-tang-cuong-suc-de-khang-phong-ngua-covid19-485.html',
    date: '08-05-2021 08:24',
    view: '293',
    description:
      'Dịch bệnh Covid lại bùng phát đợt thứ 4 gây lo âu cho mọi người. Bên cạnh việc hạn chế tiếp xúc, áp dụng 5k (Khẩu trang – Khử ...',
  },
  {
    id: 12,
    type: 'news',
    thumbnail:
      'https://nongsanlangbiang.com/thumb/365x205/1/upload/news/lycopene-chat-chong-oxy-hoa-manh-me-giup-ngan-ngua-ung-thu-hieu-qua.png',
    title: 'Lycopene - Chất chống oxy hóa mạnh mẽ giúp ngăn ngừa ung thư hiệu quả',
    url: 'https://nongsanlangbiang.com/tin-tuc/lycopene-chat-chong-oxy-hoa-manh-me-giup-ngan-ngua-ung-thu-hieu-qua-484.html',
    date: '06-05-2021 03:32',
    view: '129',
    description:
      'Lycopene là chất dinh dưỡng thực vật, là một chất chống oxy hóa có tác dụng ngăn ngừa ung thư vô cùng mạnh mẽ. Nó thường được ...',
  },
];
export const DATA_AGENT: AgentProps[] = [
  {
    id: 2125,
    region: 'south',
    name: 'LBA - Nguyễn Thái Học',
    longitude: 106.695783,
    latitude: 10.766951,
    address: '83 Nguyễn Thái Học, Cầu Ông Lãnh, Quận 1, Hồ Chí Minh',
    phoneNumber: '02866576028',
    image: 'https://nongsanlangbiang.com/upload/images/037535.jpg',
    thumbnail: 'https://img.foodbook.vn/images/fb/pos/2017-06-21-17_12_23_thumbtocoHD.jpg',
    website: 'http://www.tocotocotea.com/',
    geometry: { type: 'Point', coordinates: [106.695955, 10.766983] },
    createdAt: '2019-07-11T04:05:12.968Z',
    updatedAt: '2021-03-30T01:47:15.055Z',
    disabledAt: null,
  },
  {
    id: 13620,
    region: 'south',
    name: 'LBA - 50 Lê Quốc Hưng',
    longitude: 106.703338,
    latitude: 10.764866,
    address: '50 Lê Quốc Hưng, Phường 12, Quận 4, Hồ Chí Minh',
    phoneNumber: '0906720828',
    image: 'https://nongsanlangbiang.com/upload/images/037535.jpg',
    thumbnail: null,
    website: null,
    geometry: { type: 'Point', coordinates: [106.703338, 10.764866] },
    createdAt: '2019-12-24T17:10:35.542Z',
    updatedAt: '2021-03-30T01:58:16.510Z',
    disabledAt: null,
  },
  {
    id: 10012,
    region: 'south',
    name: 'LBA - 23 Tôn Đản, Quận 4, Hồ Chí Minh',
    longitude: 106.707797,
    latitude: 10.761245,
    address: '23 Đường Tôn Đản, Phường 13, Quận 4, Hồ Chí Minh',
    phoneNumber: '0919364433',
    image: 'https://nongsanlangbiang.com/upload/images/037535.jpg',
    thumbnail: 'https://img.foodbook.vn/images/20190216/2019-02-16-11_42_36_tstc-2.jpg',
    website: '',
    geometry: { type: 'Point', coordinates: [106.707808, 10.761308] },
    createdAt: '2019-07-11T04:05:14.235Z',
    updatedAt: '2021-03-30T02:52:10.311Z',
    disabledAt: null,
  },
  {
    id: 2560,
    region: 'south',
    name: 'LBA - 97A Nguyễn Cư Trinh',
    longitude: 106.68969190000007,
    latitude: 10.763525,
    address: '97A Nguyễn Cư Trinh, Quận 1, Hồ Chí Minh ',
    phoneNumber: '02838372939',
    image: 'https://nongsanlangbiang.com/upload/images/037535.jpg',
    thumbnail: 'https://img.foodbook.vn/images/fb/pos/2017-07-04-09_22_13_thumbtocoHD.jpg',
    website: 'http://www.tocotocotea.com/',
    geometry: { type: 'Point', coordinates: [106.689692, 10.763526] },
    createdAt: '2019-07-11T04:05:13.065Z',
    updatedAt: '2021-02-22T06:25:13.567Z',
    disabledAt: null,
  },
  {
    id: 2702,
    region: 'south',
    name: 'LBA - Đường 41',
    longitude: 106.6999876,
    latitude: 10.7580508,
    address: 'Số 2, Đường 41, Quận 4, HCM',
    phoneNumber: '0901196411',
    image: 'https://nongsanlangbiang.com/upload/images/037535.jpg',
    thumbnail: 'https://img.foodbook.vn/images/fb/pos/2017-07-18-16_25_13_thumbtocoHD.jpg',
    website: 'http://www.tocotocotea.com/',
    geometry: { type: 'Point', coordinates: [106.700341, 10.758326] },
    createdAt: '2019-07-11T04:05:13.521Z',
    updatedAt: '2021-03-30T02:17:03.758Z',
    disabledAt: null,
  },
  {
    id: 12980,
    region: 'south',
    name: 'LBA - 39 Ngô Tất Tố, P21, Bình Thạnh',
    longitude: 106.7103766,
    latitude: 10.7929547,
    address: '39 Ngô Tất Tố, P21, Bình Thạnh',
    phoneNumber: null,
    image: 'https://nongsanlangbiang.com/upload/images/037535.jpg',
    thumbnail: null,
    website: null,
    geometry: { type: 'Point', coordinates: [106.7103766, 10.7929547] },
    createdAt: '2020-06-05T04:04:46.722Z',
    updatedAt: '2021-03-19T09:02:24.942Z',
    disabledAt: null,
  },
  {
    id: 1482,
    region: 'south',
    name: 'LBA - Cao Thắng',
    longitude: 106.6826831,
    latitude: 10.7697654,
    address: '58 Cao Thắng, Phường 5, Quận 3, Hồ Chí Minh',
    phoneNumber: '02862909151',
    image: 'https://nongsanlangbiang.com/upload/images/037535.jpg',
    thumbnail: 'https://img.foodbook.vn/images/fb/pos/2017-06-21-17_18_58_thumbtocoHD.jpg',
    website: 'http://www.tocotocotea.com/',
    geometry: { type: 'Point', coordinates: [106.682769, 10.769686] },
    createdAt: '2019-07-11T04:05:13.014Z',
    updatedAt: '2021-03-30T01:52:11.140Z',
    disabledAt: null,
  },
  {
    id: 13251,
    region: 'south',
    name: 'LBA - 201 Điện Biên Phủ, Bình Thạnh',
    longitude: 106.7061096,
    latitude: 10.7998683,
    address: '201 Điện Biên Phủ, Phường 15, Bình Thạnh, Hồ Chí Minh',
    phoneNumber: '0901647013',
    image: 'https://nongsanlangbiang.com/upload/images/037535.jpg',
    thumbnail: '',
    website: '',
    geometry: { type: 'Point', coordinates: [106.69904, 10.796909] },
    createdAt: '2019-12-24T17:10:35.542Z',
    updatedAt: '2021-03-30T01:58:07.109Z',
    disabledAt: null,
  },
  {
    id: 17380,
    region: 'south',
    name: 'LBA - 157A Vạn Kiếp, P. 3, Q. Bình Thạnh, Tp. Hồ Chí Minh',
    longitude: 106.6937718,
    latitude: 10.7978813,
    address: '157A Vạn Kiếp, P. 3',
    phoneNumber: '0702270591',
    image: 'https://nongsanlangbiang.com/upload/images/037535.jpg',
    thumbnail: null,
    website: null,
    geometry: { type: 'Point', coordinates: [106.6937718, 10.7978813] },
    createdAt: '2020-03-23T07:43:29.378Z',
    updatedAt: '2021-03-30T02:07:17.218Z',
    disabledAt: null,
  },
  {
    id: 13711,
    region: 'south',
    name: 'LBA - 56 Nguyễn Khoái',
    longitude: 106.694694,
    latitude: 10.754813,
    address: '56 Nguyễn Khoái, Phường 2, Quận 4, Hồ Chí Minh',
    phoneNumber: '1900636936',
    image: 'https://nongsanlangbiang.com/upload/images/037535.jpg',
    thumbnail: null,
    website: null,
    geometry: { type: 'Point', coordinates: [106.694694, 10.754813] },
    createdAt: '2019-12-24T17:10:35.542Z',
    updatedAt: '2021-03-30T02:02:27.017Z',
    disabledAt: null,
  },
];
export const DATA_PRODUCTS: ProductProps[] = [
  {
    id: 0,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/bo-034-1kg.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/bo-034-loai-1-1kg-946.html',
    title: 'Bơ 034 (loại 1), 1kg',
    price: 55000,
    discount: null,
  },
  {
    id: 1,
    thumbnail:
      'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/thom-khom-dua-mat-1kg-304.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/thom-khom-dua-mat-1kg-945.html',
    title: 'Thơm (khóm, dứa) mật, 1kg',
    price: 30000,
    discount: null,
  },
  {
    id: 2,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/cai-dang-1kg.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/cai-dang-cai-be-xanh-1kg-933.html',
    title: 'Cải đắng (cải bẹ xanh) 1kg',
    price: 30000,
    discount: null,
  },
  {
    id: 3,
    thumbnail:
      'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/hanh-tay-tim-ti-hon-300g.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/hanh-tay-1kg-675.html',
    title: 'Hành tây 1kg',
    price: 35000,
    discount: null,
  },
  {
    id: 4,
    thumbnail:
      'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/chanh-day-hoang-gia-1kg-195.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/chanh-day-hoang-gia-1kg-929.html',
    title: 'Chanh dây Hoàng Gia 1kg',
    price: 150000,
    discount: null,
  },
  {
    id: 5,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/kho-qua-rung-1kg.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/kho-qua-rung-1kg-925.html',
    title: 'Khổ qua rừng 1kg',
    price: 55000,
    discount: 65000,
  },
  {
    id: 6,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/bi-ngo-non-1kg.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/bi-do-non-1kg-918.html',
    title: 'Bí đỏ non 1kg',
    price: 49000,
    discount: null,
  },
  {
    id: 7,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/su-tim-1kg.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/su-tim-1kg-916.html',
    title: 'Sú tim 1kg',
    price: 40000,
    discount: 45000,
  },
  {
    id: 8,
    thumbnail:
      'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/xa-lach-romaine-da-lat-1kg.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/xa-lach-da-lat-1kg-911.html',
    title: 'Xà lách Đà Lạt 1kg',
    price: 50000,
    discount: null,
  },
  {
    id: 9,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/bi-ho-lo-1kg.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/bi-ho-lo-1kg-908.html',
    title: 'Bí hồ lô 1kg',
    price: 45000,
    discount: 49000,
  },
  {
    id: 10,
    thumbnail:
      'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/sup-lo-xanh-bong-cai-xanh-1kg.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/sup-lo-xanh-bong-cai-xanh-1kg-905.html',
    title: 'Súp lơ xanh (bông cải xanh) 1kg',
    price: 45000,
    discount: 51000,
  },
  {
    id: 11,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/cu-den-1kg-692.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/cu-den-1kg-904.html',
    title: 'Củ dền 1kg',
    price: 32000,
    discount: 49000,
  },
  {
    id: 12,
    thumbnail:
      'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ot-chuong-xanh-do-vang-1kg.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/ot-chuong-xanh-do-vang-1kg-902.html',
    title: 'Ớt chuông Xanh, Đỏ, Vàng 1kg',
    price: 45000,
    discount: 55000,
  },
  {
    id: 13,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/cu-cai-trang-1kg.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/cu-cai-trang-1kg-900.html',
    title: 'Củ cải trắng 1kg',
    price: 25000,
    discount: null,
  },
  {
    id: 14,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/su-hao-1kg.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/su-hao-1kg-899.html',
    title: 'Su hào 1kg',
    price: 35000,
    discount: 38000,
  },
  {
    id: 15,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/muop-huong-1kg-102.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/muop-huong-1kg-898.html',
    title: 'Mướp hương 1kg',
    price: 40000,
    discount: 45000,
  },
  {
    id: 16,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-chua-1kg-208.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/ca-chua-1kg-896.html',
    title: 'Cà chua 1kg',
    price: 38000,
    discount: 45000,
  },
  {
    id: 17,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/khoai-tay-500g.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/khoai-tay-1kg-668.html',
    title: 'Khoai tây 1kg',
    price: 40000,
    discount: null,
  },
  {
    id: 18,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/bi-xanh-500g.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/bi-dao-1kg-750.html',
    title: 'Bí đao 1kg',
    price: 42000,
    discount: 59000,
  },
  {
    id: 19,
    thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-rot-500g-301.jpg',
    url: 'https://nongsanlangbiang.com/san-pham/ca-rot-1kg-669.html',
    title: 'Cà rốt 1kg',
    price: 38000,
    discount: 39000,
  },
];
export const DATA_CATEGORY_BY_ID: Array<ProductProps[]> = [
  [
    {
      id: 1,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-phe-elegant-hop-tui-loc-10goi-100-arabica-sach-tu-nhien.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ca-phe-elegant-hop-10-goi-tui-loc-100-arabica-chua-nhe-thom-sau-502.html',
      title: 'Cà phê ELEGANT (hộp 10 gói túi lọc), 100% Arabica - chua nhẹ, thơm sâu',
      price: 150000,
      discount: null,
    },
    {
      id: 2,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-phe-elegant-250g-100-arabica-sach-tu-nhien.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ca-phe-elegant-250g-100-arabica-chua-nhe-hau-ngot-499.html',
      title: 'Cà phê ELEGANT 250g, 100% Arabica - chua nhẹ, hậu ngọt',
      price: 165000,
      discount: null,
    },
    {
      id: 2,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/tra-oolong-100g-hop-10-goi-nho-10g.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/tra-oolong-100g-hop-10-goi-nho-10g-856.html',
      title: 'Trà Oolong 100g (hộp 10 gói nhỏ 10g)',
      price: 98000,
      discount: null,
    },
    {
      id: 3,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/tra-oo-long-100g-hop-giay-363.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/tra-oo-long-100g-hop-giay-818.html',
      title: 'Trà OO Long 100g (hộp giấy)',
      price: 85000,
      discount: null,
    },
    {
      id: 4,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-phe-original-tui-loc-dam-dang-thom-sau-706.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ca-phe-original-tui-loc-dang-nhe-thom-sau-557.html',
      title: 'Cà phê Original túi lọc,  đắng nhẹ, thơm sâu',
      price: 120000,
      discount: null,
    },
    {
      id: 5,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-phe-original-500g-dam-dang-thom-sau-95.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ca-phe-original-500g-dang-nhe-thom-sau-556.html',
      title: 'Cà phê Original 500g, đắng nhẹ, thơm sâu',
      price: 150000,
      discount: null,
    },
    {
      id: 6,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-phe-original-250g-dam-dang-thom-sau-153.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ca-phe-original-250g-dang-nhe-thom-sau-555.html',
      title: 'Cà phê Original 250g, đắng nhẹ, thơm sâu',
      price: 76000,
      discount: null,
    },
    {
      id: 7,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-phe-elegant-500g-100-arabica-sach-tu-nhien.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ca-phe-elegant-500g-100-arabica-chua-nhe-hau-ngot-504.html',
      title: 'Cà phê ELEGANT 500g, 100% Arabica - chua nhẹ, hậu ngọt',
      price: 325000,
      discount: null,
    },

    {
      id: 8,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-phe-arabica-hat-s18--500g-chua-nhe-hau-ngot-650.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ca-phe-arabica-hat-s18--500g-chua-nhe-hau-ngot-881.html',
      title: 'Cà phê Arabica hạt, S18 -  (500g) chua nhẹ, hậu ngọt',
      price: 300000,
      discount: null,
    },
    {
      id: 9,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-phe-hat-arabica-s18--1kg-chua-nhe-hau-ngot-446.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ca-phe-hat-arabica-s18--1kg-chua-nhe-hau-ngot-880.html',
      title: 'Cà phê hạt Arabica S18 -  (1kg) chua nhẹ, hậu ngọt',
      price: 600000,
      discount: null,
    },
  ],
  [
    {
      id: 0,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/cau-tich.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/cau-tich-879.html',
      title: 'Cẩu tích ',
      price: 350000,
      discount: null,
    },
    {
      id: 1,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/sa-nhan.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/sa-nhan-878.html',
      title: 'Sa nhân',
      price: 600000,
      discount: null,
    },
    {
      id: 2,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/hat-uoi-hat-duoi-uoi.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/hat-uoi-hat-duoi-uoi-877.html',
      title: 'Hạt ươi (hạt đười ươi)',
      price: 34000,
      discount: null,
    },
    {
      id: 3,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/que-tra-my-906.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/que-tra-my-cinnamon-875.html',
      title: 'Quế (Trà My) - Cinnamon',
      price: 120000,
      discount: null,
    },
    {
      id: 4,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/cam-say-deo-muoi-ot.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/cam-say-deo-dried-oranges-858.html',
      title: 'Cam sấy dẻo - Dried Oranges',
      price: 135000,
      discount: null,
    },
    {
      id: 5,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/sam-day-ngoc-say-deo.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/sam-day-ngoc-say-857.html',
      title: 'Sâm dây ngọc sấy ',
      price: 98000,
      discount: null,
    },
    {
      id: 6,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/xoai-rung-say-deo-langbiang-langbiang-food-250g-29.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/xoai-say-deo-dried-mango-611.html',
      title: 'Xoài sấy dẻo - Dried Mango',
      price: 38000,
      discount: null,
    },
  ],
  [
    {
      id: 0,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/muoi-xi-muoi-langbiang-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/muoi-xi-muoi-langbiang-1kg-707.html',
      title: 'Muối xí muội Langbiang 1kg',
      price: 200000,
      discount: null,
    },
    {
      id: 1,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/muoi-xi-muoi-langbiang-250g.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/muoi-xi-muoi-langbiang-250g-706.html',
      title: 'Muối xí muội Langbiang 250g',
      price: 55000,
      discount: null,
    },
    {
      id: 2,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/muoi-xi-muoi-langbiang-100g.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/muoi-xi-muoi-langbiang-100g-600.html',
      title: 'Muối xí muội Langbiang 100g',
      price: 25000,
      discount: null,
    },
  ],
  [
    {
      id: 0,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/dau-hanh-500g.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/dau-hanh-500g-948.html',
      title: 'Đầu hành 500g',
      price: 20000,
      discount: null,
    },
    {
      id: 1,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/hanh-la-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/hanh-la-1kg-915.html',
      title: 'Hành lá 1kg',
      price: 35000,
      discount: null,
    },
    {
      id: 2,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/gung-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/gung-1kg-913.html',
      title: 'Gừng 1kg',
      price: 80000,
      discount: 100000,
    },
    {
      id: 3,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/cu-sa-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/cu-sa-1kg-912.html',
      title: 'Củ sả 1kg',
      price: 30000,
      discount: 35000,
    },
    {
      id: 4,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/hanh-ly-son-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/hanh-ly-son-1kg-910.html',
      title: 'Hành Lý Sơn 1kg',
      price: 75000,
      discount: null,
    },
    {
      id: 5,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/toi-ly-son-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/toi-ly-son-1kg-909.html',
      title: 'Tỏi Lý Sơn 1kg',
      price: 80000,
      discount: null,
    },
    {
      id: 6,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/chanh-1kg-67.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/chanh-1kg-906.html',
      title: 'Chanh 1kg',
      price: 30000,
      discount: null,
    },
    {
      id: 7,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ot-do.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ot-do-red-chili-872.html',
      title: 'Ớt đỏ - Red chili',
      price: 40000,
      discount: 45000,
    },
  ],
  [
    {
      id: 0,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/su-su-baby-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/su-su-baby-1kg-940.html',
      title: 'Su su baby 1kg',
      price: 65000,
      discount: null,
    },
    {
      id: 1,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/cai-kale-cai-xoan-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/cai-kale-cai-xoan-1kg-936.html',
      title: 'Cải Kale (cải xoăn) 1kg',
      price: 60000,
      discount: 65000,
    },
    {
      id: 2,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/dua-leo-baby-1kg-745.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/dua-leo-baby-1kg-670.html',
      title: 'Dưa leo baby 1kg',
      price: 40000,
      discount: null,
    },
    {
      id: 3,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-rot-ti-hon-300g.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ca-rot-ti-hon-1kg-767.html',
      title: 'Cà rốt tí hon 1kg',
      price: 75000,
      discount: 105000,
    },
    {
      id: 4,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/bap-cai-tim-su-tim-1kg-100.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/bap-cai-tim-su-tim-1kg-656.html',
      title: 'Bắp cải tím (sú tím) 1kg',
      price: 65000,
      discount: null,
    },
    {
      id: 5,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ot-trai-cay-royal-langbiang-food-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ot-trai-cay-langbiang-food-1kg-673.html',
      title: 'Ớt trái cây Langbiang Food (1kg)',
      price: 85000,
      discount: null,
    },
  ],

  [
    {
      id: 0,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/bo-034-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/bo-034-loai-1-1kg-946.html',
      title: 'Bơ 034 (loại 1), 1kg',
      price: 55000,
      discount: null,
    },
    {
      id: 1,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/thom-khom-dua-mat-1kg-304.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/thom-khom-dua-mat-1kg-945.html',
      title: 'Thơm (khóm, dứa) mật, 1kg',
      price: 30000,
      discount: null,
    },
    {
      id: 2,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/cai-dang-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/cai-dang-cai-be-xanh-1kg-933.html',
      title: 'Cải đắng (cải bẹ xanh) 1kg',
      price: 30000,
      discount: null,
    },
    {
      id: 3,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/hanh-tay-tim-ti-hon-300g.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/hanh-tay-1kg-675.html',
      title: 'Hành tây 1kg',
      price: 35000,
      discount: null,
    },
    {
      id: 4,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/chanh-day-hoang-gia-1kg-195.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/chanh-day-hoang-gia-1kg-929.html',
      title: 'Chanh dây Hoàng Gia 1kg',
      price: 150000,
      discount: null,
    },
    {
      id: 5,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/kho-qua-rung-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/kho-qua-rung-1kg-925.html',
      title: 'Khổ qua rừng 1kg',
      price: 55000,
      discount: 65000,
    },
    {
      id: 6,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/bi-ngo-non-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/bi-do-non-1kg-918.html',
      title: 'Bí đỏ non 1kg',
      price: 49000,
      discount: null,
    },
    {
      id: 7,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/su-tim-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/su-tim-1kg-916.html',
      title: 'Sú tim 1kg',
      price: 40000,
      discount: 45000,
    },
    {
      id: 8,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/xa-lach-romaine-da-lat-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/xa-lach-da-lat-1kg-911.html',
      title: 'Xà lách Đà Lạt 1kg',
      price: 50000,
      discount: null,
    },
    {
      id: 9,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/bi-ho-lo-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/bi-ho-lo-1kg-908.html',
      title: 'Bí hồ lô 1kg',
      price: 45000,
      discount: 49000,
    },
    {
      id: 10,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/sup-lo-xanh-bong-cai-xanh-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/sup-lo-xanh-bong-cai-xanh-1kg-905.html',
      title: 'Súp lơ xanh (bông cải xanh) 1kg',
      price: 45000,
      discount: 51000,
    },
    {
      id: 11,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/cu-den-1kg-692.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/cu-den-1kg-904.html',
      title: 'Củ dền 1kg',
      price: 32000,
      discount: 49000,
    },
    {
      id: 12,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ot-chuong-xanh-do-vang-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ot-chuong-xanh-do-vang-1kg-902.html',
      title: 'Ớt chuông Xanh, Đỏ, Vàng 1kg',
      price: 45000,
      discount: 55000,
    },
    {
      id: 13,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/cu-cai-trang-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/cu-cai-trang-1kg-900.html',
      title: 'Củ cải trắng 1kg',
      price: 25000,
      discount: null,
    },
    {
      id: 14,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/su-hao-1kg.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/su-hao-1kg-899.html',
      title: 'Su hào 1kg',
      price: 35000,
      discount: 38000,
    },
    {
      id: 15,
      thumbnail:
        'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/muop-huong-1kg-102.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/muop-huong-1kg-898.html',
      title: 'Mướp hương 1kg',
      price: 40000,
      discount: 45000,
    },
    {
      id: 16,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-chua-1kg-208.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ca-chua-1kg-896.html',
      title: 'Cà chua 1kg',
      price: 38000,
      discount: 45000,
    },
    {
      id: 17,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/khoai-tay-500g.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/khoai-tay-1kg-668.html',
      title: 'Khoai tây 1kg',
      price: 40000,
      discount: null,
    },
    {
      id: 18,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/bi-xanh-500g.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/bi-dao-1kg-750.html',
      title: 'Bí đao 1kg',
      price: 42000,
      discount: 59000,
    },
    {
      id: 19,
      thumbnail: 'https://nongsanlangbiang.com/thumb/260x230/2/upload/product/ca-rot-500g-301.jpg',
      url: 'https://nongsanlangbiang.com/san-pham/ca-rot-1kg-669.html',
      title: 'Cà rốt 1kg',
      price: 38000,
      discount: 39000,
    },
  ],
];
export const DATA_CATEGORY = [
  {
    data: DATA_CATEGORY_BY_ID[0],
    id: 'category_1',
    name: 'TRÀ OO LONG, CÀ PHÊ LANGBIANG',
    image:
      'https://nongsanlangbiang.com/upload/product/ca-phe-elegant-250g-100-arabica-sach-tu-nhien.jpg',
  },
  {
    data: DATA_CATEGORY_BY_ID[1],
    id: 'category_2',
    name: 'NÔNG SẢN, THẢO DƯỢC XUẤT KHẨU',
    image:
      'https://media.vneconomy.vn/w800/images/upload/2021/04/20/1103744-1532487074002698810057-0-0-562-1000-crop-15342063700162136202995.jpg',
  },
  {
    data: DATA_CATEGORY_BY_ID[2],
    id: 'category_3',
    name: 'ĐẶC SẢN MUỐI LANGBIANG FOOD',
    image: 'https://nongsanlangbiang.com/upload/product/muoi-xi-muoi-langbiang-250g.jpg',
  },
  {
    data: DATA_CATEGORY_BY_ID[3],
    id: 'category_4',
    name: 'RAU GIA VỊ',
    image: 'https://media.cooky.vn/article/s640/Article216-635616831634442259.jpg',
  },
  {
    data: DATA_CATEGORY_BY_ID[4],
    id: 'category_5',
    name: 'RAU CỦ TÍ HON, ĐẶC BIỆT',
    image:
      'https://product.hstatic.net/200000294752/product/-thot-len-dich-thi-la-tien-nu-ket-lai-o-ha-pham-roi77-2021-05-22-14-58_74ce4ca3c2104882b0e5993c4482dc93.jpg',
  },
  {
    data: DATA_CATEGORY_BY_ID[5],
    id: 'category_6',
    name: 'RAU, CỦ - SẠCH, CHUẨN VIETGAP',
    image: 'https://nongsanlangbiang.com/thumb/360x830/2/upload/product/c01-combo-an-vui-470.jpg',
  },
];
// (function ($) {
//   'use strict';
//   let list = $('#product-wrap');
//   let list_items = $('.item_pro', list);
//   let res = [];
//   if (list.length) {
//     list_items.each(function (index) {
//       let obj = {
//         id: index,
//         thumbnail: window.location.origin + '/' + $('.img  img', this).attr('src'),
//         url: window.location.origin + '/' + $('.img > a', this).attr('href'),
//         title: $('.img > a > img', this).attr('alt'),
//         price: parseInt(
//           $('.info > .price > .gia', this).text().replace('Đ', '').replace('.', '').trim(),
//           10
//         ),
//         discount: parseInt(
//           $('.info > .price > .giacu', this).text().replace('Đ', '').replace('.', '').trim(),
//           10
//         ),
//       };
//       res.push(obj);
//     });
//   }
//   copy(JSON.stringify(res));
//   console.log(res);
// })(jQuery);
