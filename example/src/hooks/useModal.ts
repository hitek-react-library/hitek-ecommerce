import { useState } from 'react';
import { ProductProps } from '~constants/types';

export default () => {
  const [visible, setVisible] = useState(false);
  const [product, setProduct] = useState<ProductProps>();
  const [product_id, setProductId] = useState<string>();
  const show = (product: ProductProps) => {
    setVisible(true);
    setProduct(product);
  };
  const showWithoutProduct = (product_id: string) => {
    setVisible(true);
    setProductId(product_id);
  };
  const hide = async (onHide = () => {}) => {
    setVisible(false);
    setProduct(undefined);
    setProductId(undefined);
    if (onHide && typeof onHide === 'function') onHide();
  };

  return { visible, product, show, hide, showWithoutProduct, product_id };
};
