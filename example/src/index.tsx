import {
  NavigationContainer,
  NavigationContainerRef,
} from '@react-navigation/native';
import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import { Platform } from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';
import {
  initialWindowMetrics,
  SafeAreaProvider,
} from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import DarkTheme from '~/themes/dark-theme';
import DefaultTheme from '~/themes/default-theme';
import { ModalProvider } from '~components/modalContext';
import PSpinner from '~components/PSpinner';
import useIsDarkTheme from '~hooks/use-is-dark-theme';
import RootStack from '~navigators/root-stack';
import store, { persistor } from '~store/store';

// https://langbiang.com/share?product_id=xxxxx&voucher_id=yyyyy
// const linking: LinkingOptions = {
//   prefixes: [
//     /* your linking prefixes */
//     'langbiang://',
//     'https://langbiang.hitek.com.vn:7001',
//   ],
//   config: {
//     /* configuration for matching screens with paths */
//     screens: {
//       initialRouteName: 'Splash',
//       Splash: {
//         path: 'splash/:product_id?/:voucher_id?',
//         parse: {
//           product_id: (id) => id,
//           text: (text) => text,
//         },
//       },
//     },
//   },
// };
const Main = () => {
  const nav = useRef<NavigationContainerRef>(null);
  const [isDark] = useIsDarkTheme();

  const theme = useMemo(() => {
    if (isDark) {
      return DarkTheme;
    }
    return DefaultTheme;
  }, [isDark]);

  useEffect(() => {}, []);

  useEffect(() => {
    // Configure @hitek auth lib
    // setConfig({
    //   host: 'https://langbiang.hitek.com.vn:7001/api/v1',
    //   log: true,
    //   google: {
    //     webClientId:
    //       '910704446651-un0ph9k5bi4dbch0a7q1sfule8oqkjr2.apps.googleusercontent.com',
    //   },
    // });
  }, []);

  const onReady = useCallback(async () => {}, []);

  return (
    <SafeAreaProvider initialMetrics={initialWindowMetrics}>
      <PaperProvider theme={theme}>
        <NavigationContainer theme={theme} ref={nav} onReady={onReady}>
          <ModalProvider>
            <RootStack />
            <PSpinner />
          </ModalProvider>
        </NavigationContainer>
      </PaperProvider>
    </SafeAreaProvider>
  );
};

const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Main />
    </PersistGate>
  </Provider>
);
export default App;
