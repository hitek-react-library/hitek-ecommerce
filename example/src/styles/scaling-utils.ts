import { Dimensions } from 'react-native';

//Default guideline sizes are based on standard iPhone8 screen mobile device
const guidelineBaseWidth = 375;
const guidelineBaseHeight = 667;

const window = Dimensions.get('window');
const screen = Dimensions.get('screen');

const [shortDimension, longDimension] = window.width < window.height ? [window.width, window.height] : [window.height, window.width];
const [shortDimensionScreen, longDimensionScreen] = screen.width < screen.height ? [screen.width, screen.height] : [screen.height, screen.width];



export const scale = (size:number) => shortDimension / guidelineBaseWidth * size;
export const moderateScale = (size:number, factor = 0.5) => size + (scale(size) - size) * factor;

export const widthScale = (size:number) => shortDimension / guidelineBaseWidth * size;
export const moderateWidthScale = (size:number, factor = 0.5) => size + (widthScale(size) - size) * factor;

export const heightScale = (size:number) => longDimension / guidelineBaseHeight * size;
export const moderateHeightScale = (size:number, factor = 0.5) => size + (heightScale(size) - size) * factor;


export const windowWidth = (percent=1) => shortDimension*percent;
export const windowHeight = (percent=1) => longDimension*percent;
export const screenWidth = (percent=1) => shortDimensionScreen*percent;
export const screenHeight = (percent=1) => longDimensionScreen*percent;


