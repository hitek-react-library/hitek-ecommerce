import { PixelRatio } from 'react-native';
import { HEIGHT_SCALE, WIDTH_SCALE } from '~constants/constants';

export const pFont = {
  REGULAR: 'Montserrat-Regular',
  FONT2: 'SFUTradeGothicBoldCondTwenty',
};

const fontSize =
  PixelRatio.get() <= 1.5 ? 12 : PixelRatio.get() > 1.5 && PixelRatio.get() < 3 ? 13 : 14;
const scale = Math.min(WIDTH_SCALE, HEIGHT_SCALE);
export const pFontSize = (size = fontSize) => Math.ceil(size * scale);

export const pText = {
  H0: {
    fontSize: pFontSize(fontSize) + 16,
  },
  H1: {
    fontSize: pFontSize(fontSize) + 8,
  },
  H2: {
    fontSize: pFontSize(fontSize) + 6,
  },
  H3: {
    fontSize: pFontSize(fontSize) + 4,
  },
  H4: {
    fontSize: pFontSize(fontSize) + 2,
  },
  BODY1: {
    fontSize: pFontSize(fontSize) + 0,
  },
  BODY12: {
    fontSize: pFontSize(fontSize) - 1.75,
  },
  BODY2: {
    fontSize: pFontSize(fontSize) - 2,
  },
  SMALL1: {
    fontSize: pFontSize(fontSize) - 3,
  },
  SMALL12: {
    fontSize: pFontSize(fontSize) - 4,
  },
  SMALL2: {
    fontSize: pFontSize(fontSize) - 5,
  },
  BUTTON: {
    fontSize: pFontSize(fontSize) - 2,
    textTransform: 'uppercase',
  },
  TITLE: {
    fontSize: pFontSize(fontSize) + 50,
  },
};
