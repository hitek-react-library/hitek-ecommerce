import AsyncStorage from '@react-native-community/async-storage';
import {
  configureStore,
  isRejectedWithValue,
  Middleware,
  MiddlewareAPI,
} from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/dist/query';
import {
  FLUSH,
  PAUSE,
  PERSIST,
  persistReducer,
  persistStore,
  PURGE,
  REGISTER,
  REHYDRATE,
} from 'redux-persist';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '~reducers/rootReducer';
import { apiAddressServices } from '~services/api_address_services';
import { apiFavoriteServices } from '~services/api_favorite_services';
import { apiOrderServices } from '~services/api_order_services';
import { apiPostServices } from '~services/api_post_services';
import { apiProductServices } from '~services/api_product_services';
import { apiUserServices } from '~services/api_user_services';

export const rtkQueryErrorLogger: Middleware =
  (api: MiddlewareAPI) => (next) => (action) => {
    // RTK Query uses `createAsyncThunk` from redux-toolkit under the hood, so we're able to utilize these use matchers!
    if (isRejectedWithValue(action)) {
      console.warn('We got a rejected action!', action);
    }
    return next(action);
  };

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: [
    apiOrderServices.reducerPath,
    apiAddressServices.reducerPath,
    apiProductServices.reducerPath,
    apiUserServices.reducerPath,
    apiFavoriteServices.reducerPath,
    apiPostServices.reducerPath,
  ],
};
export type RootState = ReturnType<typeof store.getState>;

const persistedReducer = persistReducer(persistConfig, rootReducer);

const middlewares: any[] = [
  thunkMiddleware,
  apiOrderServices.middleware,
  apiAddressServices.middleware,
  apiProductServices.middleware,
  apiUserServices.middleware,
  apiFavoriteServices.middleware,
  apiPostServices.middleware,
  rtkQueryErrorLogger,
];

if (__DEV__) {
  const createDebugger = require('redux-flipper').default;
  middlewares.push(createDebugger());
}
const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(middlewares),
});
setupListeners(store.dispatch);
export type AppDispatch = typeof store.dispatch;
export const persistor = persistStore(store);

export default store;
