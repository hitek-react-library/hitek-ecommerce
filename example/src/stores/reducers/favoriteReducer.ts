import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { FavoriteProps } from '~constants/types';
import { apiFavoriteServices } from '~services/api_favorite_services';

interface FavoriteState {
  favorite: FavoriteProps[];
  totalAddress: number;
}

const initialState: FavoriteState = {
  favorite: [],
  totalAddress: 0,
};

const favoriteSlice = createSlice({
  name: 'favoriteReducer',
  initialState,
  reducers: {
    clearDataFavorite: (state, action: PayloadAction) => {
      state = initialState;
    },
    cacheFavoriteInfo: (state, action: PayloadAction<FavoriteProps[]>) => {
      state.favorite = action.payload;
    },
    addFavoriteProduct: (state, action: PayloadAction<FavoriteProps>) => {
      state.favorite = [...state.favorite, action.payload];
    },
  },
  extraReducers: (builder) => {
    builder
      .addMatcher(apiFavoriteServices.endpoints.getListFavorite.matchFulfilled, (state, action) => {
        state.favorite = action.payload;
      })
      .addMatcher(apiFavoriteServices.endpoints.addFavorite.matchFulfilled, (state, action) => {
        state.favorite = [...state.favorite, action.payload];
      })
      .addMatcher(apiFavoriteServices.endpoints.deleteFavorite.matchFulfilled, (state, action) => {
        const newArrFav = state.favorite?.filter((v) => v.id !== action.payload.id);
        state.favorite = newArrFav;
      });
  },
});

export const { clearDataFavorite, cacheFavoriteInfo, addFavoriteProduct } = favoriteSlice.actions;
const favoriteReducer = favoriteSlice.reducer;
export default favoriteReducer;
