import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AddressProps, ThemeStyle, UserProps } from '~constants/types';
import { apiAddressServices } from '~services/api_address_services';
import { apiUserServices } from '~services/api_user_services';

interface UserState {
  userTheme: string;
  count: number;
  user: UserProps | undefined;
  token: string | undefined;
  voucher: string | null;
}

const initialState: UserState = {
  userTheme: 'light',
  count: 0,
  user: undefined,
  token: undefined,
  voucher: null,
};

const userSlice = createSlice({
  name: 'userReducer',
  initialState,
  reducers: {
    clearData: (state) => (state = initialState),
    setUserColorScheme: (state, action: PayloadAction<ThemeStyle>) => {
      state.userTheme = action.payload;
    },
    setVoucher: (state, action: PayloadAction<string | null>) => {
      state.voucher = action.payload;
    },
    cacheUserInfo: (state, action: PayloadAction<UserProps>) => {
      state.user = { ...state.user, ...action.payload };
    },
    cacheToken: (state, action: PayloadAction<string>) => {
      state.token = action.payload;
    },
    updateDefaultAddress: (state, action: PayloadAction<AddressProps | undefined>) => {
      state.user = { ...state.user!, defaultAddress: action.payload };
    },
  },
  extraReducers: (builder) => {
    builder
      .addMatcher(
        apiAddressServices.endpoints.getDefaultAddress.matchFulfilled,
        (state, action) => {
          state.user = { ...state.user!, defaultAddress: action.payload };
        }
      )
      .addMatcher(apiAddressServices.endpoints.addAddress.matchFulfilled, (state, action) => {
        if (action.payload.is_default) {
          state.user = { ...state.user!, defaultAddress: action.payload };
        }
      })
      .addMatcher(apiAddressServices.endpoints.updateAddress.matchFulfilled, (state, action) => {
        if (action.payload.is_default) {
          state.user = { ...state.user!, defaultAddress: action.payload };
        }
      })
      .addMatcher(apiUserServices.endpoints.getProfile.matchFulfilled, (state, action) => {
        state.user = { ...state.user, ...action.payload };
      })
      .addMatcher(apiUserServices.endpoints.updateUser.matchFulfilled, (state, action) => {
        state.user = { ...state.user, ...action.payload };
      })
      .addMatcher(apiUserServices.endpoints.createCollabUser.matchFulfilled, (state, action) => {
        state.user = { ...state.user, is_collab: true } as UserProps;
      });
  },
});

export const {
  setUserColorScheme,
  clearData,
  cacheUserInfo,
  cacheToken,
  updateDefaultAddress,
  setVoucher,
} = userSlice.actions;
const userReducer = userSlice.reducer;
export default userReducer;
