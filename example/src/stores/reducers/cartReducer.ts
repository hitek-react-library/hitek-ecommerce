import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import _ from 'lodash';
import { CartProps, ProductProps } from '~constants/types';

interface CounterState {
  cart: CartProps[];
  totalQuantity: number;
  totalPrice: number;
}

const initialState: CounterState = { cart: [], totalQuantity: 0, totalPrice: 0 };

const cartSlice = createSlice({
  name: 'cartReducer',
  initialState,
  reducers: {
    //* Set count from item
    setCountForItem(
      state: CounterState,
      action: PayloadAction<{ product: ProductProps; quantity: number }>
    ) {
      let _cart: CartProps[] = state.cart;
      const quantity = action.payload.quantity;
      const product = action.payload.product;
      for (let i in _cart) {
        const oldItemQuantity = _cart[i].quantity;
        if (_cart[i].id === product.id && oldItemQuantity !== quantity) {
          state.totalQuantity += quantity - oldItemQuantity;
          state.totalPrice += (quantity - oldItemQuantity) * (product.price || 0);
          _cart[i].quantity = quantity;
          state.cart = _cart;
          return;
        }
      }
      const tempCart: CartProps = { ...product, quantity: quantity };
      state.cart = _.uniqBy([tempCart, ...state.cart], 'id');
      state.totalQuantity += quantity;
      state.totalPrice += quantity * (product.price || 0);
    },
    //* Add to cart
    addItemToCart(
      state: CounterState,
      action: PayloadAction<{ product: ProductProps; quantity?: number }>
    ) {
      let _cart: CartProps[] = state.cart;
      const quantity = action.payload.quantity || 1;
      const product = action.payload.product;
      for (let i in _cart) {
        if (_cart[i].id === product.id) {
          _cart[i].quantity += quantity;
          state.totalQuantity += quantity;
          state.totalPrice += quantity * (product.price || 0);
          state.cart = _cart;
          return;
        }
      }
      const tempCart: CartProps = { ...product, quantity: 1 };
      state.cart = _.uniqBy([tempCart, ...state.cart], 'id');
      state.totalQuantity++;
      state.totalPrice += product.price || 0;
    },
    //* Remove item from cart
    removeItemFromCart(state: CounterState, action: PayloadAction<ProductProps>) {
      let _cart: CartProps[] = state.cart;
      for (let i in _cart) {
        if (_cart[i].id === action.payload.id) {
          _cart[i].quantity--;
          state.cart = _cart;
          state.totalQuantity--;
          state.totalPrice -= action.payload.price || 0;
          return;
        }
      }
    },
    //* Remove all items from cart
    removeItemFromCartAll(state: CounterState, action: PayloadAction<ProductProps>) {
      let _cart: CartProps[] = state.cart;
      state.cart.forEach(function (val, i) {
        if (val.id === action.payload.id) {
          _cart.splice(i, 1);
          state.totalQuantity -= val.quantity;
          state.totalPrice -= val.quantity * (val.price || 0);
        }
      });
      state.cart = _cart;
    },
    //* Clear cart
    clearCart: (state) => (state = initialState),
  },
});

export const {
  setCountForItem,
  addItemToCart,
  removeItemFromCart,
  removeItemFromCartAll,
  clearCart,
} = cartSlice.actions;
const cartReducer = cartSlice.reducer;
export default cartReducer;
