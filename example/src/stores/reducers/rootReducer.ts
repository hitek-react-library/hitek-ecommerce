/// Imports: Dependencies
import { combineReducers } from 'redux';
import { apiAddressServices } from '~services/api_address_services';
import { apiFavoriteServices } from '~services/api_favorite_services';
import { apiOrderServices } from '~services/api_order_services';
import { apiPostServices } from '~services/api_post_services';
import { apiProductServices } from '~services/api_product_services';
import { apiUserServices } from '~services/api_user_services';
import addressReducer from './addressReducer';
import cartReducer from './cartReducer';
import favoriteReducer from './favoriteReducer';
import settingsReducer from './settingsReducer';
import userReducer from './userReducer';

/// Redux: Root Reducer
const rootReducer = combineReducers({
  settingsReducer,
  userReducer,
  cartReducer,
  addressReducer,
  favoriteReducer,
  [apiProductServices.reducerPath]: apiProductServices.reducer,
  [apiAddressServices.reducerPath]: apiAddressServices.reducer,
  [apiOrderServices.reducerPath]: apiOrderServices.reducer,
  [apiUserServices.reducerPath]: apiUserServices.reducer,
  [apiFavoriteServices.reducerPath]: apiFavoriteServices.reducer,
  [apiPostServices.reducerPath]: apiPostServices.reducer,
});
// Exports
export default rootReducer;
