import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AddressProps } from '~constants/types';

interface AddressState {
  address: AddressProps[];
  totalAddress: number;
  selectCity: string;
}

const initialState: AddressState = {
  address: [],
  totalAddress: 0,
  selectCity: 'TP. Hồ Chí Minh',
};

const addressSlice = createSlice({
  name: 'addressReducer',
  initialState,
  reducers: {
    clearDataAddress: (state, action: PayloadAction) => {
      state = initialState;
    },
    cacheAddressList: (state, action: PayloadAction<AddressProps[]>) => {
      state.address = action.payload;
    },
    changeCity: (state, action: PayloadAction<string>) => {
      state.selectCity = action.payload;
    },
  },
});

export const { clearDataAddress, cacheAddressList, changeCity } = addressSlice.actions;
const addressReducer = addressSlice.reducer;
export default addressReducer;
