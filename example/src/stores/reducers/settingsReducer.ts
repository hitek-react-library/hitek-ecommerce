import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ThemeStyle } from '~constants/types';

const settingSlice = createSlice({
  name: 'settingsReducer',
  initialState: {
    userTheme: 'light',
    count: 0,
  },
  reducers: {
    setUserColorScheme: (state, action: PayloadAction<ThemeStyle>) => {
      state.userTheme = action.payload;
    },
    setUserCount: (state, action: { payload: number }) => {
      state.count = state.count + action.payload;
    },
  },
});

export const { setUserColorScheme, setUserCount } = settingSlice.actions;
const settingsReducer = settingSlice.reducer;
export default settingsReducer;
