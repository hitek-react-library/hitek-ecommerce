import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { BASE_URL2 } from '~constants/constants';
import { PostProps, PostType } from '~constants/types';

export const apiPostServices = createApi({
  reducerPath: 'postApi',
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_URL2,
    prepareHeaders: async (headers, { getState }) => {
      // const token = await getToken();
      if (token) {
        headers.set('Authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['Post'],
  refetchOnMountOrArgChange: true,
  refetchOnFocus: true,
  refetchOnReconnect: true,
  endpoints: (builder) => ({
    listPostByType: builder.query<
      PostProps[],
      { type: PostType; page?: number; limit?: number }
    >({
      query: ({ type, page = 1, limit = 50 }) =>
        `post?fields=["$all"]&filter={"type": "${type}"}&limit=${limit}&page=${page}&order=[["created_at_unix_timestamp","desc"]]`,
      transformResponse: (response: {
        results: { objects: { rows: PostProps[] } };
      }) => {
        return response.results.objects.rows || [];
      },
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ id }) => ({ type: 'Post' as const, id })),
              { type: 'Post', id: 'LIST' },
            ]
          : [{ type: 'Post', id: 'LIST' }],
    }),
  }),
});

export const { usePrefetch, useListPostByTypeQuery } = apiPostServices;
