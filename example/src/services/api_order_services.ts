import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { BASE_URL2 } from '~constants/constants';
import {
  ActivityOrder,
  OrderCreateProps,
  OrderProps,
  PreOrderCallbackProps,
} from '~constants/types';

export const apiOrderServices = createApi({
  reducerPath: 'orderApi',
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_URL2,
    prepareHeaders: async (headers, { getState }) => {
      // const token = await getToken();
      if (token) {
        headers.set('Authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['Order'],
  refetchOnMountOrArgChange: true,
  refetchOnFocus: true,
  refetchOnReconnect: true,
  endpoints: (builder) => ({
    listOrder: builder.query<
      OrderProps[],
      { user_id: string | undefined; page?: number }
    >({
      query: ({ user_id, page = 1 }) => {
        return {
          url: `order?fields=["$all",{"order_items":["$all"]},{"order_activitys":["activity","created_at_unix_timestamp"]}]&filter={"user_id":"${user_id}"}&page=${page}&order=[["created_at_unix_timestamp","desc"]]`,
        };
      },
      transformResponse: (response: {
        results: { objects: { rows: OrderProps[] } };
      }) => {
        return response.results.objects.rows;
      },
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ id }) => ({ type: 'Order' as const, id })),
              { type: 'Order', id: 'LIST' },
            ]
          : [{ type: 'Order', id: 'LIST' }],
    }),
    addOrder: builder.mutation<OrderCreateProps, Partial<OrderCreateProps>>({
      query: (body) => ({
        url: `order`,
        method: 'POST',
        body,
      }),
      invalidatesTags: [{ type: 'Order', id: 'LIST' }],
    }),
    updateActivityOrder: builder.mutation<
      any,
      Partial<{ order_id: string; activity: ActivityOrder }>
    >({
      query: (body) => ({
        url: `order_activity`,
        method: 'POST',
        body,
      }),
      invalidatesTags: (result, error, { order_id }) => [
        { type: 'Order', id: order_id },
      ],
    }),
    preOrder: builder.mutation<
      PreOrderCallbackProps,
      Partial<OrderCreateProps>
    >({
      query: (body) => ({
        url: `order/pre_order`,
        method: 'POST',
        body,
      }),
      transformResponse: (response: {
        results: { object: PreOrderCallbackProps };
      }) => {
        return response.results.object;
      },
    }),
    getOneOrder: builder.query<OrderProps, string>({
      query: (order_id) =>
        `order/${order_id}?fields=["$all",{"order_items":["$all"]},{"order_activitys":["activity","created_at_unix_timestamp"]}]`,
      transformResponse: (response: { results: { object: OrderProps } }) => {
        return response.results.object;
      },
    }),
  }),
});

export const {
  usePrefetch,
  useListOrderQuery,
  useAddOrderMutation,
  usePreOrderMutation,
  useUpdateActivityOrderMutation,
  useGetOneOrderQuery,
} = apiOrderServices;
