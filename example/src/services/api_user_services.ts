import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { BASE_URL2 } from '~constants/constants';
import {
  CollabCreateProps,
  CreateRequestWithdrawMoneyProps,
  UserProps,
} from '~constants/types';

export const apiUserServices = createApi({
  reducerPath: 'userApi',
  refetchOnMountOrArgChange: true,
  refetchOnFocus: true,
  refetchOnReconnect: true,
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_URL2,
    prepareHeaders: async (headers, { getState }) => {
      // const token = await getToken();
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),

  endpoints: (builder) => ({
    getProfile: builder.query<UserProps, void>({
      query: () => `user/profile`,
      transformResponse: (response: { results: { object: UserProps } }) => {
        return response.results.object;
      },
    }),
    updateUser: builder.mutation<UserProps, Partial<{ id: string; body: any }>>(
      {
        query({ id, body }) {
          return {
            url: `user/${id}`,
            method: 'PUT',
            body: body,
          };
        },
        transformResponse: (response: { results: { object: UserProps } }) => {
          return response.results.object;
        },
      }
    ),
    createCollabUser: builder.mutation<UserProps, Partial<CollabCreateProps>>({
      query(body) {
        return {
          url: `collab`,
          method: 'POST',
          body: body,
        };
      },
    }),
    requestContributorTransaction: builder.mutation<
      UserProps,
      Partial<CreateRequestWithdrawMoneyProps>
    >({
      query(body) {
        return {
          url: `/user/withdraw/v2`,
          method: 'POST',
          body: body,
        };
      },
    }),

    getListStaticWithDrawMoney: builder.query<any, void>({
      query: () =>
        `contributor_transaction/static?order=[["created_at_unix_timestamp","desc"]]`,
      transformResponse: (response: { results: { object: any } }) => {
        return response.results.object;
      },
    }),
  }),
});

export const {
  usePrefetch,
  useUpdateUserMutation,
  useCreateCollabUserMutation,
  useGetProfileQuery,
  useRequestContributorTransactionMutation,
  useGetListStaticWithDrawMoneyQuery,
} = apiUserServices;
