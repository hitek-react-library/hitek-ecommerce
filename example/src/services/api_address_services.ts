import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { BASE_URL2 } from '~constants/constants';
import { AddressProps } from '~constants/types';

export const apiAddressServices = createApi({
  reducerPath: 'addressApi',
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_URL2,
    prepareHeaders: async (headers, { getState }) => {
      // const token = await getToken();
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  refetchOnMountOrArgChange: true,
  refetchOnFocus: true,
  refetchOnReconnect: true,
  tagTypes: ['Address'],
  endpoints: (builder) => ({
    getDefaultAddress: builder.query<AddressProps | undefined, {}>({
      query: () => `address?fields=["$all"]&filter={"is_default": true}`,
      transformResponse: (response: {
        results: { objects: { rows: AddressProps[] } };
      }) => {
        return (
          (response.results.objects.rows?.length &&
            response.results.objects.rows[0]) ||
          undefined
        );
      },
      providesTags: (result, error) => [{ type: 'Address', id: result?.id }],
    }),
    getListAddress: builder.query<
      AddressProps[],
      { page?: number; limit?: number }
    >({
      query: ({ page = 1, limit = 50 }) => {
        return {
          url: `address?fields=["$all"]&page=${page}&limit=${limit}`,
        };
      },
      transformResponse: (response: {
        results: { objects: { rows: AddressProps[] } };
      }) => {
        return response.results.objects.rows;
      },
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ id }) => ({ type: 'Address' as const, id })),
              { type: 'Address', id: 'LIST' },
            ]
          : [{ type: 'Address', id: 'LIST' }],
    }),
    addAddress: builder.mutation<AddressProps, Partial<AddressProps>>({
      query(body) {
        return {
          url: `address`,
          method: 'POST',
          body,
        };
      },
      transformResponse: (response: { results: { object: AddressProps } }) => {
        return response.results.object;
      },
      invalidatesTags: [{ type: 'Address', id: 'LIST' }],
    }),
    updateAddress: builder.mutation<
      AddressProps,
      Partial<{ id: string; body: any }>
    >({
      query({ id, body }) {
        return {
          url: `address/${id}`,
          method: 'PUT',
          body: body,
        };
      },
      invalidatesTags: (result, error, { id }) => [{ type: 'Address', id }],
      transformResponse: (response: { results: { object: AddressProps } }) => {
        return response.results.object;
      },
    }),
    deleteAddress: builder.mutation<AddressProps, Partial<{ id: string }>>({
      query({ id }) {
        return {
          url: `address/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: (result, error, { id }) => [{ type: 'Address', id }],
    }),
  }),
});

export const {
  usePrefetch,
  useGetListAddressQuery,
  useAddAddressMutation,
  useUpdateAddressMutation,
  useDeleteAddressMutation,
} = apiAddressServices;
