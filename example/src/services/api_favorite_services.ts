import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { BASE_URL2 } from '~constants/constants';
import { FavoriteProps } from '~constants/types';

export const apiFavoriteServices = createApi({
  reducerPath: 'favoriteApi',
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_URL2,
    prepareHeaders: async (headers, { getState }) => {
      // const token = await getToken();
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  refetchOnMountOrArgChange: true,
  refetchOnFocus: true,
  refetchOnReconnect: true,
  tagTypes: ['Favorite'],
  endpoints: (builder) => ({
    getListFavorite: builder.query<
      FavoriteProps[],
      { page?: number; limit?: number }
    >({
      query: ({ page = 1, limit = 999999 }) => {
        return {
          url: `favorite?fields=["$all",{"product":["$all"]}]&page=${page}&limit=${limit}`,
        };
      },
      transformResponse: (response: {
        results: { objects: { rows: FavoriteProps[] } };
      }) => {
        return response.results.objects.rows;
      },
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ id }) => ({ type: 'Favorite' as const, id })),
              { type: 'Favorite', id: 'LIST' },
            ]
          : [{ type: 'Favorite', id: 'LIST' }],
    }),
    addFavorite: builder.mutation<
      FavoriteProps,
      Partial<{ user_id: string; product_id: any }>
    >({
      query({ user_id, product_id }) {
        const body = { user_id, product_id };
        return {
          url: `favorite?fields=["$all",{"product":["$all"]}]`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'Favorite', id: 'LIST' }],
    }),
    deleteFavorite: builder.mutation<FavoriteProps, Partial<{ id: string }>>({
      query({ id }) {
        return {
          url: `favorite/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: (result, error, { id }) => [{ type: 'Favorite', id }],
    }),
  }),
});

export const {
  usePrefetch,
  useGetListFavoriteQuery,
  useAddFavoriteMutation,
  useDeleteFavoriteMutation,
} = apiFavoriteServices;
