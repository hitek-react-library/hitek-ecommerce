const versionJson = require('./../../package.json');
export const getAppVersion = async () => {
  try {
    return `Phiên bản ${versionJson.version}`;
  } catch (error) {
    console.log('🚀 code.bug ~ file: version.ts ~ line 10 ~ error', error);
    return `Phiên bản ${versionJson.version}`;
  }
};
