import { DefaultTheme as RNDefaultTheme, Theme } from '@react-navigation/native';
import { configureFonts, DefaultTheme as PaperDefaultTheme } from 'react-native-paper';
import { pColor } from '~styles/colors';

const fontConfig = {
  web: {
    font2: {
      fontFamily: 'SFUTradeGothicBoldCondTwenty',
      fontWeight: 'bold' as 'bold',
    },
    regular: {
      fontFamily: 'Montserrat-Bold',
      fontWeight: 'normal' as 'normal',
    },
    medium: {
      fontFamily: 'Montserrat-Bold',
      fontWeight: 'normal' as 'normal',
    },
    light: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: 'normal' as 'normal',
    },
    thin: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: 'normal' as 'normal',
    },
  },
  ios: {
    font2: {
      fontFamily: 'SFUTradeGothicBoldCondTwenty',
      fontWeight: 'bold' as 'bold',
    },
    regular: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: 'normal' as 'normal',
    },
    bold: {
      fontFamily: 'Montserrat-Bold',
      fontWeight: 'bold' as 'bold',
    },
    medium: {
      fontFamily: 'Montserrat-Bold',
      fontWeight: '500' as '500',
    },
    light: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: '300' as '300',
    },
    thin: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: '100' as '100',
    },
  },
  android: {
    font2: {
      fontFamily: 'SFUTradeGothicBoldCondTwenty',
      fontWeight: 'bold' as 'bold',
    },
    regular: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: 'normal' as 'normal',
    },
    bold: {
      fontFamily: 'Montserrat-Bold',
      fontWeight: 'bold' as 'bold',
    },
    medium: {
      fontFamily: 'Montserrat-Bold',
      fontWeight: '500' as '500',
    },
    light: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: '300' as '300',
    },
    thin: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: '100' as '100',
    },
  },
};

const DefaultTheme: Theme & ReactNativePaper.Theme = {
  ...RNDefaultTheme,
  ...PaperDefaultTheme,
  fonts: configureFonts(fontConfig),
  colors: {
    ...RNDefaultTheme.colors,
    ...PaperDefaultTheme.colors,
    primary: pColor.appColor,
    accent: pColor.appColor3,
    background: 'white',
    background2: '#f5f5f5',
    backgroundCheckBox: 'lightgray',
  },
};

export default DefaultTheme;

{
  /*
  //!colors (object): various colors used throughout different elements.
  + primary - primary color for your app, usually your brand color.
  + accent - secondary color for your app which complements the primary color.
  + background - background color for pages, such as lists.
  + surface - background color for elements containing content, such as cards.
  + text - text color for content.
  + disabled - color for disabled elements.
  + placeholder - color for placeholder text, such as input placeholder.
  + backdrop - color for backdrops of various components such as modals.
  + onSurface - background color for snackbars
  + notification - background color for badges
*/
}
