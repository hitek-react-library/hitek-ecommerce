import { DarkTheme as RNDarkTheme, Theme } from '@react-navigation/native';
import { configureFonts, DarkTheme as PaperDarkTheme, overlay } from 'react-native-paper';
import { pColor } from '~styles/colors';

const fontConfig = {
  web: {
    font5: {
      fontFamily: 'SFUTradeGothicBoldCondensedTwenty',
      fontWeight: 'bold' as 'bold',
    },
    regular: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: 'normal' as 'normal',
    },
    medium: {
      fontFamily: 'Montserrat-Bold',
      fontWeight: 'normal' as 'normal',
    },
    light: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: 'normal' as 'normal',
    },
    thin: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: 'normal' as 'normal',
    },
  },
  ios: {
    font5: {
      fontFamily: 'SFUTradeGothicBoldCondensedTwenty',
      fontWeight: 'bold' as 'bold',
    },
    regular: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: 'normal' as 'normal',
    },
    bold: {
      fontFamily: 'Montserrat-Bold',
      fontWeight: 'bold' as 'bold',
    },
    medium: {
      fontFamily: 'Montserrat-Bold',
      fontWeight: '500' as '500',
    },
    light: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: '300' as '300',
    },
    thin: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: '100' as '100',
    },
  },
  android: {
    font5: {
      fontFamily: 'SFUTradeGothicBoldCondensedTwenty',
      fontWeight: 'bold' as 'bold',
    },
    regular: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: 'normal' as 'normal',
    },
    bold: {
      fontFamily: 'Montserrat-Bold',
      fontWeight: 'bold' as 'bold',
    },
    medium: {
      fontFamily: 'Montserrat-Bold',
      fontWeight: '500' as '500',
    },
    light: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: '300' as '300',
    },
    thin: {
      fontFamily: 'Montserrat-Regular',
      fontWeight: '100' as '100',
    },
  },
};
const DarkTheme: Theme & ReactNativePaper.Theme = {
  ...RNDarkTheme,
  ...PaperDarkTheme,
  fonts: configureFonts(fontConfig),
  colors: {
    ...RNDarkTheme.colors,
    ...PaperDarkTheme.colors,
    primary: pColor.appColor2,
    accent: pColor.appColor3,
    background2: overlay(4, PaperDarkTheme.colors.surface),
    backgroundCheckBox: overlay(4, PaperDarkTheme.colors.surface),
  },
};

export default DarkTheme;

{
  /*
  //!colors (object): various colors used throughout different elements.
  + primary - primary color for your app, usually your brand color.
  + accent - secondary color for your app which complements the primary color.
  + background - background color for pages, such as lists.
  + surface - background color for elements containing content, such as cards.
  + text - text color for content.
  + disabled - color for disabled elements.
  + placeholder - color for placeholder text, such as input placeholder.
  + backdrop - color for backdrops of various components such as modals.
  + onSurface - background color for snackbars
  + notification - background color for badges
*/
}
