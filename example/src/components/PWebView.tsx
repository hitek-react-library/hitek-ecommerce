import IframeRenderer, { iframeModel } from '@native-html/iframe-plugin';
import TableRenderer, { tableModel } from '@native-html/table-plugin';
import React from 'react';
import { Linking, useWindowDimensions } from 'react-native';
import { useTheme } from 'react-native-paper';
import RenderHTML, { RenderHTMLProps } from 'react-native-render-html';
import { Edge } from 'react-native-safe-area-context';
import WebView from 'react-native-webview';
import { ModalContext } from '~components/modalContext';
import { widthScale } from '~styles/scaling-utils';

const edges: Edge[] = ['right', 'left'];

function PWebView(props: RenderHTMLProps) {
  const { width } = useWindowDimensions();

  const { colors, dark } = useTheme();
  const renderers = {
    iframe: IframeRenderer,
    table: TableRenderer,
  };

  const customHTMLElementModels = {
    iframe: iframeModel,
    table: tableModel,
  };
  let { showModalProductWithout } = React.useContext(ModalContext);

  return (
    <RenderHTML
      contentWidth={width}
      renderers={renderers}
      WebView={WebView}
      customHTMLElementModels={customHTMLElementModels}
      defaultWebViewProps={{
        androidHardwareAccelerationDisabled: true,
      }}
      baseStyle={{
        paddingTop: widthScale(20),
        paddingHorizontal: widthScale(12),
        backgroundColor: colors.background,
      }}
      ignoredDomTags={['oembed']}
      renderersProps={{
        table: {},
        iframe: {
          scalesPageToFit: true,
          webViewProps: {},
        },
        a: {
          onPress(event, url, htmlAttribs, target) {
            console.log(
              '🚀 code.bug ~ file: PWebView.tsx ~ line 29 ~ event, url, htmlAttribs, target',
              // event,
              url
              // htmlAttribs,
              // target
            );
            (async function () {
              try {
                const url = await Linking.getInitialURL();
                if (url) {
                  console.log('🚀 code.bug ~ file: bottom-tab.tsx ~ line 66 ~ url', url);
                  var splitted = url?.replace('langbiang://share/', '');
                  splitted && showModalProductWithout(splitted);
                }
              } catch (error) {
                console.error(error);
              }
            })();
          },
        },
      }}
      {...props}
    />
  );
}

export default PWebView;
