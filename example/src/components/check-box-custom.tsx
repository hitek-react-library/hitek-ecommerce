import * as React from 'react';
import { ViewStyle } from 'react-native';
import { TouchableRipple, useTheme } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { widthScale } from '~styles/scaling-utils';

interface Props {
  /**
   * Status of checkbox.
   */
  status?: 'checked' | 'unchecked' | 'indeterminate';
  /**
   * Whether checkbox is disabled.
   */
  disabled?: boolean;
  /**
   * Whether checkbox is alwaysChecked.
   */
  isAlwaysChecked?: boolean;
  /**
   * Function to execute on press.
   */
  onValueChange?: (isChecked: boolean) => void;
  /**
   * Custom color for unchecked checkbox.
   */
  uncheckedColor?: string;
  /**
   * Custom color for checkbox.
   */
  color?: string;
  /**
   * @optional
   */
  theme?: ReactNativePaper.Theme;
  /**
   * testID to be used on tests.
   */
  testID?: string;

  style: ViewStyle;
}

const CheckBoxCustom: React.FC<Props> = ({ onValueChange, style, isAlwaysChecked, ...rest }) => {
  const { colors } = useTheme();
  const [isChecked, setIsChecked] = React.useState(isAlwaysChecked || false);
  React.useEffect(() => {
    setIsChecked(!!isAlwaysChecked);
  }, [isAlwaysChecked]);
  return (
    <TouchableRipple
      rippleColor="rgba(0, 0, 0, .32)"
      style={{
        backgroundColor: colors.backgroundCheckBox,
        borderRadius: widthScale(8),
        width: widthScale(32),
        height: widthScale(32),
        alignItems: 'center',
        justifyContent: 'center',
        ...style,
      }}
      onPress={() => {
        setIsChecked(!isChecked);
        onValueChange && onValueChange(!isChecked);
      }}
    >
      <MaterialCommunityIcons
        size={widthScale(20)}
        color={isChecked ? colors.primary : 'transparent'}
        name={'check'}
      />
    </TouchableRipple>
  );
};

export default CheckBoxCustom;
