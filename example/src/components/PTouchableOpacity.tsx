import _ from 'lodash';
import React from 'react';
import { TouchableOpacity, TouchableOpacityProps } from 'react-native';

class PTouchableOpacity extends React.PureComponent<TouchableOpacityProps> {
  render() {
    const props = this.props;
    const { children } = props;

    return (
      <TouchableOpacity
        {...props}
        onPress={_.debounce(props.onPress ? props.onPress : () => {}, 600, {
          leading: true,
          trailing: false,
        })}
      >
        {children}
      </TouchableOpacity>
    );
  }
}
export default PTouchableOpacity;
