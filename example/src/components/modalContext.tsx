import React from 'react';
import { ProductProps } from '~constants/types';
import useModal from '~hooks/useModal';
import Modal from './ModalDetailProduct';

let ModalContext: React.Context<{
  product_id?: string;
  visibleModalProduct: boolean;
  product: ProductProps | undefined;
  showModalProduct: (product: ProductProps) => void;
  showModalProductWithout: (product_id: string) => void;
  hideModalProduct: (onHide?: () => void) => Promise<void>;
}>;
let { Provider } = (ModalContext = React.createContext());

let ModalProvider = ({ children }) => {
  let { visible, product, hide, show, product_id, showWithoutProduct } = useModal();
  return (
    <Provider
      value={{
        product_id,
        visibleModalProduct: visible,
        product,
        hideModalProduct: hide,
        showModalProduct: show,
        showModalProductWithout: showWithoutProduct,
      }}
    >
      <Modal />
      {children}
    </Provider>
  );
};

export { ModalContext, ModalProvider };
