import Screen from './components/menuItem';
import Navigator from './components/navigator';
import { CurvedBottomBarType } from './type';

export const CurvedBottomBar: CurvedBottomBarType = { Navigator, Screen };
