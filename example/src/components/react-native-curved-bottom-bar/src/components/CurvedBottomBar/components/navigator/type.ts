import {
  DefaultNavigatorOptions, TabRouterOptions
} from '@react-navigation/native';
import { StyleProp, ViewStyle } from 'react-native';

// Supported screen options
export type TabNavigationOptions = {
  tabBarIcon?: string;
  position?: string;
};

// Map of event name and the type of data (in event.data)
//
// canPreventDefault: true adds the defaultPrevented property to the
// emitted events.
export type TabNavigationEventMap = {
  tabPress: {
    data: { isAlreadyFocused: boolean };
    canPreventDefault: true;
  };
};

// The props accepted by the component is a combination of 3 things
export type MyTabNavigationProps = DefaultNavigatorOptions<TabNavigationOptions> & TabRouterOptions ;

interface Props extends MyTabNavigationProps {
    type?: 'down' | 'up' | string;
    style?: StyleProp<ViewStyle>;
    width?: number;
    height?: number;
    borderTopLeftRight?: boolean;
    circleWidth?: number;
    bgColor?: string;
    iconColor?: string;
    initialRouteName: string;
    strokeWidth?: number;
    renderCircle: () => JSX.Element;
    // tabBar: ({
    //   routeName,
    //   selectTab,
    //   navigation,
    // }: {
    //   routeName: string;
    //   selectTab: string;
    //   navigation: (selectTab: string) => void;
    // }) => JSX.Element;
}


export type NavigatorBottomBar = React.FC<Props>
