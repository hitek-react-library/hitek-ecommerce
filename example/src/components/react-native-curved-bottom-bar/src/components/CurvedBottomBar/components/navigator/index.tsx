import {
  createNavigatorFactory,
  NavigationHelpersContext,
  ParamListBase,
  TabActionHelpers,
  TabActions,
  TabNavigationState,
  TabRouter,
  TabRouterOptions,
  useNavigationBuilder,
} from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { DeviceEventEmitter, Dimensions, Pressable, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDeviceOrientation } from '../../../useDeviceOrientation';
import { getPath, getPathUp } from './path';
import { styles } from './styles';
import {
  NavigatorBottomBar,
  TabNavigationEventMap,
  TabNavigationOptions,
} from './type';

const defaultProps = {
  bgColor: 'gray',
  type: 'down',
  borderTopLeftRight: false,
  strokeWidth: 0,
};

const BottomBarComponent: NavigatorBottomBar = (props) => {
  const {
    type,
    style,
    width = null,
    height = 75,
    circleWidth = 60,
    bgColor,
    iconColor = '#FF3030',
    initialRouteName,
    renderCircle,
    borderTopLeftRight,
    strokeWidth,
    screenOptions,
  } = props;

  const [selectTab, setSelectTab] = useState<string>(initialRouteName);
  const [itemLeft, setItemLeft] = useState([]);
  const [itemRight, setItemRight] = useState([]);
  const [maxWidth, setMaxWidth] = useState<any>(width);
  const [maxHeight, setMaxHeight] = useState<any>(
    Dimensions.get('window').height
  );
  const children = props?.children as any[];
  const orientation = useDeviceOrientation();
  const insets = useSafeAreaInsets();

  const { state, navigation, descriptors } = useNavigationBuilder<
    TabNavigationState<ParamListBase>,
    TabRouterOptions,
    TabActionHelpers<ParamListBase>,
    TabNavigationOptions,
    TabNavigationEventMap
  >(TabRouter, {
    children,
    screenOptions,
    initialRouteName,
  });

  React.useEffect(() => {
    DeviceEventEmitter.addListener('CHANGE_TAB', (routeName) => {
      setRouteName(routeName);
    });
  }, []);
  useEffect(() => {
    const { width: w, height: h } = Dimensions.get('window');
    if (!width) {
      setMaxWidth(w);
    }
    setMaxHeight(h);
  }, [orientation]);

  const _renderButtonCenter = () => {
    return renderCircle();
  };

  useEffect(() => {
    const arrLeft: any = state.routes.filter(
      (item) => descriptors[item.key].options?.position === 'left'
    );
    const arrRight: any = state.routes.filter(
      (item) => descriptors[item.key].options?.position === 'right'
    );

    setItemLeft(arrLeft);
    setItemRight(arrRight);

    setRouteName(initialRouteName || state?.routeNames[0]);
  }, []);

  const setRouteName = (name: string) => {
    setSelectTab(name);
  };

  const d =
    type === 'down'
      ? getPath(
          maxWidth,
          height + insets.bottom / 6,
          circleWidth,
          borderTopLeftRight
        )
      : getPathUp(maxWidth, height + 30, circleWidth, borderTopLeftRight);
  const _renderIcon = (route: any, selectTab: string) => {
    const routeName = route.name;
    const routeKey = route.key;
    const tabBarIcon = descriptors[routeKey]?.options?.tabBarIcon || 'home'; //! bị mất icon refesh ko sao nha
    return (
      <MaterialCommunityIcons
        name={tabBarIcon}
        size={24}
        color={routeName === selectTab ? iconColor : '#fff7'}
      />
    );
  };
  if (d) {
    return (
      <NavigationHelpersContext.Provider value={navigation}>
        <View style={{ flex: 1 }}>
          {/* //!render screen */}
          <View style={{ height: maxHeight }}>
            {state.routes.map((route, i) => {
              const routeName = route.name;
              return (
                <View
                  key={route.key}
                  style={[
                    selectTab === routeName ? { flex: 1 } : { display: 'none' },
                  ]}
                >
                  {descriptors[route.key].render()}
                </View>
              );
            })}
          </View>
          {/* //! render item tab */}
          <View style={[styles.container, style]}>
            {insets.bottom > 0 && (
              <View
                style={{
                  backgroundColor: bgColor,
                  width: maxWidth,
                  height: insets.bottom / 2,
                  position: 'absolute',
                  bottom: 0,
                }}
              />
            )}
            <View
              style={{
                width: maxWidth,
                height: height + (type === 'down' ? 0 : 30) + insets.bottom / 2,
                backgroundColor: bgColor,
              }}
            ></View>

            <View
              style={[
                styles.main,
                { width: maxWidth },
                type === 'up' && { top: 30 },
              ]}
            >
              <View style={[styles.rowLeft, { height: height }]}>
                {itemLeft.map((route: any, index) => {
                  const routeName: string = route.name;
                  const routeKey = route.key;
                  return (
                    <Pressable
                      key={routeKey}
                      onPress={() => {
                        const event = navigation.emit({
                          type: 'tabPress',
                          target: routeKey,
                          canPreventDefault: true,
                          data: {
                            isAlreadyFocused:
                              routeKey === state.routes[state.index].key,
                          },
                        });

                        if (!event.defaultPrevented) {
                          navigation.dispatch({
                            ...TabActions.jumpTo(routeName),
                            target: state.key,
                          });
                          setRouteName(routeName);
                        }
                      }}
                      style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                    >
                      {_renderIcon(route, selectTab)}
                    </Pressable>
                  );
                })}
              </View>
              <View style={{ bottom: height * 0.3 }}>
                {_renderButtonCenter()}
              </View>
              <View style={[styles.rowRight, { height: height }]}>
                {itemRight.map((route: any, index) => {
                  const routeName = route.name;
                  const routeKey = route.key;
                  return (
                    <Pressable
                      key={routeKey}
                      onPress={() => {
                        const event = navigation.emit({
                          type: 'tabPress',
                          target: routeKey,
                          canPreventDefault: true,
                          data: {
                            isAlreadyFocused:
                              routeKey === state.routes[state.index].key,
                          },
                        });

                        if (!event.defaultPrevented) {
                          navigation.dispatch({
                            ...TabActions.jumpTo(routeName),
                            target: state.key,
                          });
                          setRouteName(routeName);
                        }
                      }}
                      style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                    >
                      {_renderIcon(route, selectTab)}
                    </Pressable>
                  );
                })}
              </View>
            </View>
          </View>
        </View>
      </NavigationHelpersContext.Provider>
    );
  }
  return null;
};

BottomBarComponent.defaultProps = defaultProps;

export const createMyNavigator = createNavigatorFactory<
  TabNavigationState<ParamListBase>,
  TabNavigationOptions,
  TabNavigationEventMap,
  typeof BottomBarComponent
>(BottomBarComponent);
