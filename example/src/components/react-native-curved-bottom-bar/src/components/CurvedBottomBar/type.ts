import { ScreenBottomBar } from './components/menuItem/type';
import { NavigatorBottomBar } from './components/navigator/type';

export type CurvedBottomBarType = {
  Navigator: NavigatorBottomBar,
  Screen: ScreenBottomBar
}
