import React from 'react';
import { Image, View } from 'react-native';
import Modal from 'react-native-modal';
import { ActivityIndicator } from 'react-native-paper';
import { IMAGE } from '~/assets/imagePath';
import { windowWidth } from '~styles/scaling-utils';

interface Props {}
interface State {
  visible: boolean;
}
export default class PSpinner extends React.PureComponent<Props, State> {
  static instance: any;

  constructor(props: Props) {
    super(props);
    PSpinner.instance = this;
    this.state = {
      visible: false,
    };
  }

  static show() {
    if (PSpinner.instance) {
      !PSpinner.instance.state.visible && PSpinner.instance.setState({ visible: true });
    }
  }

  static hide() {
    if (PSpinner.instance) {
      PSpinner.instance.setState({ visible: false });
    }
  }

  render() {
    if (PSpinner?.instance?.state?.visible) {
      return (
        <Modal
          animationInTiming={0}
          animationOutTiming={0}
          animationIn={'fadeIn'}
          animationOut={'fadeOut'}
          isVisible={this.state.visible || false}
          style={{
            margin: 0,
            padding: 0,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <View
            style={{
              borderRadius: windowWidth(0.1),
              backgroundColor: 'rgba(58, 76, 86, 0.8)',
              paddingVertical: 10,
              paddingHorizontal: 20,
            }}
          >
            <Image
              source={IMAGE.logoWhite}
              resizeMode="contain"
              style={{
                height: windowWidth(0.3),
                marginBottom: 10,
              }}
            />
            <ActivityIndicator animating={true} color={'#FCB716'} style={{ marginVertical: 10 }} />
          </View>
        </Modal>
      );
    }
    return null;
  }
}
