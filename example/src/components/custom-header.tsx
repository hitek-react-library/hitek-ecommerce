import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { ActivityIndicator, StatusBar, StatusBarStyle, View, ViewStyle } from 'react-native';
import { Appbar, Badge, useTheme } from 'react-native-paper';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useAppSelector } from '~store/storeHooks';

interface CustomHeaderProps {
  customCenter?: JSX.Element | null;
  customLeftMenu?: JSX.Element | null;
  customRightMenu?: JSX.Element | null;
  title?: string;
  subtitle?: string;
  statusBarStyle?: StatusBarStyle;
  showBackButton?: boolean;
  iconsLeftMenu?: string;
  onLeftMenuPress?: () => void;
  onTitlePress?: () => void;
  iconRightMenu?: string;
  onRightMenuPress?: () => void;
  styles?: ViewStyle;
  isLoading?: boolean;
}

const CustomHeader: React.FC<CustomHeaderProps> = ({
  customCenter,
  customLeftMenu,
  customRightMenu,
  title,
  onTitlePress,
  subtitle,
  statusBarStyle = 'light-content',
  showBackButton = false,
  iconsLeftMenu = 'menu',
  onLeftMenuPress,
  iconRightMenu = 'dots-vertical',
  onRightMenuPress,
  styles,
  isLoading = false,
}) => {
  const { top } = useSafeAreaInsets();
  const navigation = useNavigation();
  const { colors, fonts } = useTheme();
  const { totalQuantity } = useAppSelector((state) => state.cartReducer);

  return (
    <Appbar.Header statusBarHeight={top} style={styles}>
      <StatusBar barStyle={statusBarStyle} backgroundColor="transparent" />
      {onLeftMenuPress || showBackButton ? (
        <Appbar.Action
          icon={showBackButton ? 'keyboard-backspace' : iconsLeftMenu}
          onPress={
            showBackButton
              ? () => {
                  navigation.goBack();
                  onLeftMenuPress && onLeftMenuPress();
                }
              : onLeftMenuPress
          }
        />
      ) : customLeftMenu ? null : (
        <Appbar.Action icon={iconRightMenu} color="transparent" />
      )}
      {customLeftMenu || null}
      {customCenter || (
        <Appbar.Content
          title={title}
          subtitle={subtitle}
          titleStyle={{ textAlign: 'center', ...fonts.regular }}
          subtitleStyle={{ textAlign: 'center', ...fonts.regular }}
          onPress={onTitlePress}
        />
      )}

      {customRightMenu || null}
      {onRightMenuPress ? (
        <View>
          <Appbar.Action icon={iconRightMenu} color={colors.surface} onPress={onRightMenuPress} />
          {iconRightMenu === 'cart-outline' ? (
            <Badge style={{ position: 'absolute', right: 4 }} visible={totalQuantity > 0}>
              {totalQuantity}
            </Badge>
          ) : null}
        </View>
      ) : customRightMenu ? null : (
        <Appbar.Action icon={iconsLeftMenu} color="transparent" />
      )}
      {isLoading && (
        <ActivityIndicator color={colors.surface} style={{ right: 4 }}></ActivityIndicator>
      )}
    </Appbar.Header>
  );
};

export default CustomHeader;
