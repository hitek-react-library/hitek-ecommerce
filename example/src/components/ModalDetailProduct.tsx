import { skipToken } from '@reduxjs/toolkit/dist/query';
import React, { useState } from 'react';
import { Image, View } from 'react-native';
import Modal from 'react-native-modal';
import {
  Button,
  Paragraph,
  Subheading,
  Title,
  useTheme,
} from 'react-native-paper';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FixedContainer from '~components/fixed-container';
import { PRow } from '~components/PRow';
import { PScrollView } from '~components/PScrollView';
import PStepper from '~components/PStepper';
import PTouchableOpacity from '~components/PTouchableOpacity';
import { FavoriteProps, UserProps } from '~constants/types';
import { addItemToCart } from '~reducers/cartReducer';
import {
  useAddFavoriteMutation,
  useDeleteFavoriteMutation,
} from '~services/api_favorite_services';
import { useFindOneProductQuery } from '~services/api_product_services';
import { useAppDispatch, useAppSelector } from '~store/storeHooks';
import { pColor } from '~styles/colors';
import {
  heightScale,
  screenHeight,
  screenWidth,
  widthScale,
} from '~styles/scaling-utils';
import { pFont } from '~styles/typography';
import { getNumberString } from '~utils/numberUtils';
import { myAlert } from '../utils/MyAlert';
import { ModalContext } from './modalContext';

const ModalDetailProduct = () => {
  const {
    visibleModalProduct,
    product: productItem,
    hideModalProduct,
    product_id,
  } = React.useContext(ModalContext);
  const [createFavorite, { isLoading: isUpdating }] = useAddFavoriteMutation();
  const { data, isLoading } = useFindOneProductQuery(product_id ?? skipToken);
  const product = product_id ? data : productItem;

  const [removeFavorite] = useDeleteFavoriteMutation();

  const userReducer = useAppSelector(
    (state) => state.userReducer.user
  ) as UserProps;
  const listFavorite = useAppSelector(
    (state) => state.favoriteReducer.favorite
  );

  const { colors } = useTheme();
  const dispatch = useAppDispatch();
  const insets = useSafeAreaInsets();
  const [quantity, setQuantity] = useState(1);
  const [activeSlide, setActiveSlide] = useState(0);
  const [activeView, setActiveView] = useState(0);
  const [imageView, setImageView] = useState(false);
  const [isLiked, setIsLiked] = useState<FavoriteProps | undefined>(undefined);

  if (visibleModalProduct && product) {
    const renderItemImage = ({ item, index }) => {
      return (
        <PTouchableOpacity
          onPress={() => {
            setImageView(true);
            setActiveView(index);
          }}
        >
          <Image
            source={{ uri: item }}
            style={{ flex: 1, height: widthScale(350) }}
            resizeMode="cover"
          />
        </PTouchableOpacity>
      );
    };

    const listImage = product?.image
      ? [product?.thumbnail, ...product?.image]?.filter((product) => product)
      : [product?.thumbnail];

    const onModalHide = () => {
      setQuantity(1);
      setActiveSlide(0);
      setActiveView(0);
      setImageView(false);
      setIsLiked(undefined);
    };

    return (
      <Modal
        isVisible={visibleModalProduct}
        onShow={() => {
          setIsLiked(listFavorite.find((v) => v.product_id === product?.id));
        }}
        onBackButtonPress={() =>
          hideModalProduct(() => {
            onModalHide();
          })
        }
        onBackdropPress={() =>
          hideModalProduct(() => {
            onModalHide();
          })
        }
        onModalHide={onModalHide}
        style={{
          margin: 0,
          padding: 0,
          flex: 1,
          alignItems: 'center',
        }}
      >
        <FixedContainer
          edges={['right', 'left', 'bottom']}
          style={{ flex: 1, backgroundColor: pColor.white }}
        >
          <PScrollView style={{ flex: 1 }}>
            {/* <Carousel
              data={listImage}
              renderItem={renderItemImage}
              hasParallaxImages={true}
              layout={'default'}
              inactiveSlideScale={0.95}
              inactiveSlideShift={-5}
              pagingEnabled
              itemWidth={screenWidth()}
              sliderWidth={screenWidth()}
              onSnapToItem={(index) => setActiveSlide(index)}
            />
            <Pagination
              dotsLength={listImage?.length}
              activeDotIndex={activeSlide}
              containerStyle={{
                position: 'absolute',
                alignSelf: 'center',
                top: widthScale(290),
              }}
              dotStyle={{
                width: widthScale(16),
                height: widthScale(16),
                borderRadius: widthScale(16),
                marginHorizontal: widthScale(4),
                backgroundColor: colors.accent,
                borderWidth: widthScale(3),
                borderColor: colors.primary,
              }}
              inactiveDotOpacity={0.8}
              inactiveDotScale={0.6}
            /> */}
            <View
              style={{
                marginHorizontal: widthScale(10),
                marginTop: heightScale(20),
              }}
            >
              <Title
                style={{ fontWeight: 'bold', marginBottom: widthScale(4) }}
              >
                {product?.title}
              </Title>
              {userReducer.is_collab && product.contributor_rate > 0 ? (
                <PRow>
                  <Subheading
                    style={{
                      color: colors.disabled,
                      fontFamily: pFont.FONT2,
                      textDecorationLine: 'line-through',
                      top: widthScale(2),
                      marginRight: widthScale(4),
                    }}
                  >
                    {getNumberString(product?.price)}
                  </Subheading>
                  <Title
                    style={{ fontFamily: pFont.FONT2, color: colors.accent }}
                  >
                    {getNumberString(
                      product?.price * (1 - product?.contributor_rate)
                    )}
                  </Title>
                </PRow>
              ) : (
                <Title
                  style={{
                    fontFamily: pFont.FONT2,
                    color: colors.accent,
                  }}
                >
                  {product?.price ? getNumberString(product?.price) : 'Liên hệ'}
                </Title>
              )}
              <Subheading style={{ marginVertical: heightScale(8) }}>
                {userReducer.is_collab
                  ? `Tiền hoa hồng ${getNumberString(
                      product?.price * product.contributor_bonus_rate
                    )}/sản phẩm`
                  : `Điểm tích lũy ${product?.bonus_point}`}
                {!userReducer.is_collab && (
                  <MaterialCommunityIcons
                    name="star-circle"
                    size={widthScale(12)}
                    color="grey"
                  />
                )}
              </Subheading>
              <Paragraph style={{ color: colors.placeholder }}>
                {product?.description}
              </Paragraph>
            </View>
          </PScrollView>
          <View
            style={{
              height: heightScale(50),
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginHorizontal: widthScale(12),
            }}
          >
            <PStepper
              initValue={quantity}
              minValue={1}
              maxValue={999}
              onChangeValue={(count: number) => {
                setQuantity(count);
              }}
            />
            <Button
              onPress={() => {
                hideModalProduct(() => {
                  if (product) {
                    dispatch(
                      addItemToCart({ product: product, quantity: quantity })
                    );
                    myAlert('đã thêm sản phẩm');
                  }
                  onModalHide();
                });
              }}
              style={{
                height: heightScale(36),
                borderRadius: heightScale(36),
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: colors.accent,
              }}
            >
              {'Thêm giỏ hàng'}
            </Button>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingHorizontal: widthScale(12),
              width: screenWidth(),
              position: 'absolute',
              top: insets.top * 1.2,
            }}
          >
            <PTouchableOpacity
              style={{
                backgroundColor: pColor.greyLight,
                width: widthScale(34),
                height: widthScale(34),
                borderRadius: widthScale(30),
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => hideModalProduct(() => onModalHide())}
            >
              <MaterialCommunityIcons
                name="keyboard-backspace"
                color={'white'}
                size={widthScale(20)}
                style={{}}
              />
            </PTouchableOpacity>
            <View style={{ flexDirection: 'row' }}>
              <PTouchableOpacity
                style={{
                  backgroundColor: pColor.greyLight,
                  width: widthScale(34),
                  height: widthScale(34),
                  borderRadius: widthScale(30),
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginRight: widthScale(16),
                }}
                onPress={() => console.log('share')}
              >
                <MaterialCommunityIcons
                  name={'share-variant'}
                  color={colors.accent}
                  size={widthScale(20)}
                  style={{}}
                />
              </PTouchableOpacity>

              <PTouchableOpacity
                style={{
                  backgroundColor: pColor.greyLight,
                  width: widthScale(34),
                  height: widthScale(34),
                  borderRadius: widthScale(30),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() => {
                  if (product) {
                    if (isLiked) {
                      removeFavorite({ id: isLiked.id })
                        .unwrap()
                        .then((res) => {
                          setIsLiked(undefined);
                        });
                    } else {
                      createFavorite({
                        user_id: userReducer.id,
                        product_id: product.id,
                      })
                        .unwrap()
                        .then((res) => {
                          setIsLiked(res);
                        });
                    }
                  }
                }}
              >
                <MaterialCommunityIcons
                  name={isLiked ? 'heart' : 'heart-outline'}
                  color={colors.accent}
                  size={widthScale(20)}
                  style={{}}
                />
              </PTouchableOpacity>
            </View>
          </View>
        </FixedContainer>

        {imageView ? (
          <View
            style={{
              width: screenWidth(),
              height: screenHeight(),
              position: 'absolute',
              backgroundColor: 'black',
            }}
          >
            <PTouchableOpacity
              style={{
                position: 'absolute',
                backgroundColor: pColor.greyLight,
                width: widthScale(34),
                height: widthScale(34),
                borderRadius: widthScale(30),
                left: widthScale(16),
                top: insets.top * 1.2,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => setImageView(false)}
            >
              <MaterialCommunityIcons
                name="close"
                color={'white'}
                size={widthScale(20)}
                style={{}}
              />
            </PTouchableOpacity>
          </View>
        ) : null}
      </Modal>
    );
  } else return null;
};

export default ModalDetailProduct;
