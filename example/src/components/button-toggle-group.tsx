import MaskedView from '@react-native-community/masked-view';
import * as React from 'react';
import {
  Animated,
  Platform,
  StyleProp,
  StyleSheet,
  TextStyle,
  View,
  ViewStyle,
} from 'react-native';
import { Text, TouchableRipple } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

interface ButtonToggleGroupProps {
  values: string[];
  valuesShow: string[];
  value: string;
  heightItem: number;
  onSelect: (val: string) => void;
  style?: StyleProp<ViewStyle>;
  highlightBackgroundColor: string;
  highlightTextColor: string;
  inactiveBackgroundColor: string;
  inactiveTextColor: string;
  textStyle?: StyleProp<TextStyle>;
}
const ButtonToggleGroup: React.FC<ButtonToggleGroupProps> = ({
  values,
  valuesShow,
  value,
  heightItem,
  onSelect,
  style,
  highlightBackgroundColor,
  highlightTextColor,
  inactiveBackgroundColor,
  inactiveTextColor,
  textStyle = {},
}) => {
  const [prevSelectedIndex, setPrevSelectedIndex] = React.useState(0);
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const selectedPanelBottom = React.useRef(new Animated.Value(0));

  const widthSize = 100 / values.length;

  const interpolatedValuesInput = values.map((_, i) => {
    return widthSize * i;
  });

  const interpolatedValuesOutput = values.map((_, i) => {
    return `${widthSize * i}%`;
  });

  React.useEffect(() => {
    const bottom = widthSize * selectedIndex;

    Animated.timing(selectedPanelBottom.current, {
      toValue: bottom,
      duration: 300,
      useNativeDriver: false,
    }).start(() => {
      setPrevSelectedIndex(selectedIndex);
    });
  }, [widthSize, selectedPanelBottom, selectedIndex]);

  React.useEffect(() => {
    const newIndex = values.findIndex((v) => v === value);
    setPrevSelectedIndex(selectedIndex);
    setSelectedIndex(newIndex);
  }, [values, value, selectedIndex]);

  // This allows the text to render under the related animation while the mask is gliding across
  // Notice the `.start(setPrevIndex)` to reset the previous index once the animation has stabilized
  const maxIndex = selectedIndex > prevSelectedIndex ? selectedIndex : prevSelectedIndex;
  const minIndex = selectedIndex > prevSelectedIndex ? prevSelectedIndex : selectedIndex;

  const highlightMask = {
    backgroundColor: highlightBackgroundColor,
  };

  const highlightText = {
    color: highlightTextColor,
  };

  const inactiveText = {
    color: inactiveTextColor,
  };

  const inactiveBackground = {
    backgroundColor: inactiveBackgroundColor,
  };

  /**
   * For whatever reason, the `zIndex: -1` on Text works on Android, but does not work
   * on iOS. However, when we can get away with only removing the Text from zIndex,
   * the ripple effect continues to work on Android. As such, we conditionally
   * apply the logic for Android vs iOS
   */
  const inactiveContainerIOS = Platform.OS === 'ios' ? { zIndex: -1 } : {};

  return (
    <View
      style={[{ height: heightItem * values?.length }, style]}
      accessible
      accessibilityRole="radiogroup"
    >
      <MaskedView
        importantForAccessibility={'no-hide-descendants'}
        accessibilityElementsHidden={true}
        key={selectedIndex}
        style={styles.maskViewContainer}
        maskElement={
          <Animated.View
            style={[
              styles.blueMaskContainer,
              {
                top: selectedPanelBottom.current.interpolate({
                  inputRange: interpolatedValuesInput,
                  outputRange: interpolatedValuesOutput,
                }),
                height: heightItem,
              },
            ]}
          />
        }
      >
        <View style={[styles.baseButtonContainer, highlightMask]}>
          {values.map((value, i) => (
            <TouchableRipple
              rippleColor="white"
              key={i}
              onPress={() => {
                setSelectedIndex(i);
                onSelect(values[i]);
              }}
              style={[
                styles.baseTouchableRipple,
                {
                  borderWidth: 2,
                  borderColor: 'lightgrey',
                },
              ]}
            >
              <>
                <MaterialCommunityIcons
                  name="radiobox-marked"
                  color={highlightTextColor}
                  size={20}
                  style={{}}
                />
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <Text
                    style={[styles.baseButtonText, styles.highlightText, textStyle, highlightText]}
                    numberOfLines={1}
                  >
                    {(valuesShow && valuesShow[i]) || value}
                  </Text>
                </View>
              </>
            </TouchableRipple>
          ))}
        </View>
      </MaskedView>
      <View
        style={[styles.baseButtonContainer, styles.inactiveButtonContainer, inactiveContainerIOS]}
      >
        {values.map((value, i) => (
          <View
            key={i}
            style={[
              styles.baseTouchableRipple,
              {
                zIndex: minIndex <= i && maxIndex >= i ? -1 : 0,
                left: 3,
              },
              inactiveBackground,
            ]}
          >
            <View style={{ alignItems: 'center' }}>
              <View
                style={{
                  height: heightItem - 2,
                  top: -heightItem + 2,
                  left: 8,
                  width: 2,
                  backgroundColor: 0 === i ? 'transparent' : 'lightgrey',
                  position: 'absolute',
                }}
              />
              <MaterialCommunityIcons
                name="checkbox-blank-circle"
                color={'lightgrey'}
                size={18}
                style={{}}
              />
            </View>
            <TouchableRipple
              accessibilityRole="radio"
              accessibilityState={{ checked: selectedIndex === i }}
              accessibilityLiveRegion="polite"
              onPress={() => {
                setSelectedIndex(i);
                onSelect(values[i]);
              }}
              style={{ flex: 1, justifyContent: 'center' }}
            >
              <Text style={[styles.baseButtonText, textStyle, inactiveText]} numberOfLines={1}>
                {(valuesShow && valuesShow[i]) || value}
              </Text>
            </TouchableRipple>
          </View>
        ))}
      </View>
    </View>
  );
};

export default ButtonToggleGroup;

const styles = StyleSheet.create({
  container: {
    position: 'relative',
  },
  maskViewContainer: {
    width: '100%',
    height: '100%',
    position: 'relative',
  },
  blueMaskContainer: {
    position: 'absolute',
    backgroundColor: 'black',
    borderRadius: 4,
    width: '100%',
    left: 0,
    top: 0,
  },
  baseButtonContainer: {
    flex: 1,
    alignItems: 'center',
  },
  inactiveButtonContainer: {
    position: 'absolute',
    top: 4,
    left: 0,
    width: '100%',
    height: '100%',
  },
  baseTouchableRipple: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 8,
    width: '100%',
    height: '100%',
    flex: 1,
  },
  baseButtonText: {
    textAlign: 'left',
    paddingHorizontal: 16,
  },
  highlightText: {
    zIndex: 1,
  },
});
