import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

interface ButtonProp {
  text: string;
  onPressed?: Function;
}

export const NoBorderButton: React.FC<ButtonProp> = ({
  text,
  onPressed,
}) => {
  return (
    <TouchableOpacity onPress={() => (onPressed ? onPressed() : null)}>
      <Text
        style={{
          textAlign: 'center',
          padding: 14,
          marginVertical: 8,
          color: 'blue',
        }}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};
