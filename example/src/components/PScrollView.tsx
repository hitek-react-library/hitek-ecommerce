import React from 'react';
import { ScrollView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

interface Props {
  isTabScreen?: boolean;
  isHaveTextInput?: boolean;
}
export const PScrollView: React.FC<React.ComponentProps<typeof ScrollView> & Props> = ({
  children,
  style,
  contentContainerStyle,
  isTabScreen = false,
  isHaveTextInput = false,
  ...rest
}) => {
  const insets = useSafeAreaInsets();
  const KeyboardWithView = isHaveTextInput ? KeyboardAwareScrollView : ScrollView;
  return (
    <KeyboardWithView
      enableOnAndroid
      keyboardShouldPersistTaps="handled"
      nestedScrollEnabled={true}
      bounces={false}
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      keyboardDismissMode="interactive"
      {...rest}
      contentContainerStyle={[
        contentContainerStyle,
        isTabScreen && { paddingBottom: 65 + insets.bottom / 2 },
      ]}
    >
      {children}
    </KeyboardWithView>
  );
};
