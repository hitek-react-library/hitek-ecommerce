import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal';

interface Props {
  visible?: boolean;
  title?: String;
  children?: object;
  passQuiz?: Boolean;
  hideButtonClose?: Boolean;
  disableBackdrop?: boolean;
  isLoadding?: Boolean;
  styleText?: Object;
  onPressClose?: Function;
}
const MyModal = (props: Props, ref: any) => {
  const [visible, setVisible] = useState<boolean>(props?.visible || false);
  const show = () => {
    if (!props?.isLoadding) setVisible(true);
  };
  const hide = async () => {
    setVisible(false);
  };

  const getVisible = () => {
    return visible;
  };

  useImperativeHandle(ref, () => ({ show: show, hide: hide, getVisible: getVisible }));

  return (
    <Modal
      isVisible={visible}
      onBackButtonPress={hide}
      style={{
        margin: 0,
        padding: 0,
        flex: 1,
        alignItems: 'center',
      }}
    >
      <View style={{ flex: 1 }}>{props.children}</View>
    </Modal>
  );
};
export default forwardRef(MyModal);
