import React, { useEffect, useMemo, useState } from 'react';
import {
  Pressable,
  StyleSheet,
  TextInput,
  View,
  ViewStyle,
  Image,
} from 'react-native';
import { numbersOnly } from '~utils/numberUtils';
import { ICON } from '../assets/imagePath';

type StepperProps = {
  onMinValue?: () => void | undefined;
  onChangeValue?: ((count: number) => void) | undefined;
  minValue?: number;
  maxValue?: number;
  initValue?: number;
  disabledInput?: boolean;
  style?: ViewStyle;
};

type IconProps = {
  height?: number;
  width?: number;
  fill?: string;
};

const MinusIcon = ({
  width = 20,
  height = 20,
  fill = 'black',
}: IconProps): JSX.Element => {
  return (
    <Image
      source={ICON.down}
      style={{ width: width, height: height, backgroundColor: fill }}
    ></Image>
    // <Svg width={width} height={height} viewBox="0 0 24 24" fill="none">
    //   <Rect width="24" height="24" fill="none" fill-opacity="0.01" />
    //   <Path
    //     d="M22 12.15L2 12.05"
    //     stroke={fill}
    //     strokeWidth="2"
    //     strokeLinecap="round"
    //     strokeLinejoin="round"
    //   />
    // </Svg>
  );
};

const PlusIcon = ({
  width = 20,
  height = 20,
  fill = 'black',
}: IconProps): JSX.Element => {
  return (
    <Image
      source={ICON.up}
      style={{ width: width, height: height, backgroundColor: fill }}
    ></Image>

    // <Svg width={width} height={height} viewBox="0 0 24 24" fill="none">
    //   <Rect width="24" height="24" fill="none" fill-opacity="0.01" />
    //   <Path
    //     d="M21 12.135L3 12.045M12.135 21L12.045 3"
    //     stroke={fill}
    //     strokeWidth="2"
    //     strokeLinecap="round"
    //     strokeLinejoin="round"
    //   />
    // </Svg>
  );
};

const PStepper: React.FC<StepperProps> = ({
  initValue = 0,
  minValue = -100,
  maxValue = 9999,
  onMinValue,
  onChangeValue,
  disabledInput = false,
  style,
}: StepperProps) => {
  const [value, setValue] = useState(initValue);
  const [valueInput, setValueInput] = useState(initValue);
  const minIsDisabled = useMemo(() => value <= minValue, [minValue, value]);
  const maxIsDisabled = useMemo(() => value >= maxValue, [maxValue, value]);

  const handleIncrement = () => {
    setValue((oldValue: number) => oldValue + 1);
  };
  const handleDecrement = () => {
    setValue((oldValue: number) => oldValue - 1);
  };
  useEffect(() => {
    if (onMinValue && value <= minValue) {
      onMinValue();
      setValue(1);
    } else {
      if (value !== initValue) {
        onChangeValue && onChangeValue(value);
        setValueInput(value);
      }
    }
  }, [value]);
  return (
    <View style={{ flexDirection: 'row', alignItems: 'center', ...style }}>
      <Pressable
        onPress={handleDecrement}
        disabled={minIsDisabled}
        style={({ pressed }) => [
          styles.button,
          {
            borderRightWidth: 0,
            borderTopLeftRadius: 4,
            borderBottomLeftRadius: 4,
            backgroundColor: pressed ? 'lightgray' : 'white',
          },
        ]}
      >
        <MinusIcon
          fill={minIsDisabled ? 'lightgray' : 'black'}
          width={16}
          height={16}
        />
      </Pressable>
      <TextInput
        editable={!disabledInput}
        keyboardType="numeric"
        maxLength={maxValue?.toString()?.length}
        value={valueInput > 0 ? valueInput?.toString() : '0'}
        style={[
          styles.button,
          {
            width: 50,
            paddingVertical: 0,
            textAlign: 'center',
            backgroundColor: 'white',
            color: 'black',
          },
        ]}
        onChangeText={(text: string) =>
          numbersOnly(text) && setValueInput(parseInt(text))
        }
        onEndEditing={() => {
          setValue(valueInput);
        }}
      />
      <Pressable
        onPress={handleIncrement}
        disabled={maxIsDisabled}
        style={({ pressed }) => [
          styles.button,
          {
            borderLeftWidth: 0,
            borderTopRightRadius: 4,
            borderBottomRightRadius: 4,
            backgroundColor: pressed ? 'lightgray' : 'white',
          },
        ]}
      >
        <PlusIcon
          fill={maxIsDisabled ? 'lightgray' : 'black'}
          width={16}
          height={16}
        />
      </Pressable>
    </View>
  );
};

export default PStepper;

const styles = StyleSheet.create({
  button: {
    borderWidth: 1.2,
    height: 32,
    width: 32,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#0003',
  },
});
