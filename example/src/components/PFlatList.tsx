import React, { useEffect, useState } from 'react';
import { FlatList, Image, RefreshControl, ScrollView, Text, View } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { IMAGE } from '~/assets/imagePath';
import { windowWidth } from '~styles/scaling-utils';

interface Props {
  isTabScreen?: boolean;
  isLoading?: boolean;
  textNull?: string;
}
let timer: NodeJS.Timeout;
export const PFlatList: React.FC<React.ComponentProps<typeof FlatList> & Props> = ({
  children,
  style,
  data,
  refreshing,
  contentContainerStyle,
  onRefresh,
  isTabScreen = false,
  isLoading = false,
  ListFooterComponent,
  textNull = 'Đang tải dữ liệu mới vui lòng chờ...',
  ...rest
}) => {
  const insets = useSafeAreaInsets();
  const renderListFooterComponent = isLoading ? (
    <ActivityIndicator size={30} color="#FCB716" style={{ marginVertical: 16 }} />
  ) : (
    ListFooterComponent || (
      <ActivityIndicator size={30} color="transparent" style={{ marginVertical: 16 }} />
    )
  );
  const [show, setShow] = useState(true);

  useEffect(() => {
    if (!data || data?.length === 0) {
      timer = setTimeout(() => setShow(false), 10000);
      return () => {
        clearTimeout(timer);
      };
    }
  }, [data]);

  if (!data || data?.length === 0) {
    return (
      <ScrollView
        contentContainerStyle={{ flex: 1 }}
        refreshControl={
          <RefreshControl
            refreshing={(!isLoading && refreshing) || false}
            onRefresh={() => {
              setShow(true);
              onRefresh && onRefresh();
            }}
          />
        }
      >
        <View style={[{ flex: 1, alignItems: 'center', justifyContent: 'center' }]}>
          <Image source={IMAGE.logo2} resizeMode="contain" style={{ height: windowWidth(0.3) }} />
          {isLoading || show ? (
            <ActivityIndicator size={30} color="#FCB716" style={{ marginVertical: 3 }} />
          ) : (
            <Text style={[{ textAlign: 'center', alignSelf: 'center' }]}>{textNull}</Text>
          )}
        </View>
      </ScrollView>
    );
  }
  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      keyExtractor={(item: any) => item?.id}
      {...rest}
      refreshing={refreshing || false}
      onRefresh={onRefresh ? onRefresh : () => {}}
      refreshControl={
        <RefreshControl
          refreshing={(!isLoading && refreshing) || false}
          onRefresh={() => {
            setShow(true);
            onRefresh && onRefresh();
          }}
        />
      }
      data={data}
      ListFooterComponent={renderListFooterComponent}
      contentContainerStyle={[
        contentContainerStyle,
        isTabScreen && { paddingBottom: 65 + insets.bottom / 2 },
      ]}
    />
  );
};
