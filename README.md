# HITEK AUTH LIBRARY


## Latest version 0.1.15
<br/>

## Features
+ Login with socials: Google, Facebook, Apple, Kakao, Naver
+ Basic login by username/password
+ Login by phone and code verification (firebase service)
+ Register account
+ Send forgot password link
+ Find email account by provided phone and birthday
+ Management login status and auth token

<br/>

## How to use
### 1. Install library
```
yarn add https://gitlab.com/hitek-react-library/hitek-auth/-/raw/master/release/hitek-auth-<version>.tgz
```

```
// Google SignIn
yarn add @react-native-google-signin/google-signin

// Facebook SignIn
yarn add react-native-fbsdk
yarn add @types/react-native-fbsdk

// Apple SignIn
yarn add @invertase/react-native-apple-authentication

// Kakao SignIn
yarn add @react-native-seoul/kakao-login

// Naver SignIn
yarn add "https://github.com/wifihouse/react-native-naver-login/raw/master/react-native-seoul-naver-login-2.1.0.tgz"

// Firebase phone SingnIn
yarn add @react-native-firebase/app
yarn add @react-native-firebase/auth
```

<br/>

### 2. Setup provider and add configure
+ Import to App.tsx file (entry point of application)

`import { AuthProvider, setConfig } from 'hitek-auth';`


+ Wrap `<AuthProvider>` to root component

```
...
  return (
    <AuthProvider>
      ...
    </AuthProvider>
  );
```

+ Call `setConfig` inside useEffect method

```
React.useEffect(() => {
    setConfig({
      host: 'https://***/api/v1', // Root server domain
      log: true,
      google: {
        webClientId: '***', // web client id from firebase console
      },
      naver: { // config for Naver signin 
        androidConfig: {
          kConsumerKey: '***',
          kConsumerSecret: '***',
          kServiceAppName: '***',
        },
        iosConfig: {
          kConsumerKey: '***',
          kConsumerSecret: '***',
          kServiceAppName: '***',
          kServiceAppUrlScheme: '***', // only for iOS
        },
      },
    });
  }, []);
```

<br/>

### 3. Login with social
+ Example signin with Google 
```
import { SignInMethod } from 'hitek-auth';
...
const loginWithGoogle = async () => {
    try {
      const account: Account = await signIn(SignInMethod.GOOGLE);
      console.log(account);
    } catch (e) {
      console.log('@error', e);
    }
  };
```
+ To signin with another social, please set param as value of list
```
export enum SignInMethod {
    GOOGLE,
    FACEBOOK,
    APPLE,
    NAVER,
    KAKAO
}
```

<br/>

### 4. Login with phone and code verification
+ Send request signin with phone
```
import { PhoneLoginSupport } from 'hitek-auth';
...
const loginWithPhone = async (phone: string) => {
  await PhoneLoginSupport.getInstance().signPhoneNumber(phone);
  navigation.navigate('Code', { phone });
}
```

+ Input code received
```
import { PhoneLoginSupport } from 'hitek-auth';
...
const verifyCode = async (code: string) => {
  try {
    const token = await PhoneLoginSupport.getInstance().confirmCode(code);
    const user = await signInWithFbToken(token ?? "");
    print("@user", user);
  } catch(e) {
    console.log("@err", e.message)
  }
  return;
}
```

<br/>

### 5. Get auth token
```
import { getToken } from 'hitek-auth';
...
const token = await getToken();
print('@token', token);
```


<br/>

### 6. Signout
```
import { signOut } from 'hitek-auth';
...
await signOut();
```

<br/>
<br/>

## Guide to setup social project

### 1.  Google
+ [Home page](https://github.com/react-native-google-signin/google-signin)
+ [Android guide](https://github.com/react-native-google-signin/google-signin/blob/master/docs/android-guide.md)
+ [iOS guide](https://github.com/react-native-google-signin/google-signin/blob/master/docs/ios-guide.md)

### 2. Facebook
+ [Home page](https://github.com/thebergamo/react-native-fbsdk-next)

### 3. Kakao
+ [Home page](https://github.com/react-native-seoul/react-native-kakao-login)

### 4. Naver
+ [Home page](https://github.com/react-native-seoul/react-native-naver-login)

### 5. Apple
+ [Home page](https://github.com/invertase/react-native-apple-authentication)


<br/><br/>
## Example code
You can refer or reuse [example code](example/)
<br/>



<br/><br/><br/>

# Bonus part
## Available react native template
https://github.com/phattran1201/react-native-template-ts-redux


<br/><br/><br/>

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
